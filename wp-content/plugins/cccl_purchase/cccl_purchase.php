<?php
/*
Plugin Name: CCCL Purchase plugin
Version:     1.0
*/
$page_id = $current_page_id;
function cccl_purchase_plugin( $atts ) {
	// define shortcode variables
	extract( shortcode_atts( array(
			'category' => '',
			'productid' => '',
	), $atts ) );
	$output = purchase_product($category, $productid);
	// return output
	return $output;	
}
// register shortcode function
add_shortcode( 'CCCL_Purchase_Plugin', 'cccl_purchase_plugin' );

// enqueue public inline style
function cccl_purchase_plugin_css() {
	// enqueue CSS file
	$src = plugin_dir_url( __FILE__ ) .'css/cccl_purchase.css';
	wp_enqueue_style( 'cccl_purchase_plugin-public', $src, array(), null, 'all' );
}
add_action( 'wp_enqueue_scripts', 'cccl_purchase_plugin_css' );

// enqueue public inline script
function cccl_purchase_plugin_js() {
	// enqueue JS file
	$src = plugin_dir_url( __FILE__ ) .'js/cccl_purchase.js';
	wp_enqueue_script( 'cccl_purchase_plugin-public', $src, array(), null, false );
}
add_action( 'wp_enqueue_scripts', 'cccl_purchase_plugin_js' );

	// display the plugin settings page
function purchase_product($category, $productid) {
	$user = wp_get_current_user();
	$current_date = date('Y-m-d');
	$is_fixed_amount = false;
	$_SESSION["category"] = $category;
	if ($_POST['task'] == "searchMember") {
		$user_details = get_user_list($_POST["mmbrId"]);
		if (empty($productid) && $category == constant("VENUE")){
			$product_details = get_product_list_by_category($category, true);
		} else {
			$product_details = get_product_details_by_pid($productid, true);
			if ($category == constant("VENUE")){
				$item_list = get_items_availability ($current_date, $productid);
			} else {
				$item_list = get_items_by_product_id ($productid, true);
			}
		}
	} elseif ($_POST['task'] == "dtvanue") {
		$current_date = date("Y-m-d", strtotime($_POST['searchAvailability'])) ;
		$user_details = get_user_list($_POST["mmbrId"]);
		$product_details = get_product_details_by_pid($productid, true);
		$item_list = get_items_availability ($current_date, $productid);
	} elseif ($_POST['task'] == "receivePayment" && !$_POST['tempHold']){
		$_SESSION["comment"] = $_POST['comment'];
		$error = check_items_status ($_POST['description']);
		if (empty($error)) {
			$error = process_purchase_product();
		}
	} elseif ($_POST['task'] == "receivePayment" && $_POST['tempHold']) {
		$_SESSION["comment"] = $_POST['comment'];
		$error = check_items_status ($_POST['description']);
		if(empty($error)) {
			$error = process_temporary_hold();
		}
	} elseif (in_array( 'subscriber', (array) $user->roles )) {
		$user_details = get_user_list($user->user_login);
		$product_details = get_product_details_by_pid($productid, true);
		if ($category == constant("VENUE")){
			$item_list = get_items_availability ($current_date, $productid);
		} else {
			$item_list = get_items_by_product_id ($productid, true);
		}
	}
	$memberStatus = checkMemberStatus($user_details);
	$is_user_avil = !empty($user_details)? true : false;
	ob_start();
?>
	<div class="container purchaseEventProduct">
		<div class="row">
			<div class="col-md-12  col-sm-12 col-xs-12 ">
			<?php 
			if (($_POST['task'] == "searchMember" || in_array( 'subscriber', (array) $user->roles )) && !$is_user_avil){
				$msg = "";
				if(empty($user_details)) {
					$msg = "Member";
				}elseif (empty($product_details)){
					$msg = "Product";
				}elseif (empty((array)$item_list)) {
					$msg = "Item";
				}
				echo show_custom_message("$msg details not found", "warning");
			} elseif ($_POST['task'] == "receivePayment" && !empty($error)) {
				echo show_custom_message($error, "warning");
			}
			?>
				<?php if ( !in_array( 'subscriber', (array) $user->roles ) ) { ?>
				<div class="row">
					<form class="form-inline" action="<?php echo  get_permalink( $page_id );?>" id="searchMemberForm" method="post">
						<div class="form-group col-md-12 col-sm-12 col-xs-12">
							<label for="mmbrId" class="marginLeft">Search Member</label>
							<input type="type" class="form-control marginLeft" id="mmbrId" name="mmbrId" value="<?php echo $_POST["mmbrId"];?>" required placeholder="Member Id">
							<input type="hidden" name="task" value="searchMember">
							<button type="submit" class="btn btn-primary btn-sm ">Search</button>
						</div>
					</form>
				</div>
				<?php if ($is_user_avil ){?>
				<div class="row memberInfo">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-2 col-xs-4 col-sm-2"><label>ID:</label></div>
							<div class="col-md-8 col-xs-7 col-sm-9"><span class="highlight memberId"><?php echo $user_details->member_full_id;?></span></div>
						</div>
						<div class="row">
							<div class="col-md-2 col-xs-4 col-sm-2"><label>Name: </label></div>
							<div class="col-md-8 col-xs-7 col-sm-9"><span class="highlight memberName"><?php echo $user_details->display_name;?></span></div>
						</div>
						<div class="row">
							<div class="col-md-2 col-xs-4 col-sm-2"><label>Type:</label></div>
							<div class="col-md-8 col-xs-7 col-sm-9"><span class="highlight memberType"><?php echo $user_details->member_type;?></span></div>
						</div>
						<div class="row">
							<div class="col-md-2 col-xs-4 col-sm-2"><label>Status:</label></div>
							<div class="col-md-8 col-xs-7 col-sm-9"><span class="highlight memberType"><?php echo $user_details->status;?></span></div>
						</div>
					</div>
				</div>
					<?php }?>
				<?php } ?>

				<form id="main-purchase-form" action="<?php echo  get_permalink( $page_id );?>" method="post" >
					<?php 
					if (($category == constant("EVENT") || $category == constant("SERVICE")) && $is_user_avil) { ?>
					<div class="row">
						<div class="col-md-12 tableContainer">
							<table class="table eventTicketTable">
								<colgroup>
									<col width="40%">
									<col width="20%">
									<?php if ( $category != constant("SERVICE") ) { ?> <col width="20%"> <?php } ?>
									<col width="20%">
								</colgroup>
								<tr>
									<th>Type</th>
									<th class="text-right">Price</th>
									<?php if ( $category != constant("SERVICE") ) { ?> <th>Person</th> <?php } ?>
									<th>Quantity</th>
								</tr>
								<?php 
								$q = 1;
								foreach ($item_list as $item) {
									$disabled = !$product_details->status || !$item->status ? "disabled" : "";
								?>
								<tr <?php echo $disabled;?>>
									<td><?php echo $item->type;?></td>
									<td class="text-right"><span class="price"><?php echo number_format($item->price,2); ?></span></td>
									<?php if ( $category != constant("SERVICE") ) { ?> <td><span class="person"><?php echo $item->person;?></span></td> <?php } ?>
									<td>
									<?php 
									if($disabled){?>
									<div>Not avaliable</div>
									<?php }else {?>
										<input type="number" class="form-control inputField quantity" id="quantity<?php echo $q;?>" name="quantity[]" min="0" max="<?php echo $item->max?>" autocomplete="off">
									<?php }
									?>
										<input type="hidden" class="itemId" value="<?php echo $item->item_id;?>">
									</td>
								</tr>
								<?php
								$q++;
								}
								?>
							</table>
							<hr>
						</div>
					</div>
					<?php } ?>
					<?php if ($category == constant("VENUE") && $is_user_avil) { ?>
						<div class="row vanueSearchFrom">
							<div class="col-md-12 col-sm-12 col-xs-12 form-inline">
								<label for="searchAvailability" class="col-md-2 col-form-label">From Date</label>
								<input type="text" class="inputField edateFields " id="searchAvailability" name="searchAvailability" value="<?php echo $_POST["searchAvailability"] ? $_POST["searchAvailability"] : date("d-M-Y") ?>" required aria-label="Input group example" aria-describedby="btnGroupAddon">
								<div class="input-group-append btnGroupAddon datepickerIcon marginRight">
									<div class="input-group-text"><i class="fa fa-calendar"></i></div>
								</div>
								<input type="hidden" value="<?php echo $_POST["mmbrId"] ? $_POST["mmbrId"]: $user->user_login;?>" name="mmbrId">
								<input type="hidden" name="task" value="dtvanue">
								<button type="submit" class="btn btn-primary btn-sm ">Search</button>
							</div>
						</div>
						<div class="row avilabilityTableCont">
							<div class="col-md-12 venueHeaderDiv">
								<h2 class="h2 venue-heading"><?php echo $product_details->name;?></h2>
								<p class="venue-description"><?php echo $product_details->description;?></p>
							</div>
						</div>
						<div class="col-md-12">
							<table id="avilabilityPurchaseTable1" class="avilabilityPurchaseTable table table-striped table-responsive" tableFor="Gulshan Convension center 1">
								<tr>
									<th class="">Date</th>
									<th class="shiftM text-center" shift="M">Morning</th>
									<th class="shiftA text-center" shift="A">Afternoon</th>
									<th class="shiftE text-center" shift="E">Evening</th>
								</tr>
								<?php foreach ($item_list as $d =>$item) {
									$df = custom_date("d-M-Y", $d);
								?>
								<tr>
									<td class="dateName"><?php echo $df?></td>
									<?php 
									foreach ($item as $value) {
										if (isset($value->max) && $value->max == 0) {
											$t = " booked";
											$ttl= "Booked";
											$disabled = true;
										} else if(empty($value->max) || $value->max < 0) {
											$t = " notAvail";
											$ttl = "Not Available"; 
											$disabled = true;
										} else {
											$t = "Available";
											$disabled = false;
											$ttl = "Available"; 
										}
									?>
									<td class="shift<?php echo $value->shift;?>" shift="<?php echo $value->shift?>">
										<div class="form-check <?php echo $t;?>">
											<input class="form-check-input availCheckFields" type="checkbox" id="<?php echo $value->item_id?>"  name="<?php echo $value->item_id?>" price="<?php echo $value->price;?>" datefld="<?php echo $df?>" <?php echo $disabled ? "disabled" : ""?>>
											<label class="form-check-label" for="<?php echo $value->item_id?>" data-toggle='tooltip' data-placement='bottom' title="<?php echo $ttl;?>"></label>
										</div>
									</td>
									<?php 
									}
									?>
								</tr>
								<?php }?>

							</table>
						</div>
						<div class="row">
							<div class="col-md-11 col-sm-12 col-xm-12 pull-right">
								<table class="table venueSelectedShifts text-right">
									
								</table>
							</div>
						</div>
							
					<?php } ?>
					<?php 
					if ($category == constant("PAYMENT") && $is_user_avil) {
						if(!$memberStatus) {
							echo show_custom_message($user_details->status." member will not be able to make payments", "warning");
							return;
						}
						$is_fixed_amount = $product_details->payment_option == "Fixed" ? true : false;
						$paid_amount = get_due_development_fee ($product_details->product_id, $user_details->user_login);
						$due_amount = $product_details->payment_amount - $paid_amount;
						$hasDue = false;
						if ($due_amount >=0 && $is_fixed_amount) {
							$hasDue = true;
						}
						$fix_amount_desc = "$user_details->user_login/$product_details->product_id/0/0/0/$product_details->product_id-0:$due_amount:0:0";
						$fix_amount_payment = number_format($due_amount, 2);
						if (!$hasDue){
						?>
						<div class="row">
							<label for="makePaymentAmount" class="col-md-3 col-sm-3 col-xm-3 col-form-label">Amount</label>
							<div class="col-md-6 col-sm-5 col-xm-8 ">
								<input type="text" class="form-control-plaintext makePaymentAmount" id="makePaymentAmount"  name ="makePaymentAmount" min="1" value="<?php echo $is_fixed_amount ? $due_amount : ''?>" <?php echo $is_fixed_amount ? 'readonly' : '';?> required placeholder="Required" autocomplete="off">
							</div>
						</div>
						<?php }?>	
						<div class="row">
							<label for="comment" class="col-md-3 col-sm-3 col-xm-3  col-form-label">Comment</label>
							<div class="col-md-6 col-sm-5 col-xm-8 ">
								<textarea class="form-control inputField " id="comment" name="comment" rows="2" autocomplete="off"></textarea>
							</div>
						</div>
						<?php foreach ($item_list as $item) {?>
							<input type="hidden" class="item_id" value="<?php echo $item->item_id;?>">
						<?php }?>
					<?php } ?>
				</form>
				<?php 
				if ($category != constant("PAYMENT") && $is_user_avil) { ?>
				<hr>
				<div class="row">
					<div class="col-md-9 col-sm-9 col-xs-8 text-right">
						<span class="bold">Base Total</span>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-4 text-right">
						<span class="red bold" >TK <span class="baseAmount">00.00</span></span>
					</div>
				</div>
				<?php } ?>
				<?php if (($category == constant("SERVICE") || $category == constant("VENUE")) && $is_user_avil) { ?>
				<div class="row">
					<div class="col-md-9 text-right col-sm-9 col-xs-8">
						<span class="">Service Charge</span>
					</div>
					<div class="col-md-3 text-right col-sm-3 col-xs-4">
						<span class="" >TK <span class="serviceCharge">00.00</span></span>
						<input type="hidden" id="serviceChargePercent" value="<?php echo $product_details->service_charge;?>" />
					</div>
				</div>
				<div class="row borderBottom">
					<div class="col-md-9 text-right col-sm-9 col-xs-8">
						<span class="">VAT</span>
					</div>
					<div class="col-md-3 text-right col-sm-3 col-xs-4">
						<span class="" >TK <span class="vatAmount">00.00</span></span>
						<input type="hidden" id="vatAmountPercent" value="<?php echo $product_details->vat;?>" />
					</div>
				</div>
				<?php } ?>
				<?php if ($category == constant("EVENT") && $is_user_avil) { ?>
				<div class="row borderBottom promoDiv">
					<form class="form-inline pull-right promocodeform" action="<?php echo  get_permalink( $page_id );?>" id="promocodeform" method="post">
						<div class="form-group col-md-12 col-sm-12 col-xs-12 text-right">
							<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-8 col-xs-offset-0 promoInpDiv">
								<label for="promoCode" class="marginLeft">Promo Code</label>
								<input type="text" class="form-control" id="promoCode" name="promoCode" value="" required placeholder="Promo Code">
							</div>
							<input type="hidden" class="promocodepercent" value="" />
							<input type="hidden" class="afterpromocodeapplyvalue" value="00.00" />
							<input type="hidden" value="checkpromocode" name="action"/>
							<input type="hidden" value="<?php echo $user_details->user_login?>" name="mmbrId"/>
							<input type="hidden" value="<?php echo $productid?>" name="productid"/>
							<div class="col-md-2 col-sm-2 col-xs-4 promobtnDiv">
								<button type="submit" class="btn btn-primary btn-sm promoApplyBtn">
									<i class="fa fa-spinner fa-spin promoSpinner"> </i> Apply
								</button>
							</div>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12 text-right promomsg"></div>
					</form>
				</div>
				
				<div class="row totalPriceDiv">
					<div class="col-md-9 text-right col-sm-9 col-xs-8">
						<span>Discount</span>
					</div>
					<div class="col-md-3 text-right col-sm-3 col-xs-4">
						<span class="" >TK - <span class="discountAmount">00.00</span></span>
					</div>
				</div>
				<?php } ?>
				<?php if ($is_user_avil) {?>
				<div class="row">
					<?php if ($category == constant("PAYMENT")) { ?>
					<div class="col-md-3 col-sm-3 col-xs-5">
					<?php } else { ?>
					<div class="col-md-9 text-right col-sm-9 col-xs-8">
					<?php } ?>
						<p class="bold">Total Amount</p>
					</div>
					<?php if ($category == constant("PAYMENT")) { ?>
					<div class="col-md-6 col-sm-6 col-xs-6">
					<?php } else { ?>
					<div class="col-md-3 text-right col-sm-3 col-xs-4">
					<?php } ?>
					<?php if ($hasDue) {?>
						<p class="red bold" >TK <span class="totalAmount"><?php echo number_format($product_details->payment_amount, 2);?></span></p>
					<?php } else {?>
						<p class="red bold" >TK <span class="totalAmount"><?php echo $is_fixed_amount ? $fix_amount_payment : '00.00'?></span></p>
						<input type="hidden" id="totalAmount" name="totalAmount" />
					<?php }?>	
					</div>
				</div>
					<?php if ($hasDue) {?>
					<div class="row">
						<div class="col-md-3 col-sm-3 col-xs-5">
							<p class="bold">Paid Amount</p>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<p class="red bold">TK <span class="totalAmount"><?php echo number_format($paid_amount, 2);?></span></p>
							<input type="hidden" id="totalAmount" name="totalAmount">
						</div>
					</div>
					<div class="row">
						<div class="col-md-3 col-sm-3 col-xs-5">
							<p class="bold">Due Amount</p>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<p class="red bold">TK <span class="totalAmount"><?php echo number_format($due_amount, 2);?></span></p>
							<input type="hidden" id="totalAmount" name="totalAmount">
						</div>
					</div>
					<?php }?>
				<?php }?>
				<?php 
				if ($is_user_avil) {
					if ( !in_array( 'subscriber', (array) $user->roles ) && is_user_logged_in()) { ?>
					<div class="row">
						<?php if ($category == constant("PAYMENT")) { ?>
							<div class="col-md-9  col-sm-8  col-xm-10 ">
						<?php } else { ?>
							<div class="col-md-8 col-md-offset-4 col-sm-8 col-sm-offset-4 col-xm-10 col-xm-offset-2 ">
						<?php } ?>
							<form class="form-horizontal receivePaymentForm" action="<?php echo  get_permalink( $page_id );?>" id="receivePaymentForm" method="post" autocomplete="off" onsubmit="return validateForm()">
								<?php if ($category == constant("VENUE")) {?>
								<div class="row">
									<div class="col-md-8 text-right col-sm-9 col-xs-8">
										<label class="tempHold" for="tempHold">Temporary Hold</label>
									</div>
									<div class="col-md-3 text-right col-sm-3 col-xs-4 pull-right">
										<input type="checkbox" id="tempHold" name="tempHold" autocomplete="off">
									</div>
								</div>
								<?php } ?>
								<input type="hidden" class="form-control inputField" id="paymentAmount" name="paymentAmount" value="<?php echo number_format($result["due_amount"], 2);?>" readonly>
								<div class="row tptroggle">
									<label for="paymentType" class="col-md-4 col-sm-5 col-form-label">Payment Type</label>
									<div class="col-md-8 col-sm-7">
										<select class="form-control inputField" id="paymentType" name="paymentType" required >
											<option value="">Required</option>
												<?php foreach (get_paymnet_type_list() as $value) {?>
												<option value="<?php echo $value->id?>"><?php echo $value->name?></option>
												<?php }?>
										</select>
									</div>
								</div>
								<div class="row tptroggle">
									<label for="paymentRef" class="col-md-4  col-sm-5 col-form-label">Payment Reference</label>
									<div class="col-md-8  col-sm-7">
										<input type="text" class="form-control inputField" id="paymentRef" name="paymentRef">
									</div>
								</div>
								<div class="row">
									<label for="paymentRef" class="col-md-4  col-sm-5 col-form-label">Password</label>
									<div class="col-md-8 col-sm-7">
										<input type="password" class="form-control inputField" id="password" name="password" required autocomplete="off">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<button class="receivePaymentBtn pull-right" type="submit" <?php echo !$is_fixed_amount || $due_amount<= 0 ? 'disabled' :''?>>Submit</button>
										<input type="hidden" name="amount" id="finaltotalAmount" value="<?php echo $is_fixed_amount ? $due_amount : ''?>"/>
										<input type="hidden" name="description" id="description" value="<?php echo $is_fixed_amount ? $fix_amount_desc : ''?>">
										<input type="hidden" name="userLogin" id="user_login" value="<?php echo $user_details->user_login?>">
										<input type="hidden" name="receiveType" value="<?php echo constant('PAYMENT_RECEIVED')?>">
										<input type="hidden" name="category" value="<?php echo $category;?>">
										<input type="hidden" name="task" value="receivePayment">
										<input type="hidden" name="productid" id="productid" value="<?php echo $productid?>">
										<input type="hidden" name="comment" class="comment" value="">
										<input type="hidden" name="searchAvailability"  value="<?php echo $_POST["searchAvailability"] ? $_POST["searchAvailability"] : date("d-M-Y") ?>">

									</div>
								</div>
							</form>
						</div>
					</div>
				<?php } else { ?>
					<div class="row totalPriceDiv paymentGayeWayDiv">
						<?php if ($category == constant("PAYMENT")) { ?>
							<div class="col-md-5 col-sm-9 col-xs-7 selectPayGatWay">
						<?php } else { ?>
							<div class="col-md-9  col-sm-9 col-xs-7 text-md-right text-right">
						<?php } ?>
							<p class="slctPymtTtl">Select payment gateway</p>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-5 col-xs-offset-0">
							<form action="<?php echo get_permalink(constant("PAYMENT_PROCESS"))?>" method="post" autocomplete="off" onsubmit="return validateForm()">
								<div class="row">	
	        						<div class="<?php $product_details->emi_option ? 'pull-right': 'pull-left'?>">
	        							<?php if ($product_details->emi_option) {?>
        								<div class="form-radio radio">
	        								<input class="form-check-input radio__input" type="radio" name="EMI" id="withEMI" value="1" checked="checked">
	        								<label class="form-check-label radio__label" for="withEMI">
	        									With EMI
	        								</label>
	        							</div>
	        							<div class="form-radio radio">
	        								<input class="form-check-input radio__input " type="radio" name="EMI" id="withoutEMI" value="0">
	        								<label class="form-check-label radio__label" for="withoutEMI">
	        									Without EMI
	        								</label>
	        							</div>
	        							<div class="radio_emi">
		        							<div class="form-radio radio">
		        								<input class="form-check-input radio__input" type="radio" name="paymentType" id="eblpayment" value="EBL">
		        								<label class="form-check-label radio__label" for="eblpayment">
		        									Visa / Master
		        								</label>
		        							</div>
		        							<div class="form-radio radio">
		        								<input class="form-check-input radio__input" type="radio" name="paymentType" id="sslpayment" value="SSL" checked="checked">
		        								<label class="form-check-label radio__label" for="sslpayment">
		        									Others
		        								</label>
		        							</div>
	        							</div>
	        							<?php } else {?>
        								<div class="form-radio radio">
	        								<input class="form-check-input radio__input" type="radio" name="paymentType" id="eblpayment" value="EBL">
	        								<label class="form-check-label radio__label" for="eblpayment">
	        									Visa / Master
	        								</label>
	        							</div>
	        							<div class="form-radio radio">
	        								<input class="form-check-input radio__input" type="radio" name="paymentType" id="sslpayment" value="SSL">
	        								<label class="form-check-label radio__label" for="sslpayment">
	        									Others
	        								</label>
	        							</div>
	        							<?php } ?>
	        							<button class="processPaymentBtn" type="submit" <?php echo !$is_fixed_amount|| $due_amount<= 0 ? 'disabled' :''?>>Pay Now</button>
										<input type="hidden" name="order[amount]" id="finaltotalAmount" value="<?php echo $is_fixed_amount ? $due_amount : ''?>"/>
										<input type="hidden" name="order[description]" id="description" value="<?php echo $is_fixed_amount ? $fix_amount_desc : ''?>"/>
										<input type="hidden" name="receiveType" value="<?php echo constant('PAYMENT_ONLINE')?>">
										<input type="hidden" name="category" value="<?php echo $category;?>">
										<input type="hidden" name="productid" id="productid" value="<?php echo $productid?>">
										<input type="hidden" name="user_login" id="user_login" value="<?php echo $user_details->user_login?>">
										<input type="hidden" name="comment" class="comment" value="">
	        						</div>
	        					</div>
	        				</form>
						</div>
					</div>
				<?php 
					}
				} ?>
			</div>
		</div>
	</div>
	
<?php
return ob_get_clean();
}
