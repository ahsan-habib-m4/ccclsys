jQuery(document).ready(function($) {
	
	$(".quantity").on("change", function() {
		calculateEventPrice();
	});
	
	$("#promocodeform,#searchMemberForm").validate();
    $("#promocodeform").on("submit", function(e) {
    	if($("#promocodeform").valid()){
    		$(".promoSpinner").css("display","inline-block");
    		$.ajax({
    			type: 'POST',
    			dataType: 'json',
    			url: ajax_object.ajaxurl,
    			data:$(this).serialize(), 
    			success: function(data){
    				if(data.status == "success") {
    					$(".promocodepercent").val(data.products.discount);
    					calculateEventPrice();
    					$(".promomsg").removeClass("error");
    					$(".promomsg").empty().text(data.message).show().delay(5000).fadeOut();
    				}else {
    					$(".promomsg").addClass("error");
    					$(".promomsg").empty().text(data.message).show().delay(5000).fadeOut();
    				}
    				$(".promoSpinner").css("display","none");
    			}
    		});
    	}	
    	e.preventDefault();
    })
    
    $(".availCheckFields").on("change",function() {
    	 if($(this).prop("checked") == true){
    		 var tableFor = $(this).parent("div").parent("td").parent("tr").parent("tbody").parent("table").attr("tableFor");
             var shift = $(this).parent("div").parent("td").attr("shift");
             if(shift == "A") shift = "Aftrenoon";
             if(shift == "M") shift = "Morning";
             if(shift == "E") shift = "Evening";
             var row = "<tr selectedShift='"+$(this).attr("id")+"'>" +
             		"<td><a href='#' class='removeRowSelectedSlot' selectedShift='"+$(this).attr("id")+"'><i class='fa fa-minus'></i></a></td>" +
             		"<td>"+$(this).attr("datefld")+"</td><td>"+shift+"</td>" +
             		"<td>"+ tableFor +"</td>"+
             		"<td>TK <span class='price'>"+numberWithCommas($(this).attr("price"))+"</span><input type='hidden' class='itemid' value='"+$(this).attr("id")+"' /></td></tr>";
				var wrapper = $('.venueSelectedShifts');
				$(wrapper).append(row);
				calculatePriceForVenue();
         } else if($(this).prop("checked") == false){
        	var element = $(".venueSelectedShifts tbody").children("tr[selectedShift='"+$(this).attr("id")+"']");
     		element.remove();
     		calculatePriceForVenue();
         }
    });
    
    $(".removeRowSelectedSlot").live("click", function(e) {
    	e.preventDefault();
    	var selectedShift = $(this).parent("td").parent("tr").attr("selectedShift");
    	$("#"+selectedShift).prop("checked", false);
    	$(this).parent("td").parent("tr").remove();
    	calculatePriceForVenue();
    });
    
    $(".makePaymentAmount").change(function() {
    	var payment = $(this).val();
    	if (!validateForm()) {
    		$(".totalAmount").text("00.00");
	    	$("#totalAmount").val("00.00");
	    	$(".receivePaymentBtn, .processPaymentBtn").attr("disabled","disabled");
	    	jQuery("#description").empty();
    	} else {
	    	$(".totalAmount").text(numberWithCommas(parseFloat(payment).toFixed(2)));
	    	$("#totalAmount").val(parseFloat(payment).toFixed(2));
	    	$(".receivePaymentBtn, .processPaymentBtn").removeAttr("disabled");
	    	var productid = jQuery("#productid").val();
	    	var user_login = jQuery("#user_login").val();
	    	$("#finaltotalAmount").val(parseFloat(payment).toFixed(2));
	    	var description = jQuery(".item_id").val()+':'+ payment + ':'+'1'+':'+'1'
	     	jQuery("#description").val(user_login + "/" + productid + "/0/0/0/" + description);
    	}
    });

    $("#comment").keyup(function(){
    	$(".comment").val($(this).val());
    });

    $(document).on('shown.bs.tooltip', function (e) {
        setTimeout(function () {
          $(e.target).tooltip('hide');
        }, 3000);
     });
    
    $('input[name="tempHold"]').click(function () {
    	$('.tptroggle').toggle(!this.checked);
    });
    
    $('input[type=radio][name=EMI]').change(function() {
		if($(this).val()==1) {
			$("#sslpayment").prop("checked", true);		}else {
			$("#eblpayment").prop("checked", true);
		}		$(".radio_emi").fadeToggle();
	});
});


function calculatePriceForVenue() {
	var baseTotal = 0;
	var totalAmount = 0;
	var discount = 0 ;
	var serviceCharge = 0 ;
	var vatAmount = 0 ;
	var description = "";
	
	jQuery('.venueSelectedShifts tbody > tr').each(function() {
		let price = jQuery(this).find(".price").text();
		price = price.toString().replace(",", "");
		let itemId = jQuery(this).find(".itemid").val();
		baseTotal = baseTotal + parseFloat(price) ;
			var tm = itemId +":"+ price+":1:1";
			description += description ? "," + tm : tm;
	});
	
	var productid = jQuery("#productid").val();
	var user_login = jQuery("#user_login").val();
	var vat = jQuery("#vatAmountPercent").val() ? jQuery("#vatAmountPercent").val() : 0;
	var service_charge = jQuery("#serviceChargePercent").val() ? jQuery("#serviceChargePercent").val() : 0;
	var promocodepercent = jQuery(".promocodepercent").val() ? jQuery(".promocodepercent").val() : 0;
	
	jQuery(".baseAmount").text(baseTotal.toFixed(2));
	jQuery("#description").val(user_login + "/" + productid + "/"+ vat + "/"+ service_charge + "/"+ promocodepercent + "/" + description);
	
	
	jQuery(".baseAmount").text(numberWithCommas(baseTotal.toFixed(2)));
	if ( jQuery( ".discountAmount" ).length ) discount = jQuery(".discountAmount").text();
	if ( jQuery( ".serviceCharge" ).length ) { 
		let serviceChargeParcent = jQuery("#serviceChargePercent").val();
		serviceCharge = ( serviceChargeParcent / 100 ) * baseTotal ;
		jQuery(".serviceCharge").text(numberWithCommas(serviceCharge.toFixed(2))); 
	}
	if ( jQuery( ".vatAmount" ).length ) { 
		let vatAmountPercent = jQuery("#vatAmountPercent").val();
		vatAmount = ( vatAmountPercent / 100 ) * baseTotal ;
		jQuery(".vatAmount").text(numberWithCommas(vatAmount.toFixed(2)));
	}
	totalAmount = parseFloat(baseTotal) - parseFloat(discount) + parseFloat(serviceCharge) + parseFloat(vatAmount);
	jQuery(".totalAmount").text(numberWithCommas(totalAmount.toFixed(2)));
	jQuery("#totalAmount").val(totalAmount.toFixed(2));
	jQuery("#finaltotalAmount").val(totalAmount.toFixed(2));
	if ( jQuery("#totalAmount").val() > 0 ) {
		jQuery(".receivePaymentBtn, .processPaymentBtn").removeAttr("disabled");
	} else {
		jQuery(".receivePaymentBtn, .processPaymentBtn").attr("disabled","disabled");
	}
}

function calculateEventPrice() {
	var baseTotal = 0;
	var totalAmount = 0;
	var discount = 0 ;
	var serviceCharge = 0 ;
	var vatAmount = 0 ;
	var description = "";
	jQuery('.eventTicketTable tbody > tr').not(":first").each(function() {
		let price = jQuery(this).find(".price").text();
		price = price.toString().replace(",", "");
		let person = jQuery(this).find(".person").text();
		let quantity = jQuery(this).find(".quantity").val();
		let itemId = jQuery(this).find(".itemId").val();
		if (!quantity) {
			quantity = 0;
		}
		if (!person) {
			person = 1;
		}
		baseTotal += ( price  * quantity );
		if (quantity > 0) {
			var tm = itemId +":"+ price +":"+ person + ":" + quantity; 
			description += description ? "," + tm : tm;
		}
	});
	
	var productid = jQuery("#productid").val();
	var user_login = jQuery("#user_login").val();
	var vat = jQuery("#vatAmountPercent").val() ? jQuery("#vatAmountPercent").val() : 0;
	var service_charge = jQuery("#serviceChargePercent").val() ? jQuery("#serviceChargePercent").val() : 0;
	var promocodepercent = jQuery(".promocodepercent").val() ? jQuery(".promocodepercent").val() : 0;
	jQuery(".baseAmount").text(numberWithCommas(baseTotal.toFixed(2)));
	jQuery("#description").val(user_login + "/" + productid + "/"+ vat + "/"+ service_charge + "/"+ promocodepercent + "/" +description);
	applyPromoCode();
	if ( jQuery( ".discountAmount" ).length ) discount = jQuery(".afterpromocodeapplyvalue").val();
	if ( jQuery( ".serviceCharge" ).length ) { 
		let serviceChargeParcent = jQuery("#serviceChargePercent").val();
		serviceCharge = ( serviceChargeParcent / 100 ) * baseTotal ;
		jQuery(".serviceCharge").text(numberWithCommas(serviceCharge.toFixed(2))); 
	}
	if ( jQuery( ".vatAmount" ).length ) {
		let vatAmountPercent = jQuery("#vatAmountPercent").val();
		vatAmount = ( vatAmountPercent / 100 ) * baseTotal ;
		jQuery(".vatAmount").text(numberWithCommas(vatAmount.toFixed(2)));
	}
	totalAmount = parseFloat(baseTotal) - parseFloat(discount) + parseFloat(serviceCharge) + parseFloat(vatAmount);
	jQuery(".totalAmount").text(numberWithCommas(totalAmount.toFixed(2)));
	jQuery("#totalAmount").val(totalAmount.toFixed(2));
	jQuery("#finaltotalAmount").val(totalAmount.toFixed(2));

	if ( jQuery("#totalAmount").val() > 0 ) {
		jQuery(".receivePaymentBtn, .processPaymentBtn").removeAttr("disabled");
	} else {
		jQuery(".receivePaymentBtn, .processPaymentBtn").attr("disabled","disabled");
	}
}

function applyPromoCode() {
	let promopercent = jQuery(".promocodepercent").val();
	if (promopercent == "") return;
	let baseAmount = jQuery(".baseAmount").text();
	baseAmount = baseAmount.replace(",", "");
	let discountAmount = ( parseFloat(promopercent) / 100 ) * parseFloat(baseAmount) 
	jQuery(".discountAmount").text(discountAmount.toFixed(2));
	jQuery(".afterpromocodeapplyvalue").val(discountAmount.toFixed(2));
}

function validateForm () {
	if (jQuery("#main-purchase-form").valid()) {
		return true;
	} else {
		return false;
	};
}
