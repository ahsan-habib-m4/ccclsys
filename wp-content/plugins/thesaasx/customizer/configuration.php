<?php

function thesaasx_customizer_panel_configuration($wp_customize) {


	$link_google_api = 'https://developers.google.com/maps/documentation/javascript/get-api-key';

	$wp_customize->add_section( 'site_config_settings', array(
		'title' => esc_html__( 'Site Configuration', 'thesaasx' ),
		'priority' => 30,
	));

	// API Key
	$wp_customize->add_setting( "google_api_key", array(
		'sanitize_callback' => 'esc_js',
	));

	$wp_customize->add_control( 'google_api_key', array(
		'label' => esc_html__( "Google Map API key", 'thesaasx' ),
		'description' => '<a href="'. esc_url( $link_google_api ) .'" target="_blank">'. esc_html__( 'Get API Key', 'thesaasx' ) .'</a>',
		'section' => 'site_config_settings',
		'type' => 'text',
	));


	// Analytics ID
	$wp_customize->add_setting( "google_analytics_id", array(
		'sanitize_callback' => 'esc_js',
	));

	$wp_customize->add_control( 'google_analytics_id', array(
		'label' => esc_html__( "Google Analytics Tracking ID", 'thesaasx' ),
		'description' => esc_html__( 'Your tracking ID would be a value in this format: UA-12345678-9', 'thesaasx' ),
		'section' => 'site_config_settings',
		'type' => 'text',
	));


	// reCAPTCHA v3
	$wp_customize->add_setting( "google_recaptcha3_public", array(
		'sanitize_callback' => 'esc_js',
	));

	$wp_customize->add_control( 'google_recaptcha3_public', array(
		'label' => esc_html__( "Google reCAPTCHA v3 — Site Key", 'thesaasx' ),
		'description' => esc_html__( 'Register reCAPTCHA v3 keys in https://g.co/recaptcha/v3 to use it in contact forms.', 'thesaasx' ),
		'section' => 'site_config_settings',
		'type' => 'text',
	));


	$wp_customize->add_setting( "google_recaptcha3_secret", array(
		'sanitize_callback' => 'esc_js',
	));

	$wp_customize->add_control( 'google_recaptcha3_secret', array(
		'label' => esc_html__( "Google reCAPTCHA v3 — Secret Key", 'thesaasx' ),
		'section' => 'site_config_settings',
		'type' => 'text',
	));


	// Hide scrolltop button
	//
	$wp_customize->add_setting( "hide_scrolltop", [
		'default'           => true,
		'sanitize_callback' => 'thesaasx_sanitize_boolean',
	]);

	$wp_customize->add_control( 'hide_scrolltop', array(
		'label' => esc_html__( "Hide scroll to top button", 'thesaasx' ),
		'section' => 'site_config_settings',
		'type' => 'checkbox',
	));

}

