<?php

function thesaasx_customizer_panel_additional_js($wp_customize) {


		$wp_customize->add_section( 'additional_script', array(
			'title' => esc_html__( 'Additional Javascript', 'thesaasx' ),
			'priority' => 210,
		));

		// Textarea
		//
		$wp_customize->add_setting( 'additional_script' );
		$wp_customize->add_control( 'additional_script', array(
			'label' => '',
			'section' => 'additional_script',
			'type' => 'textarea',
			'description' => esc_html__( 'This code will add to bottom of each page inside a <script> tag.', 'thesaasx' ),
		));

}

