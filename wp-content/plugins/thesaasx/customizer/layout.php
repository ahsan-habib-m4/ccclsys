<?php

function thesaasx_customizer_panel_layout($wp_customize) {

	$navbars = thesaasx_customizer_cpt_choices('thesaasx_navbar');
	$headers = thesaasx_customizer_cpt_choices('thesaasx_header');
	$footers = thesaasx_customizer_cpt_choices('thesaasx_footer');


	$wp_customize->add_panel( 'panel_layout', array(
		'title' => esc_html__( 'Layout', 'thesaasx' ),
		'description' => esc_html__( 'Set the overall layout of your website', 'thesaasx' ),
		'priority' => 31,
	));


	/*
	|--------------------------------------------------------------------------
	| Section —— Navbar
	|--------------------------------------------------------------------------
	*/
	$wp_customize->add_section( 'section_layout_navbar', array(
		'title' => esc_html__( 'Navbar', 'thesaasx' ),
		'panel' => 'panel_layout'
	));


	// Global
	$wp_customize->add_setting( 'navbar_global', array(
		'default'           => '0',
		'sanitize_callback' => 'thesaasx_sanitize_int',
	));

	$wp_customize->add_control( 'navbar_global', array(
		'label'       => esc_html__( 'Global Navbar', 'thesaasx' ),
		'description' => esc_html__( 'This navbar displays in all pages, unless another navbar has specified below or from editor.', 'thesaasx' ),
		'type'        => 'select',
		'settings'    => 'navbar_global',
		'section'     => 'section_layout_navbar',
		'choices'     => $navbars,
	));


	// Blog archive
	$wp_customize->add_setting( 'navbar_blog_archive', array(
		'default'           => '0',
		'sanitize_callback' => 'thesaasx_sanitize_int',
	));

	$wp_customize->add_control( 'navbar_blog_archive', array(
		'label'       => esc_html__( 'Blog Archive Navbar', 'thesaasx' ),
		'description' => esc_html__( 'The navbar to display in all blog archive pages, unless another one specified from editor.', 'thesaasx' ),
		'type'        => 'select',
		'settings'    => 'navbar_blog_archive',
		'section'     => 'section_layout_navbar',
		'choices'     => $navbars,
	));


	// Blog post
	$wp_customize->add_setting( 'navbar_blog_post', array(
		'default'           => '0',
		'sanitize_callback' => 'thesaasx_sanitize_int',
	));

	$wp_customize->add_control( 'navbar_blog_post', array(
		'label'       => esc_html__( 'Blog Post Navbar', 'thesaasx' ),
		'description' => esc_html__( 'The navbar to display in all blog posts, unless another one specified from editor.', 'thesaasx' ),
		'type'        => 'select',
		'settings'    => 'navbar_blog_post',
		'section'     => 'section_layout_navbar',
		'choices'     => $navbars,
	));


	// Job post
	$wp_customize->add_setting( 'navbar_job_post', array(
		'default'           => '0',
		'sanitize_callback' => 'thesaasx_sanitize_int',
	));

	$wp_customize->add_control( 'navbar_job_post', array(
		'label'       => esc_html__( 'Jobs Post Navbar', 'thesaasx' ),
		'description' => esc_html__( 'The navbar to display in all job posts, unless another one specified from editor.', 'thesaasx' ),
		'type'        => 'select',
		'settings'    => 'navbar_job_post',
		'section'     => 'section_layout_navbar',
		'choices'     => $navbars,
	));


	// Portfolio post
	$wp_customize->add_setting( 'navbar_portfolio_post', array(
		'default'           => '0',
		'sanitize_callback' => 'thesaasx_sanitize_int',
	));

	$wp_customize->add_control( 'navbar_portfolio_post', array(
		'label'       => esc_html__( 'Portfolio Post Navbar', 'thesaasx' ),
		'description' => esc_html__( 'The navbar to display in all portfolio posts, unless another one specified from editor.', 'thesaasx' ),
		'type'        => 'select',
		'settings'    => 'navbar_portfolio_post',
		'section'     => 'section_layout_navbar',
		'choices'     => $navbars,
	));


	if ( class_exists( 'WooCommerce' ) ) {

		// Shop page
		$wp_customize->add_setting( 'navbar_shop_archive', array(
			'default'           => '0',
			'sanitize_callback' => 'thesaasx_sanitize_int',
		));

		$wp_customize->add_control( 'navbar_shop_archive', array(
			'label'       => esc_html__( 'Shop Page Navbar', 'thesaasx' ),
			'description' => esc_html__( 'The navbar to display in the shop (WooCommerce) page, unless another one specified from editor.', 'thesaasx' ),
			'type'        => 'select',
			'settings'    => 'navbar_shop_archive',
			'section'     => 'section_layout_navbar',
			'choices'     => $navbars,
		));


		// Product post
		$wp_customize->add_setting( 'navbar_shop_post', array(
			'default'           => '0',
			'sanitize_callback' => 'thesaasx_sanitize_int',
		));

		$wp_customize->add_control( 'navbar_shop_post', array(
			'label'       => esc_html__( 'Shop Post Navbar', 'thesaasx' ),
			'description' => esc_html__( 'The navbar to display in all shop (WooCommerce) posts, unless another one specified from editor.', 'thesaasx' ),
			'type'        => 'select',
			'settings'    => 'navbar_shop_post',
			'section'     => 'section_layout_navbar',
			'choices'     => $navbars,
		));

	}



	/*
	|--------------------------------------------------------------------------
	| Section —— Header
	|--------------------------------------------------------------------------
	*/
	$wp_customize->add_section( 'section_layout_header', array(
		'title' => esc_html__( 'Header', 'thesaasx' ),
		'panel' => 'panel_layout'
	));


	// Global
	$wp_customize->add_setting( 'header_global', array(
		'default'           => '0',
		'sanitize_callback' => 'thesaasx_sanitize_int',
	));

	$wp_customize->add_control( 'header_global', array(
		'label'       => esc_html__( 'Global Header', 'thesaasx' ),
		'description' => esc_html__( 'This header displays in all pages, unless another header has specified below or from editor.', 'thesaasx' ),
		'type'        => 'select',
		'settings'    => 'header_global',
		'section'     => 'section_layout_header',
		'choices'     => $headers,
	));


	// Blog archive
	$wp_customize->add_setting( 'header_blog_archive', array(
		'default'           => '0',
		'sanitize_callback' => 'thesaasx_sanitize_int',
	));

	$wp_customize->add_control( 'header_blog_archive', array(
		'label'       => esc_html__( 'Blog Archive Header', 'thesaasx' ),
		'description' => esc_html__( 'The header to display in all blog archive pages, unless another one specified from editor.', 'thesaasx' ),
		'type'        => 'select',
		'settings'    => 'header_blog_archive',
		'section'     => 'section_layout_header',
		'choices'     => $headers,
	));


	// Blog post
	$wp_customize->add_setting( 'header_blog_post', array(
		'default'           => '0',
		'sanitize_callback' => 'thesaasx_sanitize_int',
	));

	$wp_customize->add_control( 'header_blog_post', array(
		'label'       => esc_html__( 'Blog Post Header', 'thesaasx' ),
		'description' => esc_html__( 'The header to display in all blog posts, unless another one specified from editor.', 'thesaasx' ),
		'type'        => 'select',
		'settings'    => 'header_blog_post',
		'section'     => 'section_layout_header',
		'choices'     => $headers,
	));


	// Jobs post
	$wp_customize->add_setting( 'header_job_post', array(
		'default'           => '0',
		'sanitize_callback' => 'thesaasx_sanitize_int',
	));

	$wp_customize->add_control( 'header_job_post', array(
		'label'       => esc_html__( 'Jobs Post Header', 'thesaasx' ),
		'description' => esc_html__( 'The header to display in all job posts, unless another one specified from editor.', 'thesaasx' ),
		'type'        => 'select',
		'settings'    => 'header_job_post',
		'section'     => 'section_layout_header',
		'choices'     => $headers,
	));


	// Jobs post
	$wp_customize->add_setting( 'header_portfolio_post', array(
		'default'           => '0',
		'sanitize_callback' => 'thesaasx_sanitize_int',
	));

	$wp_customize->add_control( 'header_portfolio_post', array(
		'label'       => esc_html__( 'Portfolio Post Header', 'thesaasx' ),
		'description' => esc_html__( 'The header to display in all portfolio posts, unless another one specified from editor.', 'thesaasx' ),
		'type'        => 'select',
		'settings'    => 'header_portfolio_post',
		'section'     => 'section_layout_header',
		'choices'     => $headers,
	));



	if ( class_exists( 'WooCommerce' ) ) {

		// Shop page
		$wp_customize->add_setting( 'header_shop_archive', array(
			'default'           => '0',
			'sanitize_callback' => 'thesaasx_sanitize_int',
		));

		$wp_customize->add_control( 'header_shop_archive', array(
			'label'       => esc_html__( 'Shop Page Header', 'thesaasx' ),
			'description' => esc_html__( 'The header to display in the shop (WooCommerce) page, unless another one specified from editor.', 'thesaasx' ),
			'type'        => 'select',
			'settings'    => 'header_shop_archive',
			'section'     => 'section_layout_header',
			'choices'     => $headers,
		));


		// Shop post
		$wp_customize->add_setting( 'header_shop_post', array(
			'default'           => '0',
			'sanitize_callback' => 'thesaasx_sanitize_int',
		));

		$wp_customize->add_control( 'header_shop_post', array(
			'label'       => esc_html__( 'Shop Post Header', 'thesaasx' ),
			'description' => esc_html__( 'The header to display in all shop posts, unless another one specified from editor.', 'thesaasx' ),
			'type'        => 'select',
			'settings'    => 'header_shop_post',
			'section'     => 'section_layout_header',
			'choices'     => $headers,
		));

	}


	/*
	|--------------------------------------------------------------------------
	| Section —— Footer
	|--------------------------------------------------------------------------
	*/
	$wp_customize->add_section( 'section_layout_footer', array(
		'title' => esc_html__( 'Footer', 'thesaasx' ),
		'panel' => 'panel_layout'
	));


	// Global
	$wp_customize->add_setting( 'footer_global', array(
		'default'           => '0',
		'sanitize_callback' => 'thesaasx_sanitize_int',
	));

	$wp_customize->add_control( 'footer_global', array(
		'label'       => esc_html__( 'Global Footer', 'thesaasx' ),
		'description' => esc_html__( 'This footer displays in all pages, unless another footer has specified below or from editor.', 'thesaasx' ),
		'type'        => 'select',
		'settings'    => 'footer_global',
		'section'     => 'section_layout_footer',
		'choices'     => $footers,
	));


	// Blog archive
	$wp_customize->add_setting( 'footer_blog_archive', array(
		'default'           => '0',
		'sanitize_callback' => 'thesaasx_sanitize_int',
	));

	$wp_customize->add_control( 'footer_blog_archive', array(
		'label'       => esc_html__( 'Blog Archive Footer', 'thesaasx' ),
		'description' => esc_html__( 'The footer to display in all blog archive pages, unless another one specified from editor.', 'thesaasx' ),
		'type'        => 'select',
		'settings'    => 'footer_blog_archive',
		'section'     => 'section_layout_footer',
		'choices'     => $footers,
	));


	// Blog post
	$wp_customize->add_setting( 'footer_blog_post', array(
		'default'           => '0',
		'sanitize_callback' => 'thesaasx_sanitize_int',
	));

	$wp_customize->add_control( 'footer_blog_post', array(
		'label'       => esc_html__( 'Blog Post Footer', 'thesaasx' ),
		'description' => esc_html__( 'The footer to display in all blog posts, unless another one specified from editor.', 'thesaasx' ),
		'type'        => 'select',
		'settings'    => 'footer_blog_post',
		'section'     => 'section_layout_footer',
		'choices'     => $footers,
	));


	// Jobs post
	$wp_customize->add_setting( 'footer_job_post', array(
		'default'           => '0',
		'sanitize_callback' => 'thesaasx_sanitize_int',
	));

	$wp_customize->add_control( 'footer_job_post', array(
		'label'       => esc_html__( 'Jobs Post Footer', 'thesaasx' ),
		'description' => esc_html__( 'The footer to display in all job posts, unless another one specified from editor.', 'thesaasx' ),
		'type'        => 'select',
		'settings'    => 'footer_job_post',
		'section'     => 'section_layout_footer',
		'choices'     => $footers,
	));


	// Portfolio post
	$wp_customize->add_setting( 'footer_portfolio_post', array(
		'default'           => '0',
		'sanitize_callback' => 'thesaasx_sanitize_int',
	));

	$wp_customize->add_control( 'footer_portfolio_post', array(
		'label'       => esc_html__( 'Portfolio Post Footer', 'thesaasx' ),
		'description' => esc_html__( 'The footer to display in all portfolio posts, unless another one specified from editor.', 'thesaasx' ),
		'type'        => 'select',
		'settings'    => 'footer_portfolio_post',
		'section'     => 'section_layout_footer',
		'choices'     => $footers,
	));


	if ( class_exists( 'WooCommerce' ) ) {

		// Shop page
		$wp_customize->add_setting( 'footer_shop_archive', array(
			'default'           => '0',
			'sanitize_callback' => 'thesaasx_sanitize_int',
		));

		$wp_customize->add_control( 'footer_shop_archive', array(
			'label'       => esc_html__( 'Shop Page Footer', 'thesaasx' ),
			'description' => esc_html__( 'The footer to display in the shop (WooCommerce) pages, unless another one specified from editor.', 'thesaasx' ),
			'type'        => 'select',
			'settings'    => 'footer_shop_archive',
			'section'     => 'section_layout_footer',
			'choices'     => $footers,
		));


		// Shop post
		$wp_customize->add_setting( 'footer_shop_post', array(
			'default'           => '0',
			'sanitize_callback' => 'thesaasx_sanitize_int',
		));

		$wp_customize->add_control( 'footer_shop_post', array(
			'label'       => esc_html__( 'Shop Post Footer', 'thesaasx' ),
			'description' => esc_html__( 'The footer to display in all shop posts, unless another one specified from editor.', 'thesaasx' ),
			'type'        => 'select',
			'settings'    => 'footer_shop_post',
			'section'     => 'section_layout_footer',
			'choices'     => $footers,
		));

	}

}



/**
 * Array of choices to be use in select control
 */
function thesaasx_customizer_cpt_choices( $cpt ) {
	$choices = array();
	$choices[-1] = esc_html__( 'None', 'thesaasx' );
	$posts = get_posts([
		'numberposts' => -1,
		'post_type'   => $cpt,
		'orderby'     => 'ID',
		'order'       => 'asc',
	]);
	foreach($posts as $post) {
		$choices[ $post->ID ] = $post->post_title;
	}

	return $choices;
}
