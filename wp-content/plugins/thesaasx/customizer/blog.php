<?php

function thesaasx_customizer_panel_blog($wp_customize) {



	$wp_customize->add_panel( 'panel_blog', array(
		'title' => esc_html__( 'Blog', 'thesaasx' ),
		'description' => esc_html__( 'All the configurations regard blog and posts.', 'thesaasx' ),
		'priority' => 31,
	));


	/*
	|--------------------------------------------------------------------------
	| Section —— Archive
	|--------------------------------------------------------------------------
	*/
	$wp_customize->add_section( 'section_blog_archive', array(
		'title' => esc_html__( 'Archive', 'thesaasx' ),
		'panel' => 'panel_blog'
	));


	// Archive Type
	//
	$wp_customize->add_setting( 'blog_archive_type', array(
		'default'           => 'grid',
		'sanitize_callback' => 'thesaasx_sanitize_blog_archive_type',
	));

	$wp_customize->add_control( 'blog_archive_type', array(
		'label'       => esc_html__( 'Layout type', 'thesaasx' ),
		'type'        => 'select',
		'settings'    => 'blog_archive_type',
		'section'     => 'section_blog_archive',
		'choices'     => [
			'grid'    => esc_html__( 'Grid', 'thesaasx' ),
			'list'    => esc_html__( 'List', 'thesaasx' ),
			'classic' => esc_html__( 'Classic', 'thesaasx' ),
		],
	));


	// Sidebar in archive
	//
	$wp_customize->add_setting( "blog_archive_hide_sidebar", [
		'default'           => true,
		'sanitize_callback' => 'thesaasx_sanitize_boolean',
	]);

	$wp_customize->add_control( 'blog_archive_hide_sidebar', array(
		'label' => esc_html__( "Hide sidebar in archive pages", 'thesaasx' ),
		'section' => 'section_blog_archive',
		'type' => 'checkbox',
	));



	/*
	|--------------------------------------------------------------------------
	| Section —— Post
	|--------------------------------------------------------------------------
	*/
	$wp_customize->add_section( 'section_blog_post', array(
		'title' => esc_html__( 'Post', 'thesaasx' ),
		'panel' => 'panel_blog'
	));


	// Sidebar in post
	//
	$wp_customize->add_setting( "blog_post_hide_sidebar", [
		'default'           => true,
		'sanitize_callback' => 'thesaasx_sanitize_boolean',
	]);

	$wp_customize->add_control( 'blog_post_hide_sidebar', array(
		'label' => esc_html__( "Hide sidebar in posts", 'thesaasx' ),
		'section' => 'section_blog_post',
		'type' => 'checkbox',
	));


	// Hide tags
	//
	$wp_customize->add_setting( "blog_post_hide_tags", [
		'default'           => false,
		'sanitize_callback' => 'thesaasx_sanitize_boolean',
	]);

	$wp_customize->add_control( 'blog_post_hide_tags', array(
		'label' => esc_html__( "Hide post tags", 'thesaasx' ),
		'section' => 'section_blog_post',
		'type' => 'checkbox',
	));


	// Disable tag links
	//
	$wp_customize->add_setting( "blog_post_disable_tags_link", [
		'default'           => false,
		'sanitize_callback' => 'thesaasx_sanitize_boolean',
	]);

	$wp_customize->add_control( 'blog_post_disable_tags_link', array(
		'label' => esc_html__( "Disable link of tags", 'thesaasx' ),
		'section' => 'section_blog_post',
		'type' => 'checkbox',
	));



	/*
	|--------------------------------------------------------------------------
	| Section —— Grid
	|--------------------------------------------------------------------------
	*/
	/*
	$wp_customize->add_section( 'section_blog_grid', array(
		'title' => esc_html__( 'Layout Grid', 'thesaasx' ),
		'panel' => 'panel_blog'
	));


	// Hide classic
	//
	$wp_customize->add_setting( "blog_grid_show_image", [
		'default'           => false,
		'sanitize_callback' => 'thesaasx_sanitize_boolean',
	]);

	$wp_customize->add_control( 'blog_grid_show_image', array(
		'label' => esc_html__( "Display image", 'thesaasx' ),
		'section' => 'section_blog_grid',
		'type' => 'checkbox',
	));
	*/



	/*
	|--------------------------------------------------------------------------
	| Section —— List
	|--------------------------------------------------------------------------
	*/
	/*
	$wp_customize->add_section( 'section_blog_list', array(
		'title' => esc_html__( 'Layout List', 'thesaasx' ),
		'panel' => 'panel_blog'
	));


	// Hide classic
	//
	$wp_customize->add_setting( "blog_list_show_image", [
		'default'           => false,
		'sanitize_callback' => 'thesaasx_sanitize_boolean',
	]);

	$wp_customize->add_control( 'blog_list_show_image', array(
		'label' => esc_html__( "Display image", 'thesaasx' ),
		'section' => 'section_blog_list',
		'type' => 'checkbox',
	));
	*/



	/*
	|--------------------------------------------------------------------------
	| Section —— Classic
	|--------------------------------------------------------------------------
	*/
	/*
	$wp_customize->add_section( 'section_blog_classic', array(
		'title' => esc_html__( 'Layout Classic', 'thesaasx' ),
		'panel' => 'panel_blog'
	));


	// Show post details
	//
	$wp_customize->add_setting( "blog_classic_show_post_details", [
		'default'           => true,
		'sanitize_callback' => 'thesaasx_sanitize_boolean',
	]);

	$wp_customize->add_control( 'blog_classic_show_post_details', array(
		'label' => esc_html__( "Display post details below image", 'thesaasx' ),
		'section' => 'section_blog_classic',
		'type' => 'checkbox',
	));
	*/



}

