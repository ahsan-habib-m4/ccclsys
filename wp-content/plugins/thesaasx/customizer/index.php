<?php

require_once plugin_dir_path( __FILE__ ) . 'sanitization.php';

require_once plugin_dir_path( __FILE__ ) . 'logo.php';
require_once plugin_dir_path( __FILE__ ) . 'configuration.php';
require_once plugin_dir_path( __FILE__ ) . 'style.php';
require_once plugin_dir_path( __FILE__ ) . 'layout.php';
require_once plugin_dir_path( __FILE__ ) . 'blog.php';
require_once plugin_dir_path( __FILE__ ) . 'additional-js.php';


function thesaasx_customize_register($wp_customize){

	thesaasx_customizer_panel_logo($wp_customize);
	thesaasx_customizer_panel_configuration($wp_customize);
	thesaasx_customizer_panel_style($wp_customize);
	thesaasx_customizer_panel_layout($wp_customize);
	thesaasx_customizer_panel_blog($wp_customize);
	thesaasx_customizer_panel_additional_js($wp_customize);

}
add_action('customize_register', 'thesaasx_customize_register');
