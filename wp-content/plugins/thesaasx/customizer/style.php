<?php

function thesaasx_customizer_panel_style($wp_customize) {


	$wp_customize->add_section( 'site_style_settings', array(
		'title' => esc_html__( 'Style', 'thesaasx' ),
		'priority' => 30,
	));


    // Body font
    //
    $wp_customize->add_setting( 'style_font_body' );

    $wp_customize->add_control( 'style_font_body', array(
      'label' => esc_html__( 'Body font (e.g. Open Sans)', 'thesaasx' ),
      'section' => 'site_style_settings',
      'type' => 'text',
      'description' => '<a href="https://fonts.google.com/" target="_blank">'. esc_html__( 'Available fonts?', 'thesaasx' ) .'</a>',
    ));

    // Title font
    //
    $wp_customize->add_setting( 'style_font_title' );

    $wp_customize->add_control( 'style_font_title', array(
      'label' => esc_html__( 'Title font (e.g. Dosis)', 'thesaasx' ),
      'section' => 'site_style_settings',
      'type' => 'text',
      'description' => '<a href="https://fonts.google.com/" target="_blank">'. esc_html__( 'Available fonts?', 'thesaasx' ) .'</a>',
    ));


    // Primary color
    //
    $wp_customize->add_setting( 'style_color_primary', array(
      'default'           => '#50a1ff',
      'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'style_color_primary', array(
      'label'      => esc_html__( 'Primary color', 'thesaasx' ),
      //'mode'       => 'hue',
      'section'    => 'site_style_settings',
      'settings'   => 'style_color_primary',
    ) ) );

}

