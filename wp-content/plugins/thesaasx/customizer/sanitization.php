<?php



function thesaasx_sanitize_blog_archive_type( $value ) {
	if ( ! in_array( $value, ['grid', 'list', 'classic'] ) ) {
		$value = 'grid';
	}
	return $value;
}


function thesaasx_sanitize_boolean( $value ) {
	if ( is_string( $value ) ) {
		$value = strtolower( $value );
	}

	$true = array(
		true,
		'1',
		'true',
	);

	if ( in_array( $value, $true, true ) ) {
		return true;
	}

	return false;
}



function thesaasx_sanitize_int( $value ) {
	return intval($value);
}