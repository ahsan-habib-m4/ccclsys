<?php

function thesaasx_customizer_panel_logo($wp_customize) {

	return;
	// Default
	//
	$wp_customize->add_setting( 'logo_default', array(
		'default'           => thesaasx_get_img_uri('logo-dark.png'),
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo_default', array(
		'label'      => esc_html__( 'Logo', 'thesaasx' ),
		'section'    => 'title_tagline',
		'settings'   => 'logo_default',
		'priority'   => 1,
	) ) );


	// Light
	//
	$wp_customize->add_setting( 'logo_light', array(
		'default'           => thesaasx_get_img_uri('logo-light.png'),
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo_light', array(
		'label'      => esc_html__( 'Logo light', 'thesaasx' ),
		'section'    => 'title_tagline',
		'settings'   => 'logo_light',
		'priority'   => 2,
	) ) );

}

