<?php


/**
 * Metabox HTML code
 */
function thesaasx_metabox_extra_layout() {
	$pt = get_current_screen()->post_type;
	$the_subtitle = get_post_meta( get_the_ID(), 'the_subtitle', true );
	?>
	<div class="pagebox">
		<label><?php esc_html_e( 'Subtitle text (uses by header blocks)', 'thesaasx' ); ?></label>
		<input type="text" name="the_subtitle" value="<?php esc_attr_e( $the_subtitle ) ?>" style="width:100%">
	</div>
	<?php
}



/**
 * Define the metabox
 */
add_action( 'add_meta_boxes', function() {
	add_meta_box(
		'thesaasx-metabox-extra',
		__( 'Extra Options', 'thesaasx' ),
		'thesaasx_metabox_extra_layout',
		['page', 'post', 'the_job', 'the_portfolio', 'product'], 'side', 'core'
	);
});



/**
 * Save metabox values
 */
add_action( 'save_post', function( $post_id ) {

	if ( isset( $_POST['the_subtitle'] ) ) {
		update_post_meta( $post_id, 'the_subtitle', sanitize_text_field( $_POST['the_subtitle'] ) );
	}

});

