<?php


/**
 * Metabox HTML code
 */
function thesaasx_metabox_render_layout() {
	$pt = get_current_screen()->post_type;
	?>
	<div class="components-panel__row">
		<span><?php esc_html_e( 'Navbar', 'thesaasx' ); ?></span>
		<div><?php echo thesaasx_layout_cpt_select('thesaasx_navbar'); ?></div>
	</div>

	<?php if ( $pt !== 'page' ) : ?>
	<div class="components-panel__row">
		<span><?php esc_html_e( 'Header', 'thesaasx' ); ?></span>
		<div><?php echo thesaasx_layout_cpt_select('thesaasx_header'); ?></div>
	</div>
	<?php endif; ?>

	<div class="components-panel__row">
		<span><?php esc_html_e( 'Footer', 'thesaasx' ); ?></span>
		<div><?php echo thesaasx_layout_cpt_select('thesaasx_footer'); ?></div>
	</div>
	<?php
}



/**
 * Define the metabox
 */
add_action( 'add_meta_boxes', function() {
	add_meta_box(
		'thesaasx-metabox-layout',
		__( 'Layout', 'thesaasx' ),
		'thesaasx_metabox_render_layout',
		['page', 'post', 'the_job', 'the_portfolio', 'product'], 'side', 'core'
	);
});



/**
 * Save metabox values
 */
add_action( 'save_post', function( $post_id ) {
	$cpts = ['thesaasx_navbar', 'thesaasx_header', 'thesaasx_footer'];
	foreach ($cpts as $key => $value) {
		$meta_name = $value .'_id';
		if ( isset( $_POST[ $meta_name ] ) ) {
			update_post_meta( $post_id, $meta_name, intval( $_POST[ $meta_name ] ) );
		}
	}
});



/**
 * Create the <select> input
 */
function thesaasx_layout_cpt_select( $cpt_name ) {
	$meta_name = $cpt_name .'_id';
	$value     = get_post_meta( get_the_ID(), $meta_name, true );
	$options   = get_posts([ 'post_type' => $cpt_name ]);

	$output  = '<select name="'. $meta_name .'" style="width: 150px">';
	$output .= '<option value="0">'. __( 'Default', 'thesaasx' ) .'</option>';
	$output .= '<option value="-1"'. ($value === '-1' ? ' selected' : '') .'>'. __( 'None', 'thesaasx' ) .'</option>';

	foreach($options as $option) {
		$active = '';
		if ( intval($value) === $option->ID ) {
			$active = ' selected';
		}
		$output .= '<option value="'. $option->ID .'"'. $active .'>'. $option->post_title .'</option>';
	}

	$output .= '</select>';
	return $output;
}
