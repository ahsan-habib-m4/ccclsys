<?php
/**
 * Plugin Name: TheSaaS X
 * Plugin URI: http://thetheme.io/thesaasx
 * Description: An exclusive and required plugin for TheSaaS-X theme.
 * Author: TheThemeio
 * Author URI: https://thetheme.io/
 * Version: 1.1.4
 * License: ISC
 * License URI: https://opensource.org/licenses/ISC
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


// Bail if "TheSaaS-X" is not the active theme.
//
$theme = wp_get_theme();
$theme_name = strtolower( $theme->get( 'Name' ) );
if ( $theme_name !== 'thesaasx' && $theme_name !== 'thesaasx-child' ) {
	return;
}


define( 'THE_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
define( 'THE_PLUGIN_URL', plugins_url( '/', __FILE__ ) );
define( 'THE_PLUGIN_ASSETS_URL', THE_PLUGIN_URL . 'assets/' );

define( 'THE_BLOCK_PREFIX', 'thesaasx' );


/**
 * Register Custom Post Types.
 */
require_once plugin_dir_path( __FILE__ ) . 'cpt/index.php';

/**
 * Register custom blocks for Gutenberg.
 */
require_once plugin_dir_path( __FILE__ ) . 'gutenberg/setup.php';

/**
 * Register Customizer.
 */
require_once plugin_dir_path( __FILE__ ) . 'customizer/index.php';

/**
 * Register Metaboxes.
 */
require_once plugin_dir_path( __FILE__ ) . 'metabox/extra.php';
require_once plugin_dir_path( __FILE__ ) . 'metabox/layout.php';

/**
 * Send email for contact blocks.
 */
require_once plugin_dir_path( __FILE__ ) . 'utils/contact.php';



function thesaasx_plugin_activate() {
	thesaasx_register_job_cpt();
	thesaasx_register_portfolio_cpt();
	thesaasx_register_portfolio_category_taxonomy();
	flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'thesaasx_plugin_activate' );
