<?php

function thesaasx_register_footer_cpt() {

	$labels = array(
		'name'                  => esc_html_x( 'Footers', 'thesaasx' ),
		'singular_name'         => esc_html_x( 'Footer', 'thesaasx' ),
	);

	$args = array(
		'label'                 => esc_html__( 'Footer', 'thesaasx' ),
		'description'           => esc_html__( 'Layout to be use in footer sections', 'thesaasx' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'revisions' ),
		'show_ui'               => true,
		'show_in_rest'          => true,
		'show_in_admin_bar'     => false,
		'show_in_menu'          => false,
		'menu_position'         => 50,
		'exclude_from_search'   => true,
		'capability_type'       => 'page',
		'menu_icon'             => 'dashicons-media-text',
		'rewrite'               => array( 'slug' => 'footer' ),
		'template'              => [
										[ 'thesaasx/footer-1' ]
									 ]
	);

	register_post_type( 'thesaasx_footer', $args );
}
add_action( 'init', 'thesaasx_register_footer_cpt' );
