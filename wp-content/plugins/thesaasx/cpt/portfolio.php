<?php

function thesaasx_register_portfolio_cpt() {

	$labels = array(
		'name'                  => esc_html_x( 'Portfolios', 'Portfolio General Name', 'thesaas' ),
		'singular_name'         => esc_html_x( 'Portfolio', 'Portfolio Singular Name', 'thesaas' ),
		'menu_name'             => esc_html__( 'Portfolios', 'thesaas' ),
		'name_admin_bar'        => esc_html__( 'Portfolio', 'thesaas' ),
		'archives'              => esc_html__( 'Portfolio Archives', 'thesaas' ),
		'attributes'            => esc_html__( 'Portfolio Attributes', 'thesaas' ),
		'parent_item_colon'     => esc_html__( 'Parent Portfolio:', 'thesaas' ),
		'all_items'             => esc_html__( 'All Portfolios', 'thesaas' ),
		'add_new_item'          => esc_html__( 'Add New Portfolio', 'thesaas' ),
		'add_new'               => esc_html__( 'Add New', 'thesaas' ),
		'new_item'              => esc_html__( 'New Portfolio', 'thesaas' ),
		'edit_item'             => esc_html__( 'Edit Portfolio', 'thesaas' ),
		'update_item'           => esc_html__( 'Update Portfolio', 'thesaas' ),
		'view_item'             => esc_html__( 'View Portfolio', 'thesaas' ),
		'view_items'            => esc_html__( 'View Portfolios', 'thesaas' ),
		'search_items'          => esc_html__( 'Search Portfolio', 'thesaas' ),
		'not_found'             => esc_html__( 'Not found', 'thesaas' ),
		'not_found_in_trash'    => esc_html__( 'Not found in Trash', 'thesaas' ),
		'featured_image'        => esc_html__( 'Featured Image', 'thesaas' ),
		'set_featured_image'    => esc_html__( 'Set featured image', 'thesaas' ),
		'remove_featured_image' => esc_html__( 'Remove featured image', 'thesaas' ),
		'use_featured_image'    => esc_html__( 'Use as featured image', 'thesaas' ),
		'insert_into_item'      => esc_html__( 'Insert into portfolio', 'thesaas' ),
		'uploaded_to_this_item' => esc_html__( 'Uploaded to this portfolio', 'thesaas' ),
		'items_list'            => esc_html__( 'Portfolios list', 'thesaas' ),
		'items_list_navigation' => esc_html__( 'Portfolios list navigation', 'thesaas' ),
		'filter_items_list'     => esc_html__( 'Filter portfolios list', 'thesaas' ),
	);

	$args = array(
		'label'                 => esc_html__( 'Portfolio', 'thesaasx' ),
		'description'           => esc_html__( 'Layout to be use in portfolio sections', 'thesaasx' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions' ),
		'taxonomies'            => array( 'the_portfolio_cat' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'show_in_rest'          => true,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'menu_position'         => 30,
		'capability_type'       => 'page',
		'menu_icon'             => 'dashicons-portfolio',
		'register_meta_box_cb'  => 'thesaasx_register_portfolio_meta_box',
		'rewrite'               => array( 'slug' => 'portfolio', 'with_front' => false ),
	);
	register_post_type( 'the_portfolio', $args );
}
add_action( 'init', 'thesaasx_register_portfolio_cpt' );



// Register Categories Taxonomy
function thesaasx_register_portfolio_category_taxonomy() {

  $labels = array(
    'name'                       => esc_html_x( 'Categories', 'Category General Name', 'thesaas' ),
    'singular_name'              => esc_html_x( 'Category', 'Category Singular Name', 'thesaas' ),
    'menu_name'                  => esc_html__( 'Category', 'thesaas' ),
    'all_items'                  => esc_html__( 'All Categories', 'thesaas' ),
    'parent_item'                => esc_html__( 'Parent Category', 'thesaas' ),
    'parent_item_colon'          => esc_html__( 'Parent Category:', 'thesaas' ),
    'new_item_name'              => esc_html__( 'New Category Name', 'thesaas' ),
    'add_new_item'               => esc_html__( 'Add New Category', 'thesaas' ),
    'edit_item'                  => esc_html__( 'Edit Category', 'thesaas' ),
    'update_item'                => esc_html__( 'Update Category', 'thesaas' ),
    'view_item'                  => esc_html__( 'View Category', 'thesaas' ),
    'separate_items_with_commas' => esc_html__( 'Separate category with commas', 'thesaas' ),
    'add_or_remove_items'        => esc_html__( 'Add or remove category', 'thesaas' ),
    'choose_from_most_used'      => esc_html__( 'Choose from the most used', 'thesaas' ),
    'popular_items'              => esc_html__( 'Popular Categories', 'thesaas' ),
    'search_items'               => esc_html__( 'Search Categories', 'thesaas' ),
    'not_found'                  => esc_html__( 'Not Found', 'thesaas' ),
    'no_terms'                   => esc_html__( 'No category', 'thesaas' ),
    'items_list'                 => esc_html__( 'Categories list', 'thesaas' ),
    'items_list_navigation'      => esc_html__( 'Categories list navigation', 'thesaas' ),
  );
  $args = array(
    'labels'                     => $labels,
    'public'                     => true,
    'show_ui'                    => true,
    'show_in_rest'               => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => false,
    'show_tagcloud'              => true,
  );
  register_taxonomy( 'the_portfolio_cat', array( 'the_portfolio' ), $args );

}
add_action( 'init', 'thesaasx_register_portfolio_category_taxonomy' );




/**
 * Register Meta Box
 */
function thesaasx_register_portfolio_meta_box() {
  //add_meta_box( 'thesaasx-page-options', esc_html__( 'Portfolio Details', 'thesaasx' ), 'thesaasx_portfolio_meta_box_callback', 'the_portfolio', 'side', 'core' );
}
add_action( 'add_meta_boxes', 'thesaasx_register_portfolio_meta_box');


/**
 * Add fields
 */
function thesaasx_portfolio_meta_box_callback( $post ) {

  // Add an nonce field so we can check for it later.
  wp_nonce_field( 'thesaasx_inner_custom_box', 'thesaasx_inner_custom_box_nonce' );

  $the_location = get_post_meta( get_the_ID(), 'the_location', true );
  ?>
  <div class="pagebox">
    <label><?php esc_html_e('Location', 'thesaasx' ); ?></label>
    <input type="text" name="the_location" value="<?php echo esc_attr( $the_location ); ?>" style="width:100%">
  </div>
  <?php

}




/**
 * Save meta box content.
 */
function thesaasx_save_portfolio_meta_box( $post_id ) {

	if ( isset( $_POST['the_location'] ) ) {
		update_post_meta( $post_id, 'the_location', sanitize_text_field( $_POST['the_location'] ) );
	}

}
add_action( 'save_post', 'thesaasx_save_portfolio_meta_box' );





add_filter( 'display_post_states', 'thesaasx_cpt_portfolio_landing_post_state', 10, 2 );
function thesaasx_cpt_portfolio_landing_post_state( $post_states, $post ) {
	if( $post->post_name == 'portfolio' && $post->post_type == 'page' && $post->post_parent === 0 ) {
		$post_states[] = esc_html__('Portfolios Landing Page', 'thesaasx' );
	}
	return $post_states;
}
