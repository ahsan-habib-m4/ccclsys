<?php

/**
 * Add the separator to the admin menu
 */
/*
add_action( 'admin_init', function( $position ) {
	global $menu;
	$position = 49;
	$menu[ $position ] = array(
		0 =>  '',
		1 =>  'read',
		2 =>  'separator' . $position,
		3 =>  '',
		4 =>  'wp-menu-separator'
	);
});
*/

/**
 * Set admin menu separator for layouts
 */
add_action( 'admin_menu', function() {
	global $menu;
	$position = 49;
	$menu[ $position ] = array(
		0 =>  '',
		1 =>  'read',
		2 =>  'separator' . $position,
		3 =>  '',
		4 =>  'wp-menu-separator'
	);
	//do_action( 'admin_init', 45 );
});


/**
 * Register Layout menu
 */
add_action( 'admin_menu', 'thesaasx_register_layout_menu' );
function thesaasx_register_layout_menu() {
	global $submenu;
	$slug = 'edit.php?post_type=thesaasx_navbar';

	add_menu_page(
		esc_html_x( 'Layout', 'thesaasx' ),
		esc_html_x( 'Layout', 'thesaasx' ),
		'manage_options',
		$slug,
		'',
		'dashicons-welcome-widgets-menus',
		50
	);


	// Remove default menus
	unset( $submenu[ $slug ][5] );
	unset( $submenu[ $slug ][10] );


	add_submenu_page(
		$slug,
		esc_html_x( 'Navbar', 'thesaasx' ),
		esc_html_x( 'Navbar', 'thesaasx' ),
		'manage_options',
		'edit.php?post_type=thesaasx_navbar'
	);

	add_submenu_page(
		$slug,
		esc_html_x( 'Header', 'thesaasx' ),
		esc_html_x( 'Header', 'thesaasx' ),
		'manage_options',
		'edit.php?post_type=thesaasx_header'
	);

	add_submenu_page(
		$slug,
		esc_html_x( 'Footer', 'thesaasx' ),
		esc_html_x( 'Footer', 'thesaasx' ),
		'manage_options',
		'edit.php?post_type=thesaasx_footer'
	);
}


/**
 * Import custom post types.
 */
require_once plugin_dir_path( __FILE__ ) . 'portfolio.php';
require_once plugin_dir_path( __FILE__ ) . 'job.php';
require_once plugin_dir_path( __FILE__ ) . 'navbar.php';
require_once plugin_dir_path( __FILE__ ) . 'header.php';
require_once plugin_dir_path( __FILE__ ) . 'footer.php';
