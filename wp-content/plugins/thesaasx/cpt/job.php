<?php

function thesaasx_register_job_cpt() {

	$labels = array(
		'name'                  => esc_html_x( 'Jobs', 'Job General Name', 'thesaas' ),
		'singular_name'         => esc_html_x( 'Job', 'Job Singular Name', 'thesaas' ),
		'menu_name'             => esc_html__( 'Jobs', 'thesaas' ),
		'name_admin_bar'        => esc_html__( 'Job', 'thesaas' ),
		'archives'              => esc_html__( 'Job Archives', 'thesaas' ),
		'attributes'            => esc_html__( 'Job Attributes', 'thesaas' ),
		'parent_item_colon'     => esc_html__( 'Parent Job:', 'thesaas' ),
		'all_items'             => esc_html__( 'All Jobs', 'thesaas' ),
		'add_new_item'          => esc_html__( 'Add New Job', 'thesaas' ),
		'add_new'               => esc_html__( 'Add New', 'thesaas' ),
		'new_item'              => esc_html__( 'New Job', 'thesaas' ),
		'edit_item'             => esc_html__( 'Edit Job', 'thesaas' ),
		'update_item'           => esc_html__( 'Update Job', 'thesaas' ),
		'view_item'             => esc_html__( 'View Job', 'thesaas' ),
		'view_items'            => esc_html__( 'View Jobs', 'thesaas' ),
		'search_items'          => esc_html__( 'Search Job', 'thesaas' ),
		'not_found'             => esc_html__( 'Not found', 'thesaas' ),
		'not_found_in_trash'    => esc_html__( 'Not found in Trash', 'thesaas' ),
		'featured_image'        => esc_html__( 'Featured Image', 'thesaas' ),
		'set_featured_image'    => esc_html__( 'Set featured image', 'thesaas' ),
		'remove_featured_image' => esc_html__( 'Remove featured image', 'thesaas' ),
		'use_featured_image'    => esc_html__( 'Use as featured image', 'thesaas' ),
		'insert_into_item'      => esc_html__( 'Insert into job', 'thesaas' ),
		'uploaded_to_this_item' => esc_html__( 'Uploaded to this job', 'thesaas' ),
		'items_list'            => esc_html__( 'Jobs list', 'thesaas' ),
		'items_list_navigation' => esc_html__( 'Jobs list navigation', 'thesaas' ),
		'filter_items_list'     => esc_html__( 'Filter jobs list', 'thesaas' ),
	);

	$args = array(
		'label'                 => esc_html__( 'Job', 'thesaasx' ),
		'description'           => esc_html__( 'Layout to be use in job sections', 'thesaasx' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'show_in_rest'          => true,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'menu_position'         => 30,
		'capability_type'       => 'page',
		'menu_icon'             => 'dashicons-businessman',
		'register_meta_box_cb'  => 'thesaasx_register_job_meta_box',
		'rewrite'               => array( 'slug' => 'jobs', 'with_front' => false ),
	);
	register_post_type( 'the_job', $args );
}
add_action( 'init', 'thesaasx_register_job_cpt' );



/**
 * Register Meta Box
 */
function thesaasx_register_job_meta_box() {
  add_meta_box( 'thesaasx-page-options', esc_html__( 'Job Details', 'thesaasx' ), 'thesaasx_job_meta_box_callback', 'the_job', 'side', 'core' );
}
add_action( 'add_meta_boxes', 'thesaasx_register_job_meta_box');


/**
 * Add fields
 */
function thesaasx_job_meta_box_callback( $post ) {

  // Add an nonce field so we can check for it later.
  wp_nonce_field( 'thesaasx_inner_custom_box', 'thesaasx_inner_custom_box_nonce' );

  $the_location = get_post_meta( get_the_ID(), 'the_location', true );
  ?>
  <div class="pagebox">
    <label><?php esc_html_e('Location', 'thesaasx' ); ?></label>
    <input type="text" name="the_location" value="<?php echo esc_attr( $the_location ); ?>" style="width:100%">
  </div>
  <?php

}




/**
 * Save meta box content.
 */
function thesaasx_save_job_meta_box( $post_id ) {

	if ( isset( $_POST['the_location'] ) ) {
		update_post_meta( $post_id, 'the_location', sanitize_text_field( $_POST['the_location'] ) );
	}

}
add_action( 'save_post', 'thesaasx_save_job_meta_box' );





add_filter( 'display_post_states', 'thesaasx_cpt_job_landing_post_state', 10, 2 );
function thesaasx_cpt_job_landing_post_state( $post_states, $post ) {
	if( $post->post_name == 'jobs' && $post->post_type == 'page' && $post->post_parent === 0 ) {
		$post_states[] = esc_html__('Jobs Landing Page', 'thesaasx' );
	}
	return $post_states;
}
