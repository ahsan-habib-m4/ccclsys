<?php

function thesaasx_register_navbar_cpt() {

	$labels = array(
		'name'                  => esc_html_x( 'Navbars', 'thesaasx' ),
		'singular_name'         => esc_html_x( 'Navbar', 'thesaasx' ),
	);

	$args = array(
		'label'                 => esc_html__( 'Navbar', 'thesaasx' ),
		'description'           => esc_html__( 'Layout to be use in navbar sections', 'thesaasx' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'revisions' ),
		'show_ui'               => true,
		'show_in_rest'          => true,
		'show_in_admin_bar'     => false,
		'show_in_menu'          => false,
		'menu_position'         => 50,
		'exclude_from_search'   => true,
		'capability_type'       => 'page',
		'menu_icon'             => 'dashicons-media-text',
		'rewrite'               => array( 'slug' => 'navbar' ),
		'template'              => [
										[ 'thesaasx/navbar-1' ]
									 ]
	);

	register_post_type( 'thesaasx_navbar', $args );
}
add_action( 'init', 'thesaasx_register_navbar_cpt' );
