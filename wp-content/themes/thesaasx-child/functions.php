<?php

/**
 * Enqueue scripts and styles.
 */
add_action( 'wp_enqueue_scripts', 'thesaasx_child_enqueue_styles' );
function thesaasx_child_enqueue_styles() {

	$my_theme = wp_get_theme();
	$version = $my_theme->get( 'Version' );

	// Child theme style.
	//
	wp_enqueue_style( 'thesaasx-child',
		get_stylesheet_directory_uri() . '/style.css',
		array( 'thesaasx' ),
		$version
	);
}

require_once( get_stylesheet_directory() . '/inc/cccl-functions.php');
?>
