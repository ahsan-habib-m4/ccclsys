<?php /* Template Name: cccl_login */ ?>


<div id="primary" class="content-area">

<div class="entry-content">

<?php 
if ( current_user_can( 'subscriber' ) ){
	echo "Hi, dear Member! Glad seeing you again!";
}elseif (current_user_can( 'contributor' )){
	echo "Hi, dear Operator! Glad seeing you again!";
}
if (is_user_logged_in()) { ?>
<a class="login_button" href="<?php echo wp_logout_url( home_url() ); ?>">Logout</a>
<?php } else { ?>
	<div class="row">
		<form id="login" action="login" method="post">
		<fieldset>
	        <p class="status"></p>
	        <p>
			        <input id="username" type="text" name="username" placeholder="Username">
			</p>
			<p>
		        <input id="password" type="password" name="password" placeholder="Password">
	        </p>
			<p>
		        <input class="submit_button" type="submit" value="Login" name="submit">
		        <?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
		    </p>    
	        </fieldset>
		</form>
	</div>	
<?php } ?>
</div>
</div>
<?php get_footer(); ?>