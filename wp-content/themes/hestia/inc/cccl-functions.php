<?php

function ajax_init() {

	wp_register_script('ajax-script', get_template_directory_uri() . '/assets/js/ajax-script.js', array('jquery') );
	wp_enqueue_script('ajax-script');

	wp_localize_script( 'ajax-script', 'ajax_object', array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'redirecturl' => home_url(),
			'loadingmessage' => __('Sending user info, please wait...')
	));
	// Enable the user with no privileges to run ajax_login() in AJAX
	if (is_user_logged_in()) {
		add_action('wp_ajax_nopriv_updatemember', 'update_member');
		add_action('wp_ajax_updatemember', 'update_member');
		add_action('wp_ajax_nopriv_imageprocess', 'image_processing');
		add_action('wp_ajax_imageprocess', 'image_processing');
		add_action('wp_ajax_nopriv_changepassword', 'change_password');
		add_action('wp_ajax_changepassword', 'change_password');
		add_action('wp_ajax_getproducts', 'get_products');
		add_action('wp_ajax_checkpromocode', 'check_promo_code');
		add_action('wp_ajax_itemsavail', 'get_items');
		add_action('wp_ajax_tmprelease', 'temporary_booking_release');
	} else {
		add_action('wp_ajax_nopriv_ajaxlogin', 'ajax_login');
		add_action('wp_ajax_ajaxlogin', 'ajax_login');
	}
}

add_action('init', 'ajax_init');

function ajax_login() {

	// First check the nonce, if it fails the function will break
	check_ajax_referer( 'ajax-login-nonce', 'security' );
	// Nonce is checked, get the POST data and sign user on
	$info = array();
	$info['user_login'] = $_POST['username'];
	$info['user_password'] = $_POST['password'];
	$info['remember'] = $_POST['rememberme'];
	$user_details= get_user_list($info['user_login']);
	if (!empty($user_details) &&  $user_details->status == 'Discontinued') {
		echo json_encode(array('loggedin'=>false, 'message'=>__('You are not allowed to login. Please contact CCCL office.')));
		die();
	}
	$user_signon = wp_signon($info);
	if ( is_wp_error($user_signon) ) {
		echo json_encode(array('loggedin'=>false, 'message'=>__('Wrong username or password.')));
	} else {
		if ($user_signon->roles{0}  ==  'contributor' || $user_signon->roles{0}  ==  'author'  ) {
			$redirecturl = get_permalink(33);
		} else {
			$redirecturl = get_permalink(31);
		}
		echo json_encode(array('loggedin'=>true, 'redirecturl'=>$redirecturl, 'message'=>__('Login successful, redirecting...')));
	}
	die();
}
add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
	if (!current_user_can('administrator') && !is_admin()) {
		show_admin_bar(false);
	}
}

function add_Subscription_fee() {
	$user = wp_get_current_user();
	if (wp_check_password( $_POST['password'], $user->user_pass, $user->ID )) {
		global $wpdb;
		$data = array();
		$data['year'] = $_POST['slctYear'];
		$data['quater'] = $_POST['slctQuarter'];
		$data['subscription_fee'] = $_POST['sbcrpnfee'];
		$data['late_fee'] = $_POST['latefee'];
		$data['abroad_fee'] = $_POST['abrdfee'];
		$data['user_login'] = $user->user_login;
		$data['quarter_year'] = $data['year'].$data['quater'];
		$table_name = $wpdb->prefix . "subs_fees";
		$sql = "SELECT * FROM $table_name WHERE year = '$data[year]' AND quater = '$data[quater]'";
		$results = $wpdb->get_results($sql);
		if (count($results) > 0) {
			return "Failed! duplicate quarter";
		} else {
			$amount_format = number_format($data['subscription_fee'], 2);
			if ($wpdb->insert( $table_name, $data )) {
				add_aduit_log(SUBSFEE, "", $user->user_login, "Added subscription fee ". $amount_format .", late fee ".$data['late_fee']."%,  adroad fee " .$data['abroad_fee']. "% for quarter ". $data['quarter_year'] );
				return;
			} else {
				sql_log($wpdb->last_query, $wpdb->last_error);
				return $wpdb->last_error;
			}
		}
	} else {
		return "Incorrect password";
	}
}

function update_Subscription_fee() {
	$user = wp_get_current_user();
	if (wp_check_password( $_POST['password'], $user->user_pass, $user->ID )) {
		global $wpdb;
		$year = $_POST['slctYear'];
		$quater = $_POST['slctQuarter'];
		$subscription_fee = $_POST['sbcrpnfee'];
		$late_fee = $_POST['latefee'];
		$abroad_fee = $_POST['abrdfee'];
		$quarter_year = $year.$quater;
		$id = $_POST['id'];
		$table_name = $wpdb->prefix . "subs_fees";
		$amount_format = number_format($subscription_fee, 2);
		$sql = "UPDATE $table_name SET year=$year, quater='$quater', subscription_fee=$subscription_fee, late_fee=$late_fee, abroad_fee=$abroad_fee, user_login='$user->user_login', quarter_year='$quarter_year' WHERE id=$id";
		if ($wpdb->query($wpdb->prepare($sql))) {
			add_aduit_log(SUBSFEE, "", $user->user_login, "Updated subscription fee ". $amount_format .", late fee ".$late_fee."%,  adroad fee " .$abroad_fee. "% for quarter ". $quarter_year);
			return;
		} else {
			sql_log($wpdb->last_query, $wpdb->last_error);
			return $wpdb->last_error;
		}
	} else {
		return "Incorrect password";
	}
}

function get_Subscription_fee_list() {
	global $wpdb;
	$table_name = $wpdb->prefix . "subs_fees";
	$sql = "SELECT * FROM $table_name";
	$results = $wpdb->get_results($sql);
	return $results;
}

add_filter( 'wp_nav_menu_items', 'wti_loginout_menu_link', 10, 2 );

function wti_loginout_menu_link( $items, $args ) {
	$user = wp_get_current_user();
	
	if ( $user->roles{0}  == 'contributor' || $user->roles{0}  == 'author' ) {
		$redirecturl = get_permalink(65);
	} else {
		$redirecturl = home_url();
	}
	if ($args->theme_location == 'primary') {
		if (is_user_logged_in()) {
		    $toolTips= "data-toggle='tooltip' data-html='true' data-placement='bottom' title='".getUserDetails()."'";
		    $items .= '<li class="right"><a class="logoutBtn" '.$toolTips.' href="'. wp_logout_url($redirecturl) .'">'. __("<i class='fas fa-sign-out-alt'></i>") .'</a></li>';
		}
	}
	return $items;
}

function get_user_list($userLogin) {
	global $wpdb;
	$userLogin = add_padding ($userLogin);
	$table_user = $wpdb->prefix . "users";
	$table_user_details = $wpdb->prefix . "users_details";
	$table_member_type = $wpdb->prefix . "member_type";
	$sql = "SELECT
			`$table_user`.`ID`,
			`$table_user`.`user_login`,
			`$table_user`.`user_email`,
			`$table_user`.`display_name`,
			`$table_user_details`.`college`,
			`$table_user_details`.`batch`,
			`$table_user_details`.`cadet_no`,
			`$table_user_details`.`cadet_name`,
			`$table_user_details`.`member_full_id`,
			`$table_user_details`.`status`,
			`$table_user_details`.`type`,
			`$table_user_details`.`image`,
			`$table_user_details`.`mobile_number`,
			`$table_user_details`.`postal_address`,
			`$table_user_details`.`profession`,
			`$table_user_details`.`organization`,
			`$table_user_details`.`designation`,
			`$table_user_details`.`specialization`,
			`$table_user_details`.`office_address`,
			`$table_user_details`.`home_address`,
			`$table_user_details`.`date_of_birth`,
			`$table_user_details`.`blood_group`,
			`$table_user_details`.`hsc_year`,
			`$table_user_details`.`anniversary`,
			`$table_user_details`.`spouse`,
			`$table_user_details`.`spouse_occupation`,
			`$table_user_details`.`child1`,
			`$table_user_details`.`child1_name`,
			`$table_user_details`.`child1_date_of_birth`,
			`$table_user_details`.`child2`,
			`$table_user_details`.`child2_name`,
			`$table_user_details`.`child2_date_of_birth`,
			`$table_user_details`.`updated_by`,
			`$table_user_details`.`last_updated_time`,
			`$table_user_details`.`payment_upto`,
			`$table_member_type`.`member_type`,
			`$table_member_type`.`subcription_required`
			FROM `$table_user_details`
			INNER JOIN `$table_user` ON (`$table_user_details`.`user_login` = `$table_user`.`user_login`)
			INNER JOIN `$table_member_type` ON (`$table_user_details`.`type` = `$table_member_type`.`code`)
			WHERE `$table_user`.`user_login` = '$userLogin'";
	$result = $wpdb->get_row($sql);
	return $result;
}

function send_or_change_password($userLogin, $password = "", $oldPass= "") {
	global $wpdb;
	$table_user = $wpdb->prefix . "users";
	$sql = "SELECT id , user_login, user_email, user_pass, display_name FROM $table_user
			WHERE $table_user.`user_login` = '$userLogin'";
	$result = $wpdb->get_row($sql);
	if (@count($result)>0) {
		if (empty($password)) {
			$password = wp_generate_password( 6, true, false);
			wp_set_password( $password, $result->id );
			$headers = array('Content-Type: text/html; charset=UTF-8');
			$message = "<p>Dear $result->display_name,</p>
						<p></p>
						<p>Thank you for using Cadet College Club Limited. Your email address is $result->user_email and your password is <strong>$password</strong><p>
						<p><p>
						<p>With thanks,</p>
						<p>Cadet College Club Limited <p>";
			if (wp_mail($result->user_email, "Password recovery", $message, $headers)) {
				add_aduit_log(MEMBER, $result->user_login, $result->user_login, "Changing password send to email ($result->user_email) address ");
				add_aduit_log(MEMBER, $result->user_login, $result->user_login, "Get password from " . get_the_user_ip());
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	} else {
		return false;
	}
}

function get_the_user_ip() {
	if (! empty ( $_SERVER ['HTTP_CLIENT_IP'] )) {
		// check ip from share internet
		$ip = $_SERVER ['HTTP_CLIENT_IP'];
	} elseif (! empty ( $_SERVER ['HTTP_X_FORWARDED_FOR'] )) {
		// to check ip is pass from proxy
		$ip = $_SERVER ['HTTP_X_FORWARDED_FOR'];
	} else {
		$ip = $_SERVER ['REMOTE_ADDR'];
	}
	return $ip;
}

function show_custom_message($message, $status) {
	$html .= "<div class='row messages'>";
	$html .= 	"<div class='col-md-12 col-sm-12'>";
	$html .= 		"<div class='alert alert-$status alert-dismissible' role='alert'>";
	$html .= 			$message;
	$html .= 			"<button type='button' class='close'>";
	$html .= 				"<span aria-hidden='true'>&times;</span>";
	$html .= 			"</button>";
	$html .= 		"</div>";
	$html .= 	"</div>";
	$html .= "</div>";	
	return $html;
}

function get_college_list() {
	global $wpdb;
	$table_college = $wpdb->prefix . "college";
	$sql = "SELECT * FROM $table_college;";
	$results = $wpdb->get_results($sql);
	return $results;
}

function get_member_type_list() {
	global $wpdb;
	$table_member_type = $wpdb->prefix . "member_type";
	$sql = "SELECT * FROM $table_member_type;";
	$results = $wpdb->get_results($sql);
	return $results;
}

function get_profession_list() {
	global $wpdb;
	$table_profession = $wpdb->prefix . "profession";
	$sql = "SELECT * FROM $table_profession;";
	$results = $wpdb->get_results($sql);
	return $results;
}

function get_country_list() {
	global $wpdb;
	$table_country = $wpdb->prefix . "country";
	$sql = "SELECT * FROM $table_country;";
	$results = $wpdb->get_results($sql);
	return $results;
}

function add_member($data) {
	require_once(ABSPATH.'wp-admin/includes/user.php' );
	$password = wp_generate_password( 6, true, false);
	$user_login = $_POST['txtmemberId'];
	$member_email = $_POST['txtemail'];
	if (is_email( $member_email)) {
	    if (!isPhone($data['mobile_number'], "")) {
    		$user_id = wp_create_user($user_login, $password, $member_email);
    		if (is_wp_error($user_id)) {
    			$error = $user_id->get_error_message();
    			sql_log($wpdb->last_query, $error);
    			return $error;
    		} else {
    		    if ($_FILES['photo']['size'] != 0) {
    			 $error = member_image_upload($user_login, $newImageFile);
    		    }
    			if (!empty($error)) {
    				wp_delete_user($user_id);
    				website_log($error. $user_login.'_'.$newImageFile);
    				return $error;
    			} else {
    				global $wpdb;
    				$user = wp_get_current_user();
    				$user_data = array();
    				$user_data['display_name'] = $_POST['txtname'];
    				$user_data['ID'] = $user_id;
    				if (!wp_update_user($user_data )) {
    					wp_delete_user($user_id);
    					sql_log($wpdb->last_query, $wpdb->last_error);
    					return $wpdb->last_error;
    				}
    				$data['user_login'] = $user_login;
    				if ($_FILES['photo']['size'] != 0) {
    				    $data['image'] = $newImageFile;
    				}
    				$data['updated_by'] = $user->user_login;
    				$data['payment_upto'] = $_POST['paidupto'];
    				$table_users_details = $wpdb->prefix . "users_details";
    				if ($wpdb->insert( $table_users_details, $data )) {
    					add_aduit_log(MEMBER, $user_login, $user->user_login, "Added new member user: $user_login name: ". $_POST['txtname']);
    					return;
    				} else {
    					wp_delete_user($user_id);
    					sql_log($wpdb->last_query, $wpdb->last_error);
    					return $wpdb->last_error;
    				}
    			}
    		}
	    } else {
	    	$msg = "Duplicate mobile number!!";
	    	website_log("$msg (".$data['mobile_number'].")");
	    	return $msg;
	    }
	} else {
		$msg = "Invalid email address!!";
		website_log("$msg ($member_email)");
		return $msg;
	}
}

function edit_member() {
	$user_login = $_POST['srcByMbrId'];
	$member_email = $_POST['txtemail'];
	if (is_email( $member_email)) {
	    if ( !isPhone($_POST['txtPhone'], $user_login) ) {
    		if ($_FILES['photo']['size'] != 0) {
    			$error = member_image_upload($user_login, $newImageFile);
    			website_log($error. "User login:".$user_login. ', new image file: '.$newImageFile);
    		}
    		if (!empty($error)) {
    			return $error;
    		} else {
    			$update_data = "";
    			global $wpdb;
    			$user = wp_get_current_user();
    			$user_data = array();
    			$user_data['user_email'] = $member_email;
    			$user_data['display_name'] = $_POST['txtname'];
    			$user_data['ID'] = $_POST['id'];
    			$e_result = get_change_information("user_email", $member_email, $user_login, "Email address changed");
    			if (!empty($e_result)) {
    				$update_data .= $e_result;
    			}
				$result = wp_update_user( $user_data );
				if (is_wp_error($result)) {
					sql_log($wpdb->last_query, $wpdb->last_error);
					return $result->get_error_message();
				}
    			$table_users_details = $wpdb->prefix . "users_details";
    			$data = prepare_member_data();
    			if ($_FILES['photo']['size'] != 0) {
    				$data['image'] = $newImageFile;
    			}
    			unset($data ['id']);
    			$data['updated_by'] = $user->user_login;
    			$where = [ 'user_login' => $user_login ];
    			$e_result = get_change_information("mobile_number", $data["mobile_number"], $user_login, "Mobile number changed");
    			if (!empty($e_result)) {
    				$update_data .= empty($update_data) ? $e_result : ". $e_result";
    			}
    			$wpdb->update($table_users_details, $data, $where);
    			if ($wpdb->last_error) {
    				sql_log($wpdb->last_query, $wpdb->last_error);
    				return $wpdb->last_error;
    			} else {
					add_aduit_log(MEMBER, $user_login, $user->user_login, "Profile (".$_POST['txtname'].") has been updated. $update_data");
    				return;
    			}
    		}
	    } else {
	    	$msg = "Duplicate mobile number!!";
	    	website_log("$msg (".$_POST['txtPhone'].")");
	        return $msg;
	    }
	} else {
		$msg = "Invalid email address!!";
		website_log("$msg ($member_email)");
		return $msg;
	}
}
function prepare_member_data() {
	$data = array();
	if (!empty($_POST['id'])) {
		$data['id'] = $_POST['id'];
	}
	if (!empty($_POST['college'])) {
		$data['college'] = $_POST['college'];
	}
	if (!empty($_POST['college'])) {
		$data['batch'] = $_POST['batch'];
	}
	if (!empty($_POST['batch'])) {
		$data['cadet_no'] = $_POST['cadetno'];
	}
	if (!empty($_POST['cadetname'])) {
		$data['cadet_name'] = $_POST['cadetname'];
	}
	if (!empty($_POST['slctMbrType'])) {
		$data['type'] = $_POST['slctMbrType'];
	}
	if (!empty($_POST['mmbrfulId'])) {
		$data['member_full_id'] = $_POST['mmbrfulId'];
	}
	if (!empty($_POST['slctMbrStts'])) {
		$data['status'] = $_POST['slctMbrStts'];
	}
	if (!empty($_POST['paidupto'])) {
		$data['payment_upto'] = $_POST['paidupto'];
	}
	$data['mobile_number'] = str_replace(' ', '', $_POST['txtPhone']);
	$data['postal_address'] = $_POST['postalAddrs'];
	$data['profession'] = $_POST['profession'];
	$data['organization'] = $_POST['organization'];
	$data['designation'] = $_POST['designation'];
	$data['specialization'] = $_POST['specialization'];
	$data['office_address'] = $_POST['officeAddr'];
	$data['home_address'] = $_POST['homeAddr'];
	if (!empty($_POST['dateOfBirth'])) {
		$data['date_of_birth'] = date("Y-m-d", strtotime($_POST['dateOfBirth']));
	}
	if (!empty($_POST['bloodgrp'])) {
		$data['blood_group'] = $_POST['bloodgrp'];
	}
	if (!empty($_POST['hscyear'])) {
		$data['hsc_year'] = $_POST['hscyear'];
	}
	$data['spouse'] = $_POST['spouse'];
	$data['spouse_occupation'] = $_POST['spouseOccu'];
	if (!empty($_POST['anniversary'])) {
		$data['anniversary'] = date("Y-m-d", strtotime($_POST['anniversary']));
	} else {
		$data['anniversary']= "";
	}
	$data['child1'] = $_POST['child1'];
	$data['child1_name'] = $_POST['child1name'];
	if (!empty($_POST['child1dob'])) {
		$data['child1_date_of_birth'] = date("Y-m-d", strtotime($_POST['child1dob']));
	} else {
		$data['child1_date_of_birth'] = "";
	}
	$data['child2'] = $_POST['child2'];
	$data['child2_name'] = $_POST['child2name'];
	if (!empty($_POST['child2dob'])) {
		$data['child2_date_of_birth'] = date("Y-m-d", strtotime($_POST['child2dob']));
	} else {
		$data['child2_date_of_birth'] = "";
	}
	$data['ip_address'] = get_the_user_ip();
	
	return $data;
}

function member_image_upload($user_login, &$newImageFile) {
	$target_dir = get_template_directory()."/assets/img/member_image/";
	$target_file = $target_dir . basename($_FILES["photo"]["name"]);
	$uploadOk = 1;
	$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
	// Check file size
	if ($_FILES["photo"]["size"] > 500000) {
		return "Sorry, your file is too large. Image size should be less than 500KB";
		$uploadOk = 0;
	}
	// Allow certain file formats
	if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
		return "Sorry, only JPG, JPEG & PNGfiles are allowed.";
		$uploadOk = 0;
	}
	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) {
		return "Sorry, your file was not uploaded.";
		// if everything is ok, try to upload file
	} else {
		$newImageFile = $user_login.'.'.$imageFileType;
		if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_dir.$newImageFile)) {
			return;
		} else {
			return "Sorry, there was an error uploading your file.";
		}
	}
}

function get_member_list() {
    $college = $_POST['college'];
    $hscyear = $_POST['hscyear'];
    
    global $wpdb;
    $table_user = $wpdb->prefix . "users";
    $table_user_details = $wpdb->prefix . "users_details";
    $table_user_meta = $wpdb->prefix . "usermeta";
    $table_college = $wpdb->prefix . "college";
    
    $sql = "SELECT * FROM `$table_user` 
            INNER JOIN `$table_user_meta` ON (`$table_user`.`ID` = `$table_user_meta`.`user_id`)
            INNER JOIN `$table_user_details` ON (`$table_user`.`ID` = `$table_user_details`.`user_id`) 
            INNER JOIN `$table_college` ON (`$table_user_details`.`college` = `$table_college`.`serial_no`) 
            WHERE `$table_user_meta`.`meta_value` LIKE '%subscriber%'";
    if ( $college != "" ) $sql .= " AND `$table_user_details`.`college` = $college " ;
    if ( $hscyear != "" ) $sql .= " AND `$table_user_details`.`hsc_year` = $hscyear " ;
    
    $results = $wpdb->get_results($sql);
    return $results;
}


function member_payment_details ($user_login) {
	$details= array();
	$user_details = get_user_list($user_login);
	$due_amount = get_due_amount($user_details);
	$details["due_amount"] = $due_amount;
	$details["quarter"]= substr($user_details->payment_upto, -2);
	$details["year"]= substr($user_details->payment_upto, 0, 4);
	$details["payment_upto"]= $user_details->payment_upto;
	return $details;
}

function get_corrent_quarter ($current_month) {
	$current_quarter = get_quarter ($current_month);
	return date("Y")."Q".$current_quarter;
}

function get_quarter ($current_month) {
	if ($current_month >= 1 && $current_month <= 3) {
		return 1;
	} elseif ($current_month >= 4 && $current_month <= 6) {
		return 2;
	} elseif ($current_month >= 7 && $current_month <= 9) {
		return 3;
	} elseif ($current_month >= 10 && $current_month <= 12) {
		return 4;
	} else {
		return 0;
	}
}

//Return starting number number of a quarter e.g. Q1/Q2/Q3/Q4
function get_start_month_from_quarter ($quart)
{
  if ($quart == "Q1") return 1;
  if ($quart == "Q2") return 4;
  if ($quart == "Q3") return 7;
  if ($quart == "Q4") return 10;
}

/*
 *  Return the number of months between current date to the paid till $year_quart (e.g. 2020Q1)  
 */
function get_num_months ($nextPayQ)
{
  $today_y4 = date ("Y");
  $today_mn = date ("m");
  $from_y4 = substr ($nextPayQ, 0, 4);
  $from_mn = get_start_month_from_quarter (substr ($nextPayQ, -2));
  //
  $num_months = ((($today_y4 - $from_y4) * 12) + $today_mn) - $from_mn + 1;
  //
  //Logs - total number of months
  //
  return $num_months; 
}

function get_due_amount ($user_details) {
	$results = get_due_quarter_list($user_details);
	if (!empty($results)) {
		$member_status = $user_details->status;
		$total_subs_fee = 0;
		$total_late_fee = 0;
		$total_abroad_fee = 0;
		foreach ($results as $value) {
			$total_subs_fee += $value->subscription_fee;
			$total_late_fee += total_late_fee ($value);
			$total_abroad_fee += total_abroad_fee ($value) ;
		}
		//logs - $total_late_fee, $total_subs_fee
		$total_due_amount = $total_subs_fee + $total_late_fee;
		
		if ($member_status == "Abroad") {
		  return $total_abroad_fee;
		} else {
		  return $total_due_amount;
		}
	} else {
		return 0;
	}
}

function get_due_amount_break_down_list ($user_details) {
	$results = get_due_quarter_list($user_details);
	$list = array();
	$total_subs_fee = 0;
	$total_late_fee = 0;
	foreach ($results as $value) {
		$total_late_fee = total_late_fee ($value);
		$list[$value->quarter_year]["general_fee"] = $total_late_fee + $value->subscription_fee;
		$list[$value->quarter_year]["abroad_fee"] = total_abroad_fee ($value) ;
	}
	return $list;
}

function total_late_fee ($list) {
	$pm_late_fee = (($list->subscription_fee * $list->late_fee) / 100);
	$total_late_fee = ($pm_late_fee * get_num_months ($list->quarter_year));
	return $total_late_fee;
}

function total_abroad_fee ($list) {
	return ($list->subscription_fee * $list->abroad_fee) / 100 ;
}

function get_due_quarter_list($user_details) {
	global $wpdb;
	$member_status = $user_details->status;
	$paid_quarter = $user_details->payment_upto;
	if ($member_status == "Active" || $member_status == "Suspended" ||
	$member_status == "User" || $member_status == "Abroad") {
		;
		//logs - member_status: $member_status proceed to calculate the fees
	} else {
		return 0;
	}
	if ($user_details->subcription_required != 'Y') return 0;
	//
	$current_month = date("m");
	$current_quarter = get_corrent_quarter ($current_month);
	//
	$table_quarter_setup = $wpdb->prefix . "subs_fees";
	//
	$sql = "SELECT `subscription_fee`, `late_fee`, `quarter_year`, `abroad_fee`
	FROM `$table_quarter_setup`
	WHERE `quarter_year` > '$paid_quarter'  AND `quarter_year` <= '$current_quarter'
	ORDER BY `quarter_year`";
	$results = $wpdb->get_results($sql);
	return $results;
}
function get_advanced_quarter_list($current_quarter="") {
	if (empty($current_quarter)) {
		$current_quarter= get_corrent_quarter (date("m"));
	}
	global $wpdb;
	$table_quarter_setup = $wpdb->prefix . "subs_fees";
	$sql = "SELECT * FROM $table_quarter_setup WHERE `quarter_year` > '$current_quarter' ORDER BY `quarter_year` ASC";
	$results = $wpdb->get_results($sql);
	return $results;
}

function  get_months_quarter_position($month) {
	$quarter_map = array("Jan"=> 1,"Feb"=> 2,"Mar"=> 3,"Apr"=> 1,"May"=> 2,"Jun"=> 3,"Jul"=> 1,"Aug"=> 2,"Sep"=> 3,"Oct"=> 1,"Nov"=> 2,"Dec"=> 3);
	return $quarter_map[$month];
}

function update_payment($responseArray, $user_login, $paid_till) {
	global $wpdb;
	$user = get_user_by( 'login', $user_login );
	$receipt_number = $responseArray["id"];
	$tr = get_tr_details_by_receipt_number ($receipt_number);
	$category = $tr->category;
	$description = $responseArray["description"];
	if (!empty($paid_till) && $category == constant("SUBSFEE")) {
		$error = check_quarter_status($paid_till, $user->user_login , $afterPayment = true, $responseArray["amount"]);
		if(!empty($error)){
			website_log($error.$user->user_login.'_'.$afterPayment);
			add_aduit_log(MEMBER, $user->user_login, $user->user_login, $error);
		}
	}
	if (empty($error) && !empty($paid_till) && $category == constant("SUBSFEE")) {
		$users_details = $wpdb->prefix . "users_details";
		$sql = "UPDATE $users_details SET payment_upto='$description' WHERE user_login = '$user->user_login'";
		$wpdb->query($wpdb->prepare($sql));
		if (!empty($wpdb->last_error)) {
			sql_log($wpdb->last_query, $wpdb->last_error);
		}
	}
	$amount = $responseArray["amount"];
	$ipAddress = $responseArray["device.ipAddress"];
	$status = $responseArray["result"];
	$full_response = json_encode($responseArray);
	$payment_type = "";
	if ($responseArray["payment_type"] == "SSL") {
		$payment_transaction["id"] = $responseArray ["bank_tran_id"];
		$payment_type = $responseArray["payment_type"];
	} else {
		$payment_transaction = get_transaction ($responseArray);
		$payment_type = "EBL";
	}
	$payment_reference = !empty($payment_transaction) ? $payment_transaction["id"] : '';
	$users_transaction = $wpdb->prefix . "transaction";
	$sql = "UPDATE $users_transaction SET amount=$amount, payment_type='$payment_type', status='$status',received_by='online', payment_reference='$payment_reference', ip_address='$ipAddress', full_response='$full_response' WHERE receipt_number='$receipt_number'";
	$wpdb->query($wpdb->prepare($sql));
	if (!empty($wpdb->last_error)) {
		sql_log($wpdb->last_query, $wpdb->last_error);
	}
	$amount_format = number_format($amount, 2);
	$_SESSION["receipt_number"] = $receipt_number;
	if ($category != constant("SUBSFEE")) {
		$data = array();
		$data ["description"] = $description;
		$data ["user_login"] = $user_login;
		$data ['receipt_number'] = $receipt_number;
		$result = prepare_product_items ($data);
		send_purchase_product_email ($result, $data, $amount_format, $category);
		wp_redirect( get_permalink(PRODUCT_CONFIRM));
	} else {
		$headers = array('Content-Type: text/html; charset=UTF-8');
		if (constant("CC_EMAIL")) {
			$headers[]= 'Cc: '.constant("CC_EMAIL");
		}
		$message = "<p>Dear $user->display_name,</p>
					<h2>Thank you for the payment.</h2>
					<p>We have received BDT $amount_format. Receipt number is $receipt_number.</p>
					<p>&nbsp;</p>
					<p>With thanks,</p>
					<p>Cadet College Club Limited <p>";
		wp_mail($user->user_email, "Payment Confirmation for $user->display_name", $message, $headers);
		wp_redirect( get_permalink(210));
	}
}

function get_tr_details_by_receipt_number ($receipt_number) {
	global $wpdb;
	$table_transaction = $wpdb->prefix . "transaction";
	$sql = "SELECT * FROM $table_transaction WHERE receipt_number = '$receipt_number'";
	$result = $wpdb->get_row($sql);
	return $result;
}

function get_member_list_by_profession() {
    $profession =     $_POST['profession'];
    $organization =   $_POST['organization'];
    $designation =    $_POST['designation'];
    $specialisation = $_POST['specialisation'];
    
    global $wpdb;
    $table_user = $wpdb->prefix . "users";
    $table_user_details = $wpdb->prefix . "users_details";
    $table_user_meta = $wpdb->prefix . "usermeta";
    $table_college = $wpdb->prefix . "college";
    $table_profession = $wpdb->prefix . "profession";
    
    $sql = "SELECT * FROM `$table_user`
            INNER JOIN `$table_user_meta` ON (`$table_user`.`ID` = `$table_user_meta`.`user_id`)
            INNER JOIN `$table_user_details` ON (`$table_user`.`ID` = `$table_user_details`.`user_id`)
            INNER JOIN `$table_college` ON (`$table_user_details`.`college` = `$table_college`.`serial_no`)
            INNER JOIN `$table_profession` ON (`$table_user_details`.`profession` = `$table_profession`.`ID`)
            WHERE `$table_user_meta`.`meta_value` LIKE '%subscriber%'";
            
    if ( $profession != "" ) $sql .= " AND `$table_user_details`.`profession` = '$profession' ";
    if ( $organization != "" ) $sql .= " AND `$table_user_details`.`organization` LIKE '%$organization%' ";
    if ( $designation != "" ) $sql .= " AND `$table_user_details`.`designation` LIKE '%$designation%' ";
    if ( $specialisation != "" ) $sql .= " AND `$table_user_details`.`specialisation` LIKE '%$specialisation%' ";
    $results = $wpdb->get_results($sql);
    return $results;
}

function get_member_list_blood_group() {
    $bloodgrp = $_POST['bloodgrp'];
    
    global $wpdb;
    $table_user = $wpdb->prefix . "users";
    $table_user_details = $wpdb->prefix . "users_details";
    $table_user_meta = $wpdb->prefix . "usermeta";
    $table_college = $wpdb->prefix . "college";
    
    $sql = "SELECT * FROM `$table_user`
            INNER JOIN `$table_user_meta` ON (`$table_user`.`ID` = `$table_user_meta`.`user_id`)
            INNER JOIN `$table_user_details` ON (`$table_user`.`ID` = `$table_user_details`.`user_id`)
            INNER JOIN `$table_college` ON (`$table_user_details`.`college` = `$table_college`.`serial_no`)
            WHERE `$table_user_meta`.`meta_value` LIKE '%subscriber%'";
    if ( $bloodgrp != "" ) $sql .= " AND `$table_user_details`.`blood_group` = '$bloodgrp' " ;
    
    $results = $wpdb->get_results($sql);
    return $results;
}

function get_member_list_date_of_anniversary() {
    $dateOfBirth =    $_POST['dateOfBirth'];
    if ( $dateOfBirth != "" ) $dateOfBirth = date("Y-m-d", strtotime($dateOfBirth));
    $anniversary =    $_POST['anniversary'];
    if ( $anniversary != "" ) $anniversary = date("Y-m-d", strtotime($anniversary));
    
    global $wpdb;
    $table_user = $wpdb->prefix . "users";
    $table_user_details = $wpdb->prefix . "users_details";
    $table_user_meta = $wpdb->prefix . "usermeta";
    $table_college = $wpdb->prefix . "college";
    
    $sql = "SELECT * FROM `$table_user`
            INNER JOIN `$table_user_meta` ON (`$table_user`.`ID` = `$table_user_meta`.`user_id`)
            INNER JOIN `$table_user_details` ON (`$table_user`.`ID` = `$table_user_details`.`user_id`)
            INNER JOIN `$table_college` ON (`$table_user_details`.`college` = `$table_college`.`serial_no`)
            WHERE `$table_user_meta`.`meta_value` LIKE '%subscriber%'";
    if ($dateOfBirth != "") $sql .= " AND `$table_user_details`.`date_of_birth` = '$dateOfBirth' " ;
    if ($anniversary != "") $sql .= " AND `$table_user_details`.`anniversary` = '$anniversary' " ;
    
    $results = $wpdb->get_results($sql);
    return $results;
}

function get_member_list_by_all() {
    $mmbrId =         add_padding($_POST['mmbrId']);
    $fullname =       $_POST['fullname'];
    $cadetname =      $_POST['cadetname']; 
    $slctMbrType =    $_POST['slctMbrType'];
    $slctMbrStts =    $_POST['slctMbrStts'];
    $txtemail =       $_POST['txtemail'];
    $txtPhone =       str_replace(' ', '', $_POST['txtPhone']);
    $college =        $_POST['college'];
    $hscyear =        $_POST['hscyear'];
    $batch =          $_POST['batch'];
    $bloodgrp =       $_POST['bloodgrp'];
    $profession =     $_POST['profession'];
    $organization =   $_POST['organization'];
    $designation =    $_POST['designation'];
    $specialisation = $_POST['specialisation'];
    $postalAddrs =    $_POST['postalAddrs'];
    $homeAddr =       $_POST['homeAddr'];
    $officeAddr =     $_POST['officeAddr'];
    
    $dateOfBirth =    $_POST['dateOfBirth'];
    if ( $dateOfBirth != "" ) $dateOfBirth = date("Y-m-d", strtotime($dateOfBirth));
    $anniversary =    $_POST['anniversary'];
    if ( $anniversary != "" ) $anniversary = date("Y-m-d", strtotime($anniversary));
    
    global $wpdb;
    $table_user = $wpdb->prefix . "users";
    $table_user_details = $wpdb->prefix . "users_details";
    $table_user_meta = $wpdb->prefix . "usermeta";
    $table_college = $wpdb->prefix . "college";
    $table_type = $wpdb->prefix . "member_type";
    $table_profession = $wpdb->prefix . "profession";
    
    $sql = "SELECT *
            FROM `$table_user`
            INNER JOIN `$table_user_meta` ON (`$table_user`.`ID` = `$table_user_meta`.`user_id`)
            INNER JOIN `$table_user_details` ON (`$table_user`.`user_login` = `$table_user_details`.`user_login`)
            INNER JOIN `$table_college` ON (`$table_user_details`.`college` = `$table_college`.`serial_no`)
            INNER JOIN `$table_profession` ON (`$table_user_details`.`profession` = `$table_profession`.`ID`)
            INNER JOIN `$table_type` ON (`$table_user_details`.`type` = `$table_type`.`code`)
            WHERE `$table_user_meta`.`meta_value` LIKE '%subscriber%'";
    
    if ( $mmbrId != "" )         $sql .= " AND `$table_user`.`user_login` = '$mmbrId' " ;
    if ( $fullname != "" )       $sql .= " AND `$table_user`.`display_name` LIKE '%$fullname%' " ;
    if ( $cadetname != "" )      $sql .= " AND `$table_user_details`.`cadet_name` LIKE '%$cadetname%' " ;
    if ( $slctMbrType != "" )    $sql .= " AND `$table_user_details`.`type` = '$slctMbrType' " ;
    if ( $slctMbrStts != "" )    $sql .= " AND `$table_user_details`.`status` = '$slctMbrStts' " ;
    if ( $txtPhone != "" )       $sql .= " AND `$table_user_details`.`mobile_number` LIKE '%$txtPhone%' " ;
    if ( $txtemail != "" )       $sql .= " AND `$table_user`.`user_email` = '$txtemail' " ;
    if ( $college != "" )        $sql .= " AND `$table_user_details`.`college` = $college " ;
    if ( $batch != "" )          $sql .= " AND `$table_user_details`.`batch` = $batch " ;
    if ( $bloodgrp != "" )       $sql .= " AND `$table_user_details`.`blood_group` = '$bloodgrp' " ;
    if ( $hscyear != "" )        $sql .= " AND `$table_user_details`.`hsc_year` = $hscyear " ;
    if ( $profession != "" )     $sql .= " AND `$table_user_details`.`profession` = '$profession' ";
    if ( $organization != "" )   $sql .= " AND `$table_user_details`.`organization` LIKE '%$organization%' ";
    if ( $designation != "" )    $sql .= " AND `$table_user_details`.`designation` LIKE '%$designation%' ";
    if ( $specialisation != "" ) $sql .= " AND `$table_user_details`.`specialisation` LIKE '%$specialisation%' ";
    if ( $dateOfBirth != "" )    $sql .= " AND `$table_user_details`.`date_of_birth` = '$dateOfBirth' " ;
    if ( $anniversary != "" )    $sql .= " AND `$table_user_details`.`anniversary` = '$anniversary' " ;
    if ( $postalAddrs != "" )    $sql .= " AND `$table_user_details`.`postal_address` = '$postalAddrs' ";
    if ( $homeAddr != "" )       $sql .= " AND `$table_user_details`.`home_address` LIKE '%$homeAddr%' ";
    if ( $officeAddr != "" )     $sql .= " AND `$table_user_details`.`office_address` LIKE '%$officeAddr%' ";
    
    $results = $wpdb->get_results($sql);
    return $results;
}

function create_invoice($user_login, $amount) {
	global $wpdb;
	$table_transaction = $wpdb->prefix . "transaction";
	$invoice_prefix = $_POST['receiveType'].date("YmdHi");
	$invoice = get_invoce_number($invoice_prefix);
	$data = array();
	$data['user_login'] = $user_login;
	$data['amount'] = $amount;
	$data['receipt_number'] = $invoice;
	$data['status'] = "PROCESS";
	$data['category'] = $_POST['category'];
	if ($_POST['productid']) {
		$data['product_id'] = $_POST['productid'];
	}
	if ($_POST['order']['description']) {
		$data['description'] = $_POST['order']['description'];
	}
	if ($wpdb->insert( $table_transaction, $data )) {
		return $invoice;
	} else {
		sql_log($wpdb->last_query, $wpdb->last_error);
		return;
	}
}

function get_invoce_number($invoice_prefix) {
	global $wpdb;
	$auto_number = 001; 
	$table_transaction = $wpdb->prefix . "transaction";
	$sql = "SELECT SUBSTRING(max(`$table_transaction`.`receipt_number`), -3) as maxid FROM `$table_transaction`
			WHERE `$table_transaction`.`receipt_number` LIKE '$invoice_prefix%'
			ORDER BY `$table_transaction`.`receipt_number` DESC";
	$maxid = $wpdb->get_row($sql);
	if (!empty($maxid)) {
		$auto_number = str_pad($maxid->maxid+1, 3, "0", STR_PAD_LEFT);
	}
	$invoice = $invoice_prefix.$auto_number;
	return $invoice;
}

function update_payment_status($status, $receipt_number, $full_response) {
	global $wpdb;
	$users_transaction= $wpdb->prefix . "transaction";
	$full_response= json_encode($full_response);
	$sql = "UPDATE $users_transaction SET status='$status', full_response='$full_response' WHERE receipt_number='$receipt_number'";
	$wpdb->query($wpdb->prepare($sql));
	if (!empty($wpdb->last_error)) {
		sql_log($wpdb->last_query, $wpdb->last_error);
	}
}

function create_loader() {
	$html= "";
	$html .='<div class="modal fade" id="loader" role="dialog">';
	$html .=	'<div class="modal-dialog">';
	$html .=		'<div class="modal-content" align="center">';
	$html .=			'<div class="modal-body">';
	$html .=				'<img src="'.get_template_directory_uri() . '/assets/img/loader.gif'.'" title="loader" alt="loader">';
	$html .=			'</div>';
	$html .=		'</div>';
	$html .=	'</div>';
	$html .='</div>';
	return $html;
}

function update_member($user_login = "") {
	check_ajax_referer( 'update-member-nonce', 'security' );
	$member_email = $_POST['txtemail'];
	$message = "";
	$status = "";
	if (empty($user_id)) {
		$user = wp_get_current_user();
		$user_id = $user->ID;
		$user_login = $user->user_login;
	}
	if ( !isPhone($_POST['txtPhone'], $user_login) ) {
	    if (is_email( $member_email)) {
	    	$update_data = "";
	    	global $wpdb;
	        $userdata = array();
	        $userdata['ID'] = $user_id;
	        $userdata['user_email'] = $member_email;
	        if (!empty($_POST['txtname'])) {
	            $userdata['display_name'] = $_POST['txtname'];
	        }
	        $e_result = get_change_information("user_email", $member_email, $user->user_login, "Email address changed");
	        if (!empty($e_result)) {
	        	$update_data .= $e_result;
	        }
	        $result = wp_update_user( $userdata );
	        if (is_wp_error($result)) {
				$message = $result->get_error_message();
				sql_log($wpdb->last_query, $message);
				$status= "warning";
	        }
	    } else {
	        $status = "warning";
	        $message = "Invalid email address!!";
			website_log("$message. ($member_email)");
	         
	    }
	} else {
	    $status = "warning";
	    $message = "Duplicate mobile number";
	    website_log("$message. (".$_POST['txtPhone'].")");
	} 

	if (empty($status)) {
		global $wpdb;
		$table_users_details= $wpdb->prefix . "users_details";
		$data = prepare_member_data();
		$where = [ 'user_login' => $user_login];
		$e_result = get_change_information("mobile_number", $data["mobile_number"], $user->user_login, "Mobile number changed");
		if (!empty($e_result)) {
			$update_data .= empty($update_data) ? $e_result : ". $e_result";
		}
		$wpdb->update($table_users_details, $data, $where);
		if ($wpdb->last_error) {
			$status = "warning";
			$message = $wpdb->last_error;
			sql_log($wpdb->last_query, $wpdb->last_error);
		} else {
			add_aduit_log(MEMBER, $user_login, "", "Member (".$userdata['display_name'].") updated own profile. $update_data");
			$status = "success";
			$message = "Member record updated successfully";
		}
	}
	echo json_encode(array('status'=>$status, 'message'=>__($message)));
	die();
}

function get_paymnet_type_list() {
	global $wpdb;
	$table_payment_type = $wpdb->prefix . "payment_type";
	$sql = "SELECT * FROM $table_payment_type";
	$results = $wpdb->get_results($sql);
	return $results;
}

function image_processing() {
	$user = wp_get_current_user();
	$result = member_image_upload($user->user_login, $newImageFile);
	if (!empty($result)) {
		$status = "error";
		website_log("$result. $user->user_login.'_'. $newImageFile");
	} else {
	    global $wpdb;
        $table_users_details = $wpdb->prefix . "users_details";
        $data = array();
        if ($_FILES['photo']['size'] != 0) {
	        $data['image'] = $newImageFile;
	    }
	    $data['updated_by'] = $user->user_login;
	    $where = [ 'user_login' => $user->user_login];
	    if ($wpdb->update($table_users_details, $data, $where)) {
	        $status = "success";
	        $result = "Image updated successfully";
	    } else {
	        $status= "error";
	        $result = $wpdb->last_error;
	        sql_log($wpdb->last_query, $wpdb->last_error);
	    }
	}
	echo json_encode(array('status'=>$status, 'message'=>__($result)));
	die();
}
function get_membership_report_on_Payment() {
	global $wpdb;
    $srcByMbrId = add_padding($_POST['srcByMbrId']);
    $table_user = $wpdb->prefix . "users";
    $table_user_details = $wpdb->prefix . "users_details";
    $table_user_meta = $wpdb->prefix . "usermeta";
    $table_member_type = $wpdb->prefix . "member_type";
    $table_member_college = $wpdb->prefix . "college";
    
    $sql = "SELECT * FROM `$table_user`
            INNER JOIN `$table_user_meta` ON (`$table_user`.`ID` = `$table_user_meta`.`user_id`)
            INNER JOIN `$table_user_details` ON (`$table_user`.`user_login` = `$table_user_details`.`user_login`)
            INNER JOIN `$table_member_type` ON (`$table_user_details`.`type` = `$table_member_type`.`code`)
            INNER JOIN `$table_member_college` ON (`$table_user_details`.`college` = `$table_member_college`.`serial_no`)
            WHERE `$table_user_meta`.`meta_value` LIKE '%subscriber%'";
    if ($srcByMbrId != "") $sql .= " AND `$table_user`.`user_login` = '$srcByMbrId' " ;
    $results = $wpdb->get_results($sql);
    return $results;
}

function get_membership_collection_report($all = "") {
	if (($_POST['slctCat'] && $_POST['slctCat'] == "SE") ||  (!$_POST['slctCat'] && $all == "SE")) {
		$service_or_event = true;
	} else {
		$category = $_POST['slctCat']; 
	}
	$product = $_POST['slctProd'];
    $clctnFrom = $_POST['clctnFrom'];
    $member_id = $_POST['mmbrId'];
    if ($clctnFrom == "") $clctnFrom = date('d-M-Y', strtotime('-7 days'));
    if ( $clctnFrom != "" ) $clctnFrom = date("Y-m-d", strtotime($clctnFrom));
    $clctnTo =  $_POST['clctnTo'];
    if ($clctnTo == "") $clctnTo = date("d-M-Y");
    if ( $clctnTo != "" ) $clctnTo = date("Y-m-d", strtotime($clctnTo));
    global $wpdb;
    $table_user = $wpdb->prefix . "users";
    $table_user1 = $wpdb->prefix . "users1";
    $table_user_details = $wpdb->prefix . "users_details";
    $table_user_meta = $wpdb->prefix . "usermeta";
    $table_transactions = $wpdb->prefix . "transaction";
    $table_payment_type = $wpdb->prefix . "payment_type";
    $table_products = $wpdb->prefix . "products";
    
    $sql = "SELECT 
            `$table_transactions`.`payment_date`,
			`$table_transactions`.`receipt_number`,
			`$table_transactions`.`amount`,
            `$table_transactions`.`payment_reference`,
            `$table_transactions`.`category`,
            `$table_user_details`.`member_full_id`,
			`$table_user`.`display_name` as member_name,
            `$table_user1`.`display_name` as receiver_name,
			`$table_payment_type`.`name` as payment_type,
			`$table_products`.`name` as product_name
            FROM $table_transactions
            INNER JOIN `$table_payment_type` ON (`$table_transactions`.`payment_type` = `$table_payment_type`.`id`)
            INNER JOIN `$table_user` ON (`$table_transactions`.`user_login` = `$table_user`.`user_login`)
            LEFT JOIN `$table_user` `$table_user1` ON (`$table_transactions`.`received_by` = `$table_user1`.`user_login`)
            INNER JOIN `$table_user_meta` ON (`$table_user`.`ID` = `$table_user_meta`.`user_id`)
            INNER JOIN `$table_user_details` ON (`$table_transactions`.`user_login` = `$table_user_details`.`user_login`)
            LEFT OUTER JOIN `$table_products` ON (`$table_transactions`.`product_id` = `$table_products`.`product_id`)
            WHERE `$table_user_meta`.`meta_value` LIKE '%subscriber%'
            AND `$table_transactions`.`status` = 'SUCCESS'";
    
    if ($clctnFrom != "") $sql .= " AND `$table_transactions`.`payment_date` >= '$clctnFrom 00:00:01' " ;
    if ($clctnTo != "") $sql .= " AND `$table_transactions`.`payment_date` <= '$clctnTo 23:59:59' " ;
    if ($category != "") $sql .= " AND `$table_transactions`.`category` = '$category' " ;
    if ($product != "") $sql .= " AND `$table_transactions`.`product_id` = '$product' " ;
    if ($member_id != "") $sql .= " AND `$table_user`.`user_login` = '$member_id' " ;
    if ($service_or_event) $sql .= " AND (`$table_products`.`category` = 'Event' or `$table_products`.`category` = 'Service')";
	$sql .= " ORDER BY `$table_transactions`.`payment_date` DESC";
    $results = $wpdb->get_results($sql);
    return $results;
}

function receive_payment() {
	if (empty($_POST['description']))return "Empty quarter";
	$user= wp_get_current_user();
	$userLogin = add_padding($_POST['userLogin']);
	if (wp_check_password( $_POST['password'], $user->user_pass, $user->ID )) {
		$error = check_quarter_status($_SESSION["paid_till"], $userLogin);
		if (!empty($error)) {
			website_log($error.$_SESSION["paid_till"].'_'.$userLogin);
			return $error;
		} 
		$invoice_prefix= $_POST['receiveType'].date("YmdHi");
		$data = array();
		$data['user_login'] = $userLogin;
		$data['amount'] = $_POST['amount'];
		$data['receipt_number'] = get_invoce_number($invoice_prefix);
		$data['payment_type'] = $_POST['paymentType'];
		$data['received_by'] = $user->user_login;
		$data['status'] = "SUCCESS";
		$data['payment_reference'] = $_POST['paymentRef'];
		$data['ip_address'] = get_the_user_ip();
		$data['category'] = $_POST['category'];
		global $wpdb;
		$table_transaction = $wpdb->prefix . "transaction";
		if ($wpdb->insert( $table_transaction, $data )) {
			$users_details = $wpdb->prefix . "users_details";
			$description = $_POST['description'];
			$sql = "UPDATE $users_details SET payment_upto='$description' WHERE user_login=".(int)$data['user_login'];
			$wpdb->query($wpdb->prepare($sql));
			if (!empty($wpdb->last_error)) {
				sql_log($wpdb->last_query, $wpdb->last_error);
			}
			$table_user = $wpdb->prefix . "users";
			$sql = "SELECT id , user_login, user_email, user_pass, display_name FROM $table_user
					WHERE $table_user.`user_login` = ".$data['user_login'];
			$result = $wpdb->get_row($sql);
			$amount_format = number_format($_POST['amount'], 2);
			$headers = array('Content-Type: text/html; charset=UTF-8');
			if (constant("CC_EMAIL")) {
				$headers[]= 'Cc: '.constant("CC_EMAIL");
			}
			$message = "<p>Dear $result->display_name,</p>
						<h2>Thank you for the payment.</h2>
						<p>We have received BDT $amount_format. Receipt number is ".$data['receipt_number'].".</p>
						<p>&nbsp;</p>
						<p>With thanks,</p>
						<p>Cadet College Club Limited <p>";
			wp_mail($result->user_email, "Payment Confirmation for $result->display_name", $message, $headers);
			return;
		} else {
			sql_log($wpdb->last_query, $wpdb->last_error);
			return $wpdb->last_error;
		}
	} else {
		return "Incorrect password";
	}
}
function custom_date($date_format, $val) {
	if (empty($val) || $val<=0) {
		return;
	} else {
		return date($date_format, strtotime($val));
	}
}

function wpb_sender_email( $original_email_address ) {
	return SENDER_EMAIL;
}
 
// Function to change sender name
function wpb_sender_name( $original_email_from ) {
	return SENDER_NAME;
}
// Hooking up our functions to WordPress filters 
add_filter( 'wp_mail_from', 'wpb_sender_email' );
add_filter( 'wp_mail_from_name', 'wpb_sender_name' );


function get_quarter_details($year_quater) {
	if (empty($year_quater))return;
	$quater= substr($year_quater, -2);
	$year= substr($year_quater, 0,4);
	return get_quarters_month($quater)." ".$year;
}
function get_quarters_month($quater, $sort_format = true) {
	if ($sort_format) {
		$month =["Q1"=>"Mar","Q2"=>"Jun","Q3"=>"Sep","Q4"=>"Dec"];
	} else {
		$month =["Q1"=>"March","Q2"=>"June","Q3"=>"September","Q4"=>"Decmber"];
	}
	return $month[$quater];
}

function get_membership_payment_history() {
    $user = wp_get_current_user();
    global $wpdb;
    $table_user = $wpdb->prefix . "users";
    $table_user1 = $wpdb->prefix . "users1";
    $table_user_details = $wpdb->prefix . "users_details";
    $table_user_meta = $wpdb->prefix . "usermeta";
    $table_transactions = $wpdb->prefix . "transaction";
    $table_payment_type = $wpdb->prefix . "payment_type";
    
    $sql = "SELECT
            `$table_transactions`.`payment_date`,
			`$table_transactions`.`receipt_number`,
			`$table_transactions`.`amount`,
            `$table_transactions`.`payment_reference`,
            `$table_user_details`.`member_full_id`,
			`$table_user`.`display_name` as member_name,
            `$table_user1`.`display_name` as receiver_name,
			`$table_payment_type`.`name` as payment_type
            FROM $table_transactions
            INNER JOIN `$table_payment_type` ON (`$table_transactions`.`payment_type` = `$table_payment_type`.`id`)
            INNER JOIN `$table_user` ON (`$table_transactions`.`user_login` = `$table_user`.`user_login`)
            LEFT JOIN `$table_user` `$table_user1` ON (`$table_transactions`.`received_by` = `$table_user1`.`user_login`)
            INNER JOIN `$table_user_meta` ON (`$table_user`.`ID` = `$table_user_meta`.`user_id`)
            INNER JOIN `$table_user_details` ON (`$table_transactions`.`user_login` = `$table_user_details`.`user_login`)
            WHERE `$table_user_meta`.`meta_value` LIKE '%subscriber%'
            AND `$table_transactions`.`status` = 'SUCCESS'
            AND `$table_user`.`ID` = $user->ID 
            ORDER BY `$table_transactions`.`payment_date` DESC";
    
    $results = $wpdb->get_results($sql);
    return $results;
}

function send_password_phone($userLogin) {
	global $wpdb;
	$table_user = $wpdb->prefix . "users";
	$sql = "SELECT id , user_email, user_pass, user_login, display_name FROM $table_user
	WHERE $table_user.`user_login` = '$userLogin'";
	$result = $wpdb->get_row($sql);
	if (@count($result)>0) {
		$password = wp_generate_password( 6, true, false);
		wp_set_password( $password, $result->id );
		$user_details = get_user_list($result->user_login);
		$message = "ওয়েবসাইটে লগইন করার জন্য আপনার পাসওয়ার্ডঃ
		\n
		$password
		\n
		ক্যাডেট কলেজ ক্লাব লিমিটেড।";
		$response = send_sms($user_details->mobile_number, convertBanglatoUnicode($message));
		if (!empty($response->SMSINFO->REFERENCEID)) {
			add_aduit_log(MEMBER, $result->user_login, $result->user_login, "Changing password sent by SMS ($user_details->mobile_number). Referance ID : ".$response->SMSINFO->REFERENCEID);
			add_aduit_log(MEMBER, $result->user_login, $result->user_login, "Get password from " . get_the_user_ip());
			return true;
		} else {
			return false;
		}
	} else {
		return false;
	}
}
	
function send_sms($phone_number, $msg) {
	$user = USER;
	$pass = PASSWORD;
	$sid = SID;
	$uniqId= uniqid();
	$param = "user=$user&pass=$pass&sms[0][0]= $phone_number &sms[0][1]=".urlencode($msg)."&sms[0][2]=$uniqId&sid=$sid";
	$crl = curl_init();
	curl_setopt($crl,CURLOPT_SSL_VERIFYPEER,FALSE);
	curl_setopt($crl,CURLOPT_SSL_VERIFYHOST,2);
	curl_setopt($crl,CURLOPT_URL,URL);
	curl_setopt($crl,CURLOPT_HEADER,0);
	curl_setopt($crl,CURLOPT_RETURNTRANSFER,1);
	curl_setopt($crl,CURLOPT_POST,1);
	curl_setopt($crl,CURLOPT_POSTFIELDS,$param);
	$response = curl_exec($crl);
	curl_close($crl);
	$response = new SimpleXMLElement($response, 1);
	return $response;

}

function isPhone($phoneno, $user_login) {
    $found = false;
    global $wpdb;
    $phoneno = str_replace(' ', '', $phoneno);
    $phoneno = substr($phoneno,-10);
    $table_users_details = $wpdb->prefix . "users_details";
    $sql = "SELECT count(*) FROM `$table_users_details` WHERE `$table_users_details`.`mobile_number` LIKE '%$phoneno%'";
    if ($user_login != "") $sql .= " AND `$table_users_details`.`user_login` != '$user_login' " ;
    $count = $wpdb->get_var($sql);
    if ($count > 0) $found = true;
    return $found;
}

function getUserDetails() {
    $user = wp_get_current_user();
    $userData = ( array ) $user->data;
    $userLogin = $userData['user_login'];
    $roles = ( array ) $user->roles;
    
    global $wpdb;
    $table_user = $wpdb->prefix . "users";
    $sql = "SELECT * FROM `$table_user`	WHERE `$table_user`.`user_login` = '$userLogin'";
    $result = $wpdb->get_row($sql);

    $html .= '<div class="row">';
    if ($roles[0] == "subscriber") {
        $html .= '<p class="name">Member ID: '.$result->user_login.' <br>Name: '.$result->display_name.'</p>';
    } else {
        $html .= '<p class="name">User ID: '.$result->user_login.' <br>Name: '.$result->display_name.'</p>';
    }
    $html .= '</div>';
    
    return $html;
}

function get_audit_log($category) {
    $auditFrom =  $_POST['auditFrom'];
    if ($auditFrom == "") $auditFrom = date('d-M-Y', strtotime('-7 days'));
    if ( $auditFrom != "" ) $auditFrom = date("Y-m-d", strtotime($auditFrom));
    
    $auditTo =  $_POST['auditTo'];
    if ($auditTo == "") $auditTo = date("d-M-Y");
    if ( $auditTo != "" ) $auditTo = date("Y-m-d", strtotime($auditTo));
    
    
    global $wpdb;
    $table_user = $wpdb->prefix . "users";
    $table_user1 = $wpdb->prefix . "users1";
    $table_audit_log = $wpdb->prefix . "audit";
    
    $sql = "SELECT *,
			`$table_user`.`user_login` as user_id,
            `$table_user1`.`user_login` as member_id
            FROM $table_audit_log
            LEFT JOIN `$table_user` ON (`$table_audit_log`.`user_login` = `$table_user`.`user_login`)
            LEFT JOIN `$table_user` `$table_user1` ON (`$table_audit_log`.`member_login` = `$table_user1`.`user_login`)";
    
    if ($auditFrom != "") $sql .= " WHERE `$table_audit_log`.`created_on` >= '$auditFrom 00:00:01' " ;
    if ($auditTo != "") $sql .= " AND `$table_audit_log`.`created_on` <= '$auditTo 23:59:59' " ;
    if ($category != "") $sql .= " AND `$table_audit_log`.`category` = '$category' " ;
    $sql .= " ORDER BY `$table_audit_log`.`created_on` DESC";
    $results = $wpdb->get_results($sql);
    return $results;
}

function add_aduit_log($category, $member_login, $user_login, $note) {
	global $wpdb;
	$table_audit= $wpdb->prefix . "audit";
	$data = array();
	$data['category'] = $category;
	$data['member_login'] = $member_login;
	$data['user_login'] = $user_login;
	$data['note'] = $note;
	$data['ip_address'] = get_the_user_ip();
	if ($wpdb->insert( $table_audit, $data )) {
		return;
	} else {
		sql_log($wpdb->last_query, $wpdb->last_error);
		return $wpdb->last_error;
	}
}

function get_transaction ($tr) {
	$transaction = array();
	for ($i = 0; $i < 10; $i++) {
		if (!empty($tr["transaction[$i].transaction.acquirer.transactionId"])) {
			$transaction ["id"] = $tr["transaction[$i].transaction.acquirer.transactionId"];
			$transaction ["result"] = $tr["transaction[$i].result"];
			$transaction ["authorizationCode"] = $tr["transaction[$i].transaction.authorizationCode"];
			$transaction ["receipt"] = $tr["transaction[$i].transaction.receipt"];
		} else {
			break;
		}
	}
	return $transaction;
}

function get_change_information($old_data_key, $new_data, $user_login, $text) {
	$old_data = get_user_list($user_login);
	if (trim($new_data) != trim($old_data->$old_data_key)) {
		return "$text (".$old_data->$old_data_key." to ".$new_data.")";
	} else {
		return false;
	}
}

function check_quarter_status($orig_quarter, $user_login, $afterPayment = false , $amount = 0) {
	$user_details = get_user_list($user_login);
	if ($user_details->payment_upto == $orig_quarter) {
		return "";
	} else {
		if ($afterPayment) {
			$amount_format = number_format($amount, 2);
			return "Conflict in paid till date. Payment received BDT $amount_format upto $orig_quarter";
		} else {
			return "Please try again";
		}
	}
}

add_action('init', 'start_session', 1);

function start_session() {
	if (!session_id()) {
		session_start();
	}
}

function change_password() {
	check_ajax_referer( 'change-password-nonce', 'security' );
	$oldPass = $_POST['oldpassword'];
	$new_password = $_POST['newpassword'];
	$user = wp_get_current_user();
	$userLogin = $user->user_login;
	if (wp_check_password($oldPass, $user->user_pass, $user->id)) {
		wp_set_password( $new_password, $user->id );
		add_aduit_log(MEMBER, $userLogin, $userLogin, "$user->display_name changed password");
		$status = "success";
		$message = "Password changed successfully, redirecting...";
		if ( $user->roles{0}  == 'contributor' || $user->roles{0}  == 'author' ) {
			$redirecturl = get_permalink(65);
		} else {
			$redirecturl = home_url();
		}
		$redirecturl = add_query_arg( 'changePass', 'success', $redirecturl );
		echo json_encode(array('status'=>$status, 'redirecturl'=>$redirecturl, 'message'=>__($message)));
		die();
	} else {
		$status = "warning";
		$message = "Existing password mismatched. You can logout and reset password.";
		website_log("$message($userLogin)");
		echo json_encode(array('status'=>$status, 'message'=>__($message)));
		die();
	}
}

function mask($str, $first, $last) {
	$len = strlen($str);
	$toShow = $first + $last;
	return substr($str, 0, $len <= $toShow ? 0 : $first).str_repeat("*", $len - ($len <= $toShow ? 0 : $toShow)).substr($str, $len - $last, $len <= $toShow ? 0 : $last);
}

function isMemberCanPay ($user_details) {
	if ($user_details->status == "Discontinued" || $user_details->status == "Expired") {
		return false;
	} else {
		return true;
	}
}

function convertBanglatoUnicode($banglaText) {
	$unicodeBanglaTextForSms = strtoupper(bin2hex(iconv('UTF-8', 'UCS-2BE', $banglaText)));
	return $unicodeBanglaTextForSms;
}


function r2m_login($user_login) {
	$user_login = add_padding($user_login);
	$user = get_user_by( 'login', $user_login );
	if ($user)
	{
		$info = array();
		$info['user_login'] = $user_login;
		$info['remember'] = true;
		$user_details = get_user_list($info['user_login']);
		if (!empty($user_details) &&  $user_details->status == 'Discontinued') {
			echo json_encode(array('status'=>false, 'message'=>__('You are not allowed to login. Please contact CCCL office.')));
			die();
		}

		wp_clear_auth_cookie();
		wp_set_current_user ( $user->ID );
		wp_set_auth_cookie  ( $user->ID, true );

		if ($user->roles{0}  ==  'contributor' || $user->roles{0}  ==  'author'  ) {
			$redirecturl = get_permalink(33);
		} else {
			$redirecturl = get_permalink(31);
		}
		wp_safe_redirect( $redirecturl );
		exit();
	} else {
		echo json_encode(array('status'=>false, 'message'=>__('Wrong username or password.')));
		exit();
	}
	die();
}

function add_padding($user_login) {
	return !empty($user_login) ? str_pad($user_login, 5, "0", STR_PAD_LEFT): '';
}

function create_product($product) {
	global $wpdb;
	$table_name = $wpdb->prefix . "products";
	if ($wpdb->insert( $table_name, $product )) {
		$user = wp_get_current_user();
		add_aduit_log($product['category'], "", $user->user_login, "Added new product: ". $product['name']." (".$product['product_id'].")" );
		delete_items_by_product_id($product['product_id']);
		if ($product['category'] == constant("VENUE")) {
			delete_items_by_date($product['product_id']);
			create_product_items_venue($product);
			update_items_price($product);
		} else {
			create_product_items($product['product_id']);
		} 
		if (!empty($_POST['tokens'])) {
			create_product_token ($product['product_id']);
		}
		return;
	} else {
		sql_log($wpdb->last_query, $wpdb->last_error);
		return $wpdb->last_error;
	};
}

function create_product_token ($pid) {
	delete_token_by_product_id($pid);
	for ($i = 0; $i < count($_POST['tokens']); $i++) {
		$data = array();
		$data ["product_id"]= $pid;
		$data ["description"]= $_POST['tokens'][$i];
		$data ["token_id"]= $pid.'-'.$i;
		create_token ($data);
	}
}

function delete_token_by_product_id($pid) {
	global $wpdb;
	$where = [ 'product_id' => $pid ];
	$table_name = $wpdb->prefix . "product_tokens";
	$wpdb->delete($table_name,$where);
	if ($wpdb->last_error) {
		sql_log($wpdb->last_query, $wpdb->last_error);
		return $wpdb->last_error;
	} else {
		return;
	}
}

function create_token ($item) {
	global $wpdb;
	$table_product_tokens = $wpdb->prefix . "product_tokens";
	if ($wpdb->insert( $table_product_tokens, $item )) {
		return;
	} else {
		sql_log($wpdb->last_query, $wpdb->last_error);
		return $wpdb->last_error;
	};	
}

function update_product($product) {
	global $wpdb;
	$where = [ 'product_id' => $product['product_id'] ];
	$table_name = $wpdb->prefix . "products";
	$wpdb->update($table_name, $product, $where);
	if ($wpdb->last_error) {
		sql_log($wpdb->last_query, $wpdb->last_error);
		return $wpdb->last_error;
	} else {
		$user = wp_get_current_user();
		add_aduit_log($product['category'], "", $user->user_login, "Update product: ". $product['name']." (".$product['product_id'].")" );
		if ($product['category'] == constant("VENUE")) {
			delete_items_by_date($product['product_id']);
			create_product_items_venue($product);
			update_items_price($product);
		} else {
			delete_items_by_product_id($product['product_id']);
			create_product_items($product['product_id']);
		} 
		if (!empty($_POST['tokens'])) {
			delete_token_by_product_id($product['product_id']);
			create_product_token ($product['product_id']);
		}
		return;
	}
}

function update_items_price ($product) {
	global $wpdb;
	$items = get_items_by_product_id ($product["product_id"]);
	$product_details = get_product_details_by_pid($product["product_id"]);
	$data = array();
	foreach ($items as $item) {
		if ($item->shift == "M") {
			$data['price'] = $product_details->venue_morning_price;
		} elseif ($item->shift == "A") {
			$data['price'] = $product_details->venue_afternoon_price;
		} elseif ($item->shift == "E") {
			$data['price'] =  $product_details->venue_evening_price;
		} else {
			$price = 0;
		}
		$table_items = $wpdb->prefix . "items";
		$where ['product_id'] = $product["product_id"];
		$where ['shift'] = $item->shift;
		$wpdb->update($table_items, $data, $where);
		if (!empty($wpdb->last_error)) {
			sql_log($wpdb->last_query, $wpdb->last_error);
		}
	}
}

function delete_items_by_date($pid) {
	global $wpdb;
	$table_items = $wpdb->prefix . "items";
	for ($i = 0; $i < count($_POST['venue']); $i++) {
		$v_list = explode('#', $_POST['venue'][$i]);
		$year = $v_list [0];
		$shift = $v_list [1];
		$type = $year.'#'.$shift;
		$where ['product_id']= $pid ;
		$where ['item_id']= $pid.'#'.$type;
		$wpdb->delete($table_items, $where);
		if (!empty($wpdb->last_error)) {
			sql_log($wpdb->last_query, $wpdb->last_error);
		}
	}
}
function get_tokens_by_product_id ($pid) {
	global $wpdb;
	$table_product_tokens = $wpdb->prefix . "product_tokens";
	$sql = "SELECT * FROM $table_product_tokens WHERE `product_id`= '$pid'";
	$results = $wpdb->get_results($sql);
	return $results;
}

function get_product_details_by_pid($pid, $check_avalibility = false) {
	global $wpdb;
	$current_date = date("Y-m-d");
	$table_products = $wpdb->prefix . "products";
	$sql = "SELECT * FROM $table_products WHERE `product_id`= '$pid'";
	$result = $wpdb->get_row($sql);
	if (!empty($result) && $check_avalibility) {
		$result->status = check_avail_product_by_date ($result->start_date, $result->end_date, $current_date);
	}
	return $result;
}

function get_product_list_by_category($cid, $current_date='', $check_avalibility = false) {
	global $wpdb;
	$current_date = !empty($current_date) ? $current_date : date("Y-m-d");
	$table_products = $wpdb->prefix . "products";
	$sql = "SELECT * FROM $table_products WHERE `category`= '$cid'";
	$results = $wpdb->get_results($sql);
	if ($check_avalibility) {
		$array_object = new stdClass();
		foreach ($results as $key => $result) {
			$result->status = check_avail_product_by_date ($result->start_date, $result->end_date, $current_date);
			$array_object->$key = $result;
		}
		$results = $array_object;
	}
	return $results;
}

function check_avail_product_by_date ($start_date, $end_date, $current_date, $max_quantity = 1) {
	if ($start_date <= $current_date && $end_date >=$current_date) {
		return true;
	} else {
		return false;
	}
}

function prepare_product_list() {
	$user = wp_get_current_user();
	$product = array();
	if (!empty($_POST['productId'])) {
		$product['product_id'] = $_POST['productId'];
	}
	if (!empty($_POST['productname'])) {
		$product['name'] = $_POST['productname'];
	}
	if (!empty($_POST['description'])) {
		$product['description'] = $_POST['description'];
	}
	if (!empty($_POST['startDate'])) {
		$product['start_date'] = custom_date("Y-m-d", $_POST['startDate']);
	}
	if (!empty($_POST['endDate'])) {
		$product['end_date'] = custom_date("Y-m-d", $_POST['endDate']);
	}
	if (!empty($_POST['eventDate'])) {
		$product['event_date'] = custom_date("Y-m-d", $_POST['eventDate']);
	}
	if (!empty($_POST['location'])) {
		$product['location'] = $_POST['location'];
	}
	if (!empty($_POST['prommoCode'])) {
		$product['promo_code'] = $_POST['prommoCode'];
	}
	if (!empty($_POST['promoValidTill'])) {
		$product['promo_valid_till'] = custom_date("Y-m-d", $_POST['promoValidTill']);
	}
	if (isset($_POST['discount'])) {
		$product['discount'] = $_POST['discount'];
	}
	if (isset($_POST['vat'])) {
		$product['vat'] = $_POST['vat'];
	}
	if (isset($_POST['serviceCharge']) ) {
		$product['service_charge'] = $_POST['serviceCharge'];
	}
	if (isset($_POST['paymentOption'])) {
		$product['payment_option'] = $_POST['paymentOption'];
	}
	if (isset($_POST['paymentAmount'])) {
		$product['payment_amount'] = $_POST['paymentAmount'];
	}	
	if (!empty($_POST['category'])) {
		$product['category'] = $_POST['category'];
	}
	if (isset($_POST['venue_morning_price'])) {
		$product['venue_morning_price'] = $_POST['venue_morning_price'];
	}
	if (isset($_POST['venue_afternoon_price'])) {
		$product['venue_afternoon_price'] = $_POST['venue_afternoon_price'];
	}
	if (isset($_POST['venue_evening_price'])) {
		$product['venue_evening_price'] = $_POST['venue_evening_price'];
	}
	if (isset($_POST['emi_option'])) {
		$product['emi_option'] = $_POST['emi_option'];
	}
	$product['updated_by'] = $user->user_login;
	return $product;
}

function create_product_items($pid) {
	for ($i = 0; $i < count($_POST['passCatg']); $i++) {
		if (empty($_POST['passCatg'][$i])) continue;
		$data = array();
		$data ["product_id"]= $pid;
		$data ["type"]= $_POST['passCatg'][$i];
		$data ["price"]= $_POST['passPrice'][$i];
		if ($_POST['person'][$i]) {
			$data ["person"]= $_POST['person'][$i];
		}
		if (isset($_POST['quantiy'][$i])) {
			$data ["max"]= $_POST['quantiy'][$i];
		}
		$data ["item_id"]= $pid.'-'.$i;
		create_items ($data);
	}
}

function create_product_items_venue($product) {
	$pid = $product['product_id'];
	$venue_morning_price = $product['venue_morning_price'];
	$venue_afternoon_price = $product['venue_afternoon_price'];
	$venue_evening_price = $product['venue_evening_price'];
	for ($i = 0; $i < count($_POST['venue']); $i++) {
		$v_list = explode('#', $_POST['venue'][$i]); 
		$day = $v_list [0];
		$shift = $v_list[1];
		$max_quantity = $v_list[2];
		if ($shift == "M") {
			$price = $venue_morning_price;
		} elseif ($shift == "A") {
			$price = $venue_afternoon_price;
		} elseif ($shift == "E") {
			$price = $venue_evening_price;
		} else {
			$price = 0;
		}
		$data = array();
		$data ["product_id"]= $pid;
		$data ["type"] = $_POST['venue'][$i];
		$data ["price"] = $price;
		$data ["shift"] = $shift;
		$data ["max"] = $max_quantity;
		$data ["item_id"]= $pid.'#'.$day.'#'.$shift;
		create_items ($data);
	}
}

function create_items ($item) {
	global $wpdb;
	$table_name = $wpdb->prefix . "items";
	if ($wpdb->insert( $table_name, $item )) {
		return;
	} else {
		sql_log($wpdb->last_query, $wpdb->last_error);
		return $wpdb->last_error;
	};
}

function delete_items_by_product_id($pid) {
	global $wpdb;
	$where = [ 'product_id' => $pid ];
	$table_name = $wpdb->prefix . "items";
	$wpdb->delete($table_name,$where);
	if ($wpdb->last_error) {
		sql_log($wpdb->last_query, $wpdb->last_error);
		return $wpdb->last_error;
	} else {
		return;
	}
}

function get_items_by_product_id ($pid, $check_avalibility = false) {
	global $wpdb;
	$table_items = $wpdb->prefix . "items";
	$sql = "SELECT * FROM $table_items WHERE `product_id`= '$pid'";
	$results = $wpdb->get_results($sql);
	if ($check_avalibility) {
		$array_object = new stdClass();
		foreach ($results as $key => $result) {
			$result->status = $result->max > 0 && $result->price >= 0 ? true : false;
			$array_object->$key = $result;
		}
		$results = $array_object;
	}
	return $results;
}

function generate($content, $view = "") {
	$dir = WP_CONTENT_DIR . '/uploads/qrcodes/';
	$dir_uri = content_url()."/uploads/qrcodes/";
	$filename = $dir."$content.png";
	QRcode::png($content, $filename, QR_ECLEVEL_L, 3);
	if($view == "server") {
		$path = $dir."$content.png";
	}else {
		$path = $dir_uri."$content.png";
	}
	return $path;
}

function get_products() {
	$category = $_POST['category'];
	global $wpdb;
	$table_products = $wpdb->prefix . "products";
	$sql = "SELECT product_id, name FROM $table_products";
	$sql .= " WHERE `$table_products`.`category` = '$category' " ;
	$results = $wpdb->get_results($sql);
	echo json_encode(array('status'=>"success", 'products' => $results));
	die();
}

function check_promo_code() {
	$promoCode = $_POST['promoCode'];
	global $wpdb;
	$table_products = $wpdb->prefix . "products";
	$sql = "SELECT promo_code, discount, promo_valid_till FROM $table_products";
	$sql .= " WHERE `$table_products`.`promo_code` = '$promoCode' " ;
	$result = $wpdb->get_row($sql);
	$current_date = date("Y-m-d");
	if ($result->promo_code == $promoCode  && $result->promo_valid_till >= $current_date) {
		echo json_encode(array('status'=>"success", 'products' => $result, 'message' => $result->discount.'% discount applied'));
	} else {
		$msg = "Invalid code. Please check for any typing errors.";
		website_log("$msg ($promoCode)");
		echo json_encode(array('status' => "Failed", 'products' => $result, 'message' => $msg));
	}
	die();
}

function get_sales_report_data($is_tmp_rpt) {
	$category =  $_POST['slctCat'];
	$product =  $_POST['slctProd'];
	$saleFrom =  $_POST['saleFrom'];
	if ($is_tmp_rpt) {
		if ($saleFrom == "") $saleFrom = date("d-M-Y");		
	}else {
		if ($saleFrom == "") $saleFrom = date('d-M-Y', strtotime('-7 days'));
	}
	if ( $saleFrom != "" ) $saleFrom = date("Y-m-d", strtotime($saleFrom));
	
	$saleTo =  $_POST['saleTo'];
	
	if ($is_tmp_rpt) {
		if ($saleTo == "") $saleTo = date('d-M-Y', strtotime('+1 month'));
	}else {
		if ($saleTo == "") $saleTo = date("d-M-Y");
	}
	if ( $saleTo != "" ) $saleTo = date("Y-m-d", strtotime($saleTo));
	
	global $wpdb;
	$table_purches_item =  $is_tmp_rpt ? $wpdb->prefix . "temporary_hold" : $wpdb->prefix . "purches_item";
	$table_products = $wpdb->prefix . "products";
	$table_items = $wpdb->prefix . "items";
	$table_users = $wpdb->prefix . "users";
	
	$sql = "SELECT *,
	`$table_products`.`name` as product_name,
	`$table_products`.`category` as product_category,
	`$table_users`.`display_name`,
	((`$table_purches_item`.`service_charge` / 100) * (`$table_purches_item`.`amount` * `$table_purches_item`.`quantity`)) as ser_charge,
	((`$table_purches_item`.`vat` / 100) * (`$table_purches_item`.`amount` * `$table_purches_item`.`quantity`)) as vat_amount,
	((`$table_purches_item`.`discount` / 100) * (`$table_purches_item`.`amount` * `$table_purches_item`.`quantity`)) as discount_amount,
	(`$table_purches_item`.`amount` * `$table_purches_item`.`quantity`) as t_amount
	FROM $table_purches_item
	INNER JOIN `$table_users` ON (`$table_purches_item`.`user_login` = `$table_users`.`user_login`)
	INNER JOIN `$table_products` ON (`$table_purches_item`.`product_id` = `$table_products`.`product_id`)
	INNER JOIN `$table_items` ON (`$table_purches_item`.`item_id` = `$table_items`.`item_id`)";
	if ($is_tmp_rpt) {
		if ($saleFrom != "") $sql .= " WHERE `$table_purches_item`.`booking_date` >= '$saleFrom 00:00:00' " ;
		if ($saleTo != "") $sql .= " AND `$table_purches_item`.`booking_date` <= '$saleTo 00:00:00' " ;
	} else {
		if ($saleFrom != "") $sql .= " WHERE `$table_purches_item`.`purchase_date` >= '$saleFrom 00:00:01' " ;
		if ($saleTo != "") $sql .= " AND `$table_purches_item`.`purchase_date` <= '$saleTo 23:59:59' " ;
	} 

	if ($category != "") $sql .= " AND `$table_products`.`category` = '$category' " ;
	if ($product != "") $sql .= " AND `$table_products`.`product_id` = '$product' " ;
	$sql .= " ORDER BY `$table_purches_item`.`purchase_date` DESC";
	$results = $wpdb->get_results($sql);
	return $results;
}

function process_purchase_product() {
	if (empty($_POST['description']))return "Empty description";
	global $wpdb;
	$user= wp_get_current_user();
	$userLogin = add_padding($_POST['userLogin']);
	if (wp_check_password( $_POST['password'], $user->user_pass, $user->ID )) {
		$invoice_prefix = $_POST['receiveType'].date("YmdHi");
		$receipt_number = get_invoce_number($invoice_prefix);
		$_SESSION["receipt_number"] = $receipt_number;
		$data = array();
		$data['user_login'] = $userLogin;
		$data['amount'] = $_POST['amount'];
		$data['receipt_number'] = $receipt_number;
		$data['payment_type'] = $_POST['paymentType'];
		$data['received_by'] = $user->user_login;
		$data['status'] = "SUCCESS";
		$data['payment_reference'] = $_POST['paymentRef'];
		$data['ip_address'] = get_the_user_ip();
		$data['category'] = $_POST['category'];
		$data['description'] = $_POST['description'];
		$data['product_id'] = $_POST['productid'];
		$table_transaction = $wpdb->prefix . "transaction";
		if ($wpdb->insert( $table_transaction, $data )) {
			$result = prepare_product_items ($data);
			$amount_format = number_format($_POST['amount'], 2);
			send_purchase_product_email ($result, $data, $amount_format, $data['category']);
			wp_redirect( get_permalink(PRODUCT_CONFIRM));
		} else {
			sql_log($wpdb->last_query, $wpdb->last_error);
			return $wpdb->last_error;
		}
	} else  {
		return "Incorrect password";
	}
}

function prepare_product_items ($data) {
	global $wpdb;
	$table_purches_item = $wpdb->prefix . "purches_item";
	$items = prepare_data_from_description ($data['description']);
	foreach ($items as $item) {
		$wpdb->insert( $table_purches_item, $item);
		if (!empty($wpdb->last_error)) {
			sql_log($wpdb->last_query, $wpdb->last_error);
		}
	}
	$table_items = $wpdb->prefix . "items";
	foreach ($items as $item) {
 		$sql = "UPDATE `$table_items` SET max = max - $item[quantity]  WHERE item_id = '$item[item_id]'";
		$wpdb->query($wpdb->prepare($sql));
		if (!empty($wpdb->last_error)) {
			sql_log($wpdb->last_query, $wpdb->last_error);
		}
	}
	$table_user = $wpdb->prefix . "users";
	$sql = "SELECT id , user_login, user_email, user_pass, display_name FROM $table_user
			WHERE $table_user.`user_login` = ".$data['user_login'];
	$result = $wpdb->get_row($sql);
	return $result;
}

function send_purchase_product_email ($result, $data, $amount, $category = '') {
	$purchase_list = get_purchase_list_by_receipt_number ($data['receipt_number']);
	$headers = array('Content-Type: text/html; charset=UTF-8');
	if (constant("CC_EMAIL")) {
		$headers[]= 'Cc: '.constant("CC_EMAIL");
	}
	$message = "<p>Dear $result->display_name,</p>
				<h2>Thank you for the payment.</h2>
				<p>We have received BDT $amount. Receipt number is ".$data['receipt_number'].".</p>";
	$message .= purchase_details_html ($purchase_list, $category);
	$message .= "<p>&nbsp;</p>
				<p>With thanks,</p>
				<p>Cadet College Club Limited <p>";
	$file = $data['receipt_number'].".pdf";
	$attachments = array();
	if ($category == constant("SERVICE") || $category == constant("EVENT")) {
		$html = generate_purchase_ticket ($data['receipt_number'], "server");
		$error = pdf_generator ($html, $file);
		if (empty($error)) {
			$attachments[] = WP_CONTENT_DIR . '/uploads/tickets/'.$file;
		}else {
			website_log("$error ($file)");
		}
	}
	wp_mail($result->user_email, "Payment Confirmation for $result->display_name", $message, $headers, $attachments);
	
}


function prepare_data_from_description ($description) {
	$data = array();
	if ($description) {
		$fst_lst = explode("/", $description);
		$snd_lst = explode(",", $fst_lst[5]);
		$i = 0;
		foreach ($snd_lst as $lst) {
			$trd_lst = explode(":", $lst);
			$data[$i]["user_login"] = $fst_lst[0];
			$data[$i]["product_id"] = $fst_lst[1];
			$data[$i]["vat"] = $fst_lst[2];
			$data[$i]["service_charge"] = $fst_lst[3];
			$data[$i]["discount"] = $fst_lst[4];
			$data[$i]["item_id"] = $trd_lst[0];
			$data[$i]["amount"] = $trd_lst[1];
			$data[$i]["person"] = $trd_lst[2];
			$data[$i]["quantity"] = $trd_lst[3];
			$data[$i]["receipt_number"] = $_SESSION["receipt_number"];
			$data[$i]["comment"] = $_SESSION["comment"];
			$i ++;
		}
	}
	return $data;
}

function get_purchase_list_by_receipt_number ($receipt_number) {
	global $wpdb;
	$table_products = $wpdb->prefix . "products";
	$table_transaction = $wpdb->prefix . "transaction";
	$sql = "SELECT
	`$table_transaction`.`receipt_number`,
	`$table_transaction`.`payment_date`,
	`$table_transaction`.`category`,
	`$table_products`.`name`,
	`$table_products`.`location`,
	`$table_products`.`description`,
	`$table_products`.`event_date`,
	`$table_transaction`.`amount`
	FROM
	`$table_transaction`
	INNER JOIN `$table_products` ON (`$table_transaction`.`product_id` = `$table_products`.`product_id`)
	WHERE
	`$table_transaction`.`receipt_number` = '$receipt_number'";
	$result = $wpdb->get_row($sql);
	return $result;
}

function purchase_details_html ($purchase_details, $category = '') {
	$html = "";
	$html .= '<div class="row purchase-details-div">';
	$html .= '<div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0">';
	if ($category == constant("PAYMENT")) {
		$html .= '<h2>Payment Details</h2>';
	} else {
		$html .= '<h2>Purchase Details</h2>';
	}
	$html .= '<table class="table table-responsive table-striped">';
	$html .= '<colgroup>';
	$html .= '<col style="font-weight:bold;">';
	$html .= '<col>';
	$html .= '</colgroup>';
	$html .= '<tbody>';
	$html .= '<tr>';
	$html .= '<td>Receipt No.</td>';
	$html .= "<td>$purchase_details->receipt_number</td>";
	$html .= '</tr>';
	$html .= '<tr>';
	$html .= '<td>Purchase date</td>';
	$html .= "<td>".custom_date("d-M-Y", $purchase_details->payment_date)."</td>";
	$html .= '</tr>';
	$html .= '<tr>';
	$html .= "<td>$purchase_details->category</td>";
	$html .= "<td>$purchase_details->name</td>";
	$html .= '</tr>';
	$html .= '<tr>';
	$html .= '<td>Description</td>';
	$html .= "<td>$purchase_details->description</td>";
	$html .= '</tr>';
	if ($purchase_details->category == "Event") {
		$html .= '<tr>';
		$html .= '<td>Event Date</td>';
		$html .= '<td>'.custom_date("d-M-Y", $purchase_details->event_date).'</td>';
		$html .= '</tr>';
	}
	if ($purchase_details->category == constant("EVENT") || $purchase_details->category == constant("SERVICE")) {
		$html .= '<tr>';
		$html .= '<td>Location</td>';
		$html .= '<td>'.$purchase_details->location.'</td>';
		$html .= '</tr>';
	}
	$html .= '<tr>';
	$html .= '<td>Total Price</td>';
	$html .= '<td>'.number_format($purchase_details->amount, 2).' TK</td>';
	$html .= '</tr>';
	$html .= '</tbody>';
	$html .= '</table>';
	$html .= '</div>';
	$html .= '</div>';
	return $html;
}

add_action( 'template_redirect', 'wpse_inspect_page_id' );
function wpse_inspect_page_id() {
	$current_page_id = get_queried_object_id();
}

function get_items_availability ($avil_date, $pid) {
	global $wpdb;
	$date = strtotime($avil_date);
	$no_of_days = 7;
	$results = array();
	if (!empty($pid)) {
		$start_day = date("Y-m-d", strtotime("-1 day", $date));
		$next_days = date("Y-m-d", strtotime("+$no_of_days day", $date));
		$table_items = $wpdb->prefix . "items";
		$sql = "SELECT * FROM `cccl_items`
				WHERE `$table_items`.`item_id` BETWEEN '$pid#$start_day%' AND '$pid#$next_days%' ORDER BY `id`";
		$results = $wpdb->get_results($sql);
	}
	$list_items = array();
	foreach ($results as $result) {
		$r_list = explode("#", $result->item_id);
		$r_date = $r_list[1];
		$list_items[$r_date][]= $result;
	}
	//Check 7 days avilable
	$d_list = get_date_list($date, $no_of_days);
	foreach ($d_list as $d) {
		if (empty($list_items[$d])) {
			@$list_items[$d][]->item_id = "$pid#$d#M#-999";
			@$list_items[$d][]->item_id = "$pid#$d#A#-999";
			@$list_items[$d][]->item_id = "$pid#$d#E#-999";
		}
	}
	ksort($list_items);
	return $list_items;	
}

function get_date_list($avil_date, $no_of_days) {
	$date_list = array();
	for ($i=0; $i < $no_of_days; $i++) {
		$date_list[] = date("Y-m-d", strtotime("+$i day", $avil_date));
	}
	return $date_list;
}

function select_html ($type, $option) {
	$op_list = explode("#", $type);
	$d = $op_list[1];
	$shift = $op_list[2];
	$s_list = array("Not Available" => "-999", "Available" => "1", "Booked" => "0");
	$html = "<td class='shift$shift'>";
	$html .= "<div class='form-group'>";
	$html .= '<select class="form-control inputField availSelFields" name="venue[]">';
	foreach ($s_list as $key =>$value) {
		$selected = $option == $value ? "selected" : "";
		$html .= "<option value='$d#$shift#$value' $selected>$key</option>";
	}
	$html .= '</select>';
	$html .= "</div>";
	$html .= '</td>';
	return $html;
}

function create_vanue_table_html ($items) {
	$html = '<tr>';
	$html .= '<th class="text-right">Date</th>';
	$html .= '<th class="shiftM">Morning</th>';
	$html .= '<th class="shiftA">Afternoon</th>';
	$html .= '<th class="shiftE">Evening</th>';
	$html .= '</tr>';
	foreach ($items as $date => $values) {
	$html .= '<tr class="trdt">';
	$html .= '<td class="text-right dateName">'.custom_date("d-M-Y",$date).'</td>';
		foreach ($values as $val) {
			$html .= select_html ($val->item_id, $val->max);
		}
	$html .= '</tr>';
	}
	echo $html;
}

function  get_items () {
	$avil_date = custom_date("Y-m-d", $_POST['setAvailability']);
	$items = get_items_availability ($avil_date, $_POST['productId']);
	create_vanue_table_html ($items);
}

function check_items_status ($desc) {
	$itm_list = explode('/', $desc);
	$items_details = $itm_list[5];
	$select_items = explode(",", $items_details);
	foreach ($select_items as $s_item) {
		$item = explode(":", $s_item);
		if (!process_check_items_status ($itm_list[1], $item[0], $item[3])) {
			// not avaliable
			return "Something wrong, Please try again";
		}
	}
	return "";
}

function process_check_items_status ($pid, $item_id, $quanity) {
	$items = get_items_by_product_id ($pid, true);
	foreach ($items as $item) {
		if ($item->item_id == $item_id) {
			if (!$item->status || $quanity > $item->max) {
				return false;
			} else {
				return true;
			}
		}
	}
}

/*
 * Log SQL message
 */
function sql_log($sql, $errDesc = "") {
	if (!SQL_DEBUG) return ;
	$log_file = ABSPATH.CMS_LOG_PATH."SQLMSG.LOG_".date("Ymd");
	$fh = fopen($log_file, "a");
	if (!$fh) return;
	fwrite($fh, "\n");
	fwrite($fh, date("Y-m-d H:i:s")."\n");
	fwrite($fh, "Command\n");
	fwrite($fh, "========\n");
	fwrite($fh, "$sql\n");
	if ($errDesc != "") {
		fwrite($fh, "Error\n");
		fwrite($fh, "========\n");
		fwrite($fh, "$errDesc\n");
	}
	fclose($fh);
}

/*
 * Log website message
 */
function website_log($message) {
	if (!WEBMSG_DEBUG) return ;
	$log_file = ABSPATH.CMS_LOG_PATH."WEBMSG.LOG".date("Ymd");
	$fh = fopen($log_file, "a");
	if (!$fh) return;
	fwrite($fh, "\n");
	fwrite($fh, date("Y-m-d H:i:s")."\n");

	fwrite($fh, "Message\n");
	fwrite($fh, "========\n");
	fwrite($fh, "$message\n");
	fclose($fh);
}

function pdf_generator ($html, $file = 'temp.pdf', $pdf_type = 'F') {
	require_once get_template_directory() . '/inc/tcpdf/config/tcpdf_config.php';
	require_once get_template_directory() . '/inc/tcpdf/tcpdf.php';
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('CCCL');
	$pdf->SetTitle('Cadet College Club Limited');
	$pdf->SetSubject('CCCL');
	$pdf->SetKeywords('CCCL');
	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(false);
	$pdf->SetCellPadding(0);
	$pdf->setCellHeightRatio(1);
	// set default monospaced font
 	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	// set margins
	$pdf->SetMargins(3, 5, 5);
	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, 0);
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}
	// set font
	$pdf->SetFont('helvetica', '', 10);
	
	// add a page
	$pdf->AddPage();
	// output the HTML content
	$pdf->writeHTML($html, true, false, true, false, '');
	// reset pointer to the last page
	$pdf->lastPage();
	
	// ---------------------------------------------------------
	ob_end_clean();
	//Close and output PDF document
	if ($pdf_type == "D") {
		$pdf->Output($file, $pdf_type);
	} else {
		$fileNL = WP_CONTENT_DIR . "/uploads/tickets/$file";
		$pdf->Output($fileNL, $pdf_type);
	}
}

function get_purchase_items ($receipt_number) {
	global $wpdb;
	$table_users = $wpdb->prefix . "users";
	$table_products = $wpdb->prefix . "products";
	$table_purches_item = $wpdb->prefix . "purches_item";
	$table_items = $wpdb->prefix . "items";
	$sql = "SELECT 
	`$table_products`.`name`,
	`$table_products`.`event_date`,
	`$table_products`.`location`,
	`$table_products`.`category`,
	`$table_users`.`display_name`,
	`$table_users`.`user_login`,
	`$table_purches_item`.`item_id`,
	`$table_purches_item`.`id`,
	`$table_products`.`description`,
	`$table_products`.`product_id`,
	`$table_items`.`type`,
	`$table_purches_item`.`person`,
	`$table_purches_item`.`quantity`
	FROM
	`$table_purches_item`
	INNER JOIN `$table_users` ON (`$table_purches_item`.`user_login` = `$table_users`.`user_login`)
	INNER JOIN `$table_products` ON (`$table_purches_item`.`product_id` = `$table_products`.`product_id`)
	INNER JOIN `$table_items` ON (`$table_purches_item`.`item_id` = `$table_items`.`item_id`)
	WHERE
	`$table_purches_item`.`receipt_number` = '$receipt_number'";
	$results = $wpdb->get_results($sql);
	return $results;
}

function generate_purchase_ticket ($receipt_number, $view = '') {
	$html = '';
	$results = get_purchase_items ($receipt_number);
	$k = 1;
	$total_result = count($results);
	foreach ($results as $result) {
		$loop = $result->person*$result->quantity;
		$ticket_no = $result->id;
		for ($i = 0; $i < $loop; $i++){
			if ( $i > 0 ) {
				$tkt = $ticket_no.'-'.$i;
			} else {
				$tkt = $ticket_no;
			}
			$html .= ticket_html ($result, $tkt, $view);
			if ($loop == $i+1 && $k == $total_result ) {
				//continue;
			} else {
				$html .= '<div style="page-break-before:always"></div>';
			}
		}
		$k++;
	}
	return $html;
} 

function ticket_html($result, $ticaket_no, $view = ''){
	if($view == "server") {
		$geust_copy_logo_uri = get_template_directory()."/assets/img/guest_copy_logo.png";
		$qr = generate($ticaket_no, $view);
	}else {
		$geust_copy_logo_uri = get_template_directory_uri()."/assets/img/guest_copy_logo.png";
		$qr = generate($ticaket_no, $view);
	}

	$name = $result->name;
	$category = $result->category;
	$event_date = custom_date("d-M-Y", $result->event_date);
	$location = $result->location;
	$display_name = $result->display_name;
	$type = $result->type;
	$description = $result->description;
	$product_id = $result->product_id;
	$user_login = $result->user_login;
	$tokens = get_tokens_by_product_id ($product_id);
	$html = '
	<div class="tickets col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0">
	  <table cellpadding="5" cellspacing="0" style="padding: 10px;border:1px #000 solid;" >
	    <tr>
	      <td colspan="2" align="left" width="70%"><h2>Guest Copy</h2></td>
	      <td class="bdr" width="30%" colspan="2" align="left" style="border-left:dashed 1px solid"><h2>Office Copy</h2></td>
	    </tr>
	    <tr>
	      <td colspan="2"><div align="left"><img src="'.$geust_copy_logo_uri.'" alt="geust_copy_logo" width="237" height="44"/></div></td>
	      <td class="bdr" colspan="2" style="border-left:dashed 1px solid"><div align="left"><img src="'.$geust_copy_logo_uri.'" alt="geust_copy_logo" width="237" height="44"/></div></td>
	    </tr>
	    <tr>
	      <td width="50%" rowspan="2" height="140">
	      	<div><strong>'.$category.' Name: </strong>'.$name.'</div>';
	if ($category == constant("EVENT")) {
		$html.= '<div><strong>Date: </strong>'.$event_date.'</div>';
	}
	$html .= '
	        <div><strong>Location: </strong>'.$location.'</div>
	        <div><strong>Name: </strong>'.$display_name.'</div>
	        <div><strong>Member ID: </strong>'.$user_login.'</div>
	        <div><strong>Type: </strong>'.$type.'</div>
	        <div><strong>Ticket No.: </strong>'.$ticaket_no.'</div>
	       </td>
	      <td width="20%" rowspan="2"><img src="'.$qr.'" alt="qrcode" title="qrcode"></td>
	      <td class="bdr" width="30%" height="44" colspan="2" style="border-left:dashed 1px solid" align="center"><img src="'.$qr.'" alt="qrcode" title="qrcode"></td>
	    </tr>
	    <tr>
	      <td colspan="2" style="border-left:dashed 1px solid" height="100"><div><strong>'.$category.' Name: </strong>'.$name.'</div>';
			if ($category == constant("EVENT")) {
				$html.= '<div><strong>Date: </strong>'.$event_date.'</div>';
			}
	$html .= '
		    <div><strong>Member ID: </strong>'.$user_login.'</div>
	        <div><strong>Ticket No. </strong>: '.$ticaket_no.'</div>
	      </td>
	    </tr>
	    <tr>
	      <td height="40" colspan="2"><strong>Description: </strong>'.$description.'</td>
	      <td class="bdr" colspan="2" rowspan="2" style="border-left:dashed 1px solid"></td>
	    </tr>
	    <tr>
	      <td height="auto" colspan="2"><table>
	          <tr width= "2">';
		if(!empty($tokens)){
			$html.= build_token($tokens);
		}else {
			$html.= '<td  height="60" align="center"><p></P></td>';
		}
	$html .= '</tr>
	        </table></td>
	    </tr>
	  </table>
	</div>';
	return $html;
}

function build_token ($tokens) {
	$html = "";
	foreach ($tokens as $value) {
		$html .= '<td  height="60" align="center"><p>'.$value->description.'</P></td>';
	}
	return $html;
}

function print_tickets () {
	$html = "";
	$results = get_membership_collection_report("SE");
	foreach ($results as $result) {
		$html .= generate_purchase_ticket ($result->receipt_number, 'server');
	}
	pdf_generator ($html, "tickets.pdf", 'D');
}

function get_temporary_hold_list_by_receipt_number ($receipt_number) {
	global $wpdb;
	$table_temporary_hold = $wpdb->prefix . "temporary_hold";
	$table_products = $wpdb->prefix . "products";
	$sql = "SELECT
	`$table_temporary_hold`.`product_id`,
	`$table_temporary_hold`.`item_id`,
	`$table_temporary_hold`.`user_login`,
	`$table_temporary_hold`.`purchase_date`,
	`$table_temporary_hold`.`amount`,
	`$table_temporary_hold`.`person`,
	`$table_temporary_hold`.`quantity`,
	`$table_temporary_hold`.`vat`,
	`$table_temporary_hold`.`service_charge`,
	`$table_temporary_hold`.`discount`,
	`$table_temporary_hold`.`receipt_number`,
	`$table_products`.`name`
	FROM
	`$table_temporary_hold`
	INNER JOIN `$table_products` ON (`$table_temporary_hold`.`product_id` = `$table_products`.`product_id`)
	WHERE
	`$table_temporary_hold`.`receipt_number` = '$receipt_number'";
	$results = $wpdb->get_results($sql);
	return $results;
}

function process_temporary_hold() {
	if (empty($_POST['description'])) return "Empty description";
	global $wpdb;
	$user = wp_get_current_user();
	$userLogin = add_padding($_POST['userLogin']);
	if (wp_check_password( $_POST['password'], $user->user_pass, $user->ID )) {
		$invoice_prefix = $_POST['receiveType'].date("YmdHi");
		$receipt_number = get_invoce_number($invoice_prefix);
		$_SESSION["receipt_number"] = $receipt_number;
		$data = array();
		$data['user_login'] = $userLogin;
		$data['amount'] = $_POST['amount'];
		$data['receipt_number'] = $receipt_number;
		$data['description'] = $_POST['description'];
		$data['updated_by'] = $user->user_login;
		$result = prepare_hold_items ($data);
		$amount_format = number_format($_POST['amount'], 2);
		send_hold_product_email ($result, $data, $amount_format);
 		wp_redirect( get_permalink(TEMP_CONFIRM));
	} else  {
		return "Incorrect password";
	}
}

function prepare_hold_items ($data) {
	global $wpdb;
	$table_temporary_hold = $wpdb->prefix . "temporary_hold";
	$items = prepare_data_from_description ($data['description']);
	foreach ($items as $item) {
		$itemList = explode("#", $item["item_id"]);
		$item['booking_date'] = $itemList[1];
		$item['comment'] = "Hold";
		$item['updated_by'] = $data['updated_by'];
		$wpdb->insert( $table_temporary_hold, $item);
		if (!empty($wpdb->last_error)) {
			sql_log($wpdb->last_query, $wpdb->last_error);
		}
	}
	$table_items = $wpdb->prefix . "items";
	foreach ($items as $item) {
		$sql = "UPDATE `$table_items` SET max = max - $item[quantity]  WHERE item_id = '$item[item_id]'";
		$wpdb->query($wpdb->prepare($sql));
		if (!empty($wpdb->last_error)) {
			sql_log($wpdb->last_query, $wpdb->last_error);
		}
	}
	$table_user = $wpdb->prefix . "users";
	$sql = "SELECT id , user_login, user_email, user_pass, display_name FROM $table_user
			WHERE $table_user.`user_login` = ".$data['user_login'];
	$result = $wpdb->get_row($sql);
	return $result;
}

function send_hold_product_email ($result, $data, $amount) {
	$headers = array('Content-Type: text/html; charset=UTF-8');
	if (constant("CC_EMAIL")) {
		$headers[]= 'Cc: '.constant("CC_EMAIL");
	}
	$message = "<p>Dear $result->display_name,</p>
				<h2>Thank you for the booking.</h2>
				<p>Your due amount BDT $amount. Receipt number is ".$data['receipt_number'].".</p>";
	$message .= venue_temp_html($data['receipt_number']);
	$message .= "<p>&nbsp;</p>
				<p>With thanks,</p>
				<p>Cadet College Club Limited <p>";
	wp_mail($result->user_email, "Temporary booking confirmation for $result->display_name", $message, $headers, $attachments);
}

function venue_temp_html($receipt_number) {
	$results = get_temporary_hold_list_by_receipt_number ($receipt_number);
	$html = "<h2>Booking Details</h2>";
	$html .= "<table class='table table-responsive table-striped'>";
	$html .= "<tr>";
	$html .= "<td><strong>Product</strong></td>";
	$html .= "<td><strong>Details</strong></td>";
	$html .= "</tr>";
	foreach ($results as $value) {
		$html .= "<tr>";
		$html .= "<td>$value->name</td>";
		$html .= "<td>".venue_details($value->item_id)."</td>";
		$html .= "</tr>";
	}
	$html .= "</table>";
	return $html;
}

function venue_details($type) {
	$sft = array("M" => "Morning", "A" => "Afternoon", "E" => "Evening" );
	$lt = explode('#', $type);
	$type = custom_date('d M Y', $lt[1]).', '.  $sft[$lt[2]];
	return $type;
}

function create_modal($id, $title) {
	$html= "";
	$html .="<div class='modal fade' id='$id' role='dialog'>";
	$html .=	'<div class="modal-dialog">';
	$html .=		'<div class="modal-content">';
	$html .=			'<div class="modal-header">';
	$html .=				"<h4 class='modal-title'>$title</h4>";
	$html .=			'</div>';
	$html .=			'<div id="errormessage"></div>';
	$html .=			'<form method="post" id="tempReleaseBooking" autocomplete="off">';
	$html .=			'<div class="modal-body">';
	$html .=				'<div class="row">&nbsp;</div>';
	$html .=				'<div class="row">';
	$html .=					'<div class="col-md-12">';
	$html .=						'<label>Do you want to release following booking?</label>';
	$html .=					'</div>';
	$html .=				'</div>';
	$html .=				'<div class="row">';
	$html .=					'<div class="col-md-12" id="temItemDetails"></div>';
	$html .=				'</div>';
	$html .=				'<div class="row">&nbsp</div>';
	$html .=				'<div class="row">';
	$html .=					'<label class="col-md-3 col-form-label">Password</label>';
	$html .=					'<div class="col-md-4">';
	$html .=						'<input type="password" class="form-control inputField" id="password" name="password" required placeholder="Required" autocomplete="off">';
	$html .=					'</div>';
	$html .=				'</div>';
	$html .=			'</div>';
	$html .=			'<div class="modal-footer">';
	$html .=				'<input type="hidden" name="item_id" id="tmpItemId"/>';
	$html .=				'<input type="hidden" name="tmpId" id="tmpId"/>';
	$html .=				'<button type="button" id="tempReleaseConfirm" class="btn-secondary">Confirm</button>';
	$html .=				'<button type="button" class="btn-secondary" data-dismiss="modal">Close</button>';
	$html .=			'</div>';
	$html .=			'</form>';
	$html .=		'</div>';
	$html .=	'</div>';
	$html .='</div>';
	return $html;
}

function temporary_booking_release () {
	$user = wp_get_current_user();
	if (wp_check_password( $_POST['password'], $user->user_pass, $user->ID )) { 
		global $wpdb;
		$table_temporary_hold = $wpdb->prefix . "temporary_hold";
		$data = array();
		$data['comment'] = "Released";
		$data['updated_by'] = $user->user_login;
		$where = [ 'item_id' => $_POST["tmpItemId"] ];
		$wpdb->update($table_temporary_hold, $data, $where);
		if (!empty($wpdb->last_error)) {
			sql_log($wpdb->last_query, $wpdb->last_error);
		}
		$message = "Booking item has been released";
		echo json_encode(array('status'=>"success", 'message' => $message));
		die();		
	}else {
		$message = "Incorrect password";
		echo json_encode(array('status'=>"warning", 'message' => $message));
		die();
	}
}

function get_due_development_fee ($pid, $member_id) {
	global $wpdb;
	$table_purches_item = $wpdb->prefix . "purches_item";
	$sql = "SELECT SUM(amount) AS paid  FROM $table_purches_item WHERE `product_id`= '$pid' and user_login = '$member_id'";
	$result = $wpdb->get_row($sql);
	return $result->paid;
}

function get_products_by_category($category, $isFixedAmount = false) {
	global $wpdb;
	$table_products = $wpdb->prefix . "products";
	$sql = "SELECT product_id, name FROM $table_products";
	$sql .= " WHERE `$table_products`.`category` = '$category' " ;
	if($isFixedAmount){
		$sql .= " AND `$table_products`.`payment_option` = 'Fixed' " ;
	}
	$results = $wpdb->get_results($sql);
	return $results;

}
function checkMemberStatus($user_details = array()) {
	if (empty($user_details) || ($user_details->status == "Discontinued" || $user_details->status == "Expired" )) {
		return false;
	}else {
		return true;
	}
}
