jQuery(document).ready(function($) {

	$(".addSubFeeBtn").on("click",function() {
		//alert();
		if ($("#addSubscriptionfeeform").valid()) {
			
			// Todo
		}
	});
	
	$("#clearSubFee").on("click",function() {
		$(".addSubFeeBtn").show();
		$(".updateSubFeeBtn").hide();
		$("#slctYear").val("");
		$("#slctQuarter").val("");
		$("#sbcrpnfee").val("");
		$("#latefee").val("");
		$("#abrdfee").val("");
		$("#password").val("");
		$("#dataid").val("");
		$("#task").val("addSubFee");
	});
	
	var subscriptionTable = $('#subscriptionTable').DataTable({
		rowReorder: {
			selector: 'td:nth-child(3)'
		},		
		responsive: true,		
		order : [ [ 0, 'desc' ], [ 1, 'desc' ] ],
        "pageLength": 50,
		"columnDefs" : [  {
			"targets" : 'no-sort',
			"orderable" : false
		}, {
			"targets" : -1,
			"data" : null,
			"defaultContent" : "<a href='#' class='tabeleditbtn'>Edit</a>",
	
		},
         {
            targets: [2,3,4],
            className: 'text-right'
        } ]
	});
	
	if ( subscriptionTable.responsive.hasHidden() ) {
		$('#subscriptionTable tbody').on( 'click', 'a.tabeleditbtn', function (event) {
            event.preventDefault();
			var data = subscriptionTable.row( $(this).parents('tr').prev()[0] ).data();
			var origTr = $(this).parents('tr').prev()[0];
			var dataid = $(origTr).children(":last").attr("dataid");
			$("#dataid").val(dataid);
			$("#slctYear").val(data[0]);
			$("#slctQuarter").val(data[1]);
			$("#sbcrpnfee").val(parseInt(data[2].replace(",","")));
			$("#latefee").val(data[3]);
			$("#abrdfee").val(data[4]);
			$("#task").val("update");
			$(".updateSubFeeBtn").show();
			$(".addSubFeeBtn").hide();
			$("html, body").animate({ scrollTop: 0 }, "slow");
			$(".alert").remove();
			 
		});	
	}
	else {
		$('#subscriptionTable tbody').on( 'click', 'a.tabeleditbtn', function (event) {
            event.preventDefault();
			var data = subscriptionTable.row( $(this).parents('tr') ).data();
			var dataid = $(this).parents("td").attr("dataid");
			$("#dataid").val(dataid);
			$("#slctYear").val(data[0]);
			$("#slctQuarter").val(data[1]);
			$("#sbcrpnfee").val(parseInt(data[2].replace(",","")));
			$("#latefee").val(data[3]);
			$("#abrdfee").val(data[4]);
			$("#task").val("update");
			$(".updateSubFeeBtn").show();
			$(".addSubFeeBtn").hide();
			$("html, body").animate({ scrollTop: 0 }, "slow");
			$(".alert").remove();
			 
		});	
	}
	
	$( "#addSubscriptionfeeform" ).validate({
		rules: {
			sbcrpnfee: {
			required: true,
			number: true,
			},
			latefee: {
				required: true,
				number: true,
				min: 0,
				max: 100,
			},
			abrdfee: {
				required: true,
				number: true,
				min: 0,
				max: 100,
			}
		}
	});

});

