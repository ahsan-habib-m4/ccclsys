jQuery(document).ready(function($) {
    // Show the login dialog box on click
    $('a#show_login').on('click', function(e){
        $('body').prepend('<div class="login_overlay"></div>');
        $('form#login').fadeIn(500);
        $('div.login_overlay, form#login a.close').on('click', function(){
            $('div.login_overlay').remove();
            $('form#login').hide();
        });
        e.preventDefault();
    });

    // Perform AJAX login on form submit
    $('form#login').on('submit', function(e){
        e.preventDefault();
        $('form#login p.status').show().text(ajax_object.loadingmessage);
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajax_object.ajaxurl,
            data: { 
                'action': 'ajaxlogin', //calls wp_ajax_nopriv_ajaxlogin
                'username': $('form#login #username').val(),
                'password': $('form#login #password').val(),
                'rememberme': $('form#login #rememberme').prop( "checked" ),
                'security': $('form#login #security').val() },
            success: function(data){
                $('form#login p.status').text(data.message);
                if (data.loggedin == true){
                   document.location.href = data.redirecturl;
                }
            }
        });
    });


    $('form#changePassword').on('submit', function(e){
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajax_object.ajaxurl,
            data: { 
                'action': 'changepassword', //calls wp_ajax_nopriv_ajaxlogin
                'oldpassword': $('form#changePassword #oldpassword').val(), 
                'newpassword': $('form#changePassword #newpassword').val(),
                'confnewpassword': $('form#changePassword #confnewpassword').val(),
                'security': $('form#changePassword #security').val()}, 
            success: function(data){
				$("#message").empty().show().html(show_message(data.message, data.status)).delay(5000).hide(0);
                if (data.status == 'success'){
                    document.location.href = data.redirecturl;
                }
            }
        });
        e.preventDefault();
     });

	$(".editbtn").on("click", function () {
		var operationMode = $(this).attr("data-mode");
		if (operationMode == "edit") {
			var dataSection = $(this).attr("data-section");
			$(this).attr("data-mode","update");
			$(this).children("span").text("Save");
			$(this).children("i").removeClass("fa-edit");
			$(this).children("i").addClass("fa-save");
			$("."+ dataSection).removeAttr("disabled");
			$("."+ dataSection).siblings(".datepickerIcon").show();
		} else {
			var dataSection = $(this).attr("data-section");
            var clickedElement = $(this);
            if(!$("."+dataSection).valid()) return;
			$("#loader").modal("show");
			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: ajax_object.ajaxurl,
				data: {
					'action': 'updatemember',
					'security': $('form #security').val(),
                    'cadetname': $('form #cadetname').val(),
                    'txtname': $('form #txtname').val(),
					'txtPhone': $('form #txtPhone').val(), 
					'txtemail': $('form #txtemail').val(), 
					'postalAddrs': $('form #postalAddrs').val(),
					'profession': $('form #profession').val(),
					'organization': $('form #organization').val(),
					'designation': $('form #designation').val(),
					'specialization': $('form #specialization').val(),
					'officeAddr': $('form #officeAddr').val(),
					'homeAddr': $('form #homeAddr').val(),
					'dateOfBirth': $('form #dateOfBirth').val(),
					'hscyear': $('form #hscyear').val(),
					'spouse': $('form #spouse').val(),
					'spouseOccu': $('form #spouseOccu').val(),
					'anniversary': $('form #anniversary').val(),
					'child1': $("form input[name='child1']:checked").val(),
					'child1name': $('form #child1name').val(),
					'child1dob': $('form #child1dob').val(),
					'child2': $("form input[name='child2']:checked").val(),
					'child2name': $('form #child2name').val(),
					'child2dob': $('form #child2dob').val(),
				},
				success: function(data){
					$("#loader").modal("hide");
					$("#message").empty().show().html(show_message(data.message, data.status)).delay(5000).hide(0);
					$(clickedElement).attr("data-mode","edit");
					$(clickedElement).children("span").text("Edit");
					$(clickedElement).children("i").removeClass("fa-save");
					$(clickedElement).children("i").addClass("fa-edit");
                    $(clickedElement).blur();
					$("."+ dataSection).attr("disabled", "disabled");
					$("."+ dataSection).siblings(".datepickerIcon").hide();
					$("html, body").animate({ scrollTop: 0 }, "slow");
                    
				}
			});
		}
	});	
	
	$(".photoUpdateBtn").on('click', function(e){
		$obj = $("#photo");
		$("#loader").modal("show");
		file_data = $obj.prop('files')[0];
		form_data = new FormData();
		form_data.append('photo', file_data);
		form_data.append('action', 'imageprocess');
		try {
			$.ajax({
				url: ajax_object.ajaxurl,
				type: 'POST',
				dataType: 'json',
				contentType: false,
				processData: false,
				data: form_data,
				success: function (data) {
					$("#loader").modal("hide");
					$("#message").empty().show().html(show_message(data.message, data.status)).delay(5000).hide(0);
                    $(".photoUpdateBtn, .photoCancelBtn").hide();
				}
			});
		} catch(e) {
			$("#loader").modal("hide");
			$("#message").empty().show().html(show_message(e, 'error')).delay(5000).hide(0);
		}
	});

	$("#setAvailability").on("change", function(e) {
		if ($("#isChange").val()) {
			var msg = "Please save modified items, otherwise you will lose this item. Do you want to continue without saving?"
			if (!confirm(msg)) {
				setTimeout( function(){ 
					$("#setAvailability").val($("#avilabilitySetupTable tbody tr:nth-child(2) td:first-child").text());
				}, 50 );
				return;	
			}else {
				$("#isChange").val("");
			}
		}
		$("#loader").modal("show");
		$.ajax({
			url: ajax_object.ajaxurl,
			type: 'POST',
			dataType: 'html',
	        data: { 
            'action': 'itemsavail',
            'setAvailability': $('form#addEditProductform #setAvailability').val(),
            'productId': $('form#addEditProductform #productId').val(),
        },
			success: function (data) {
				$("#avilabilitySetupTable").empty().html(data);
				$("#loader").modal("hide");
			    $(".availSelFields").on("change", function() {
			    	$("#isChange").val(1);
			    });
			}
		});
	});

	$("#tempReleaseConfirm").on("click",function(e) {
		 if ($("#tempReleaseBooking").valid()){
			 e.preventDefault();
			 $('#tempReleaseConfirm').prop("disabled",true);
			 $itemId = $('form#tempReleaseBooking #tmpItemId').val();
			 $tmpId = $('form#tempReleaseBooking #tmpId').val();
		        $.ajax({
		            type: 'POST',
		            dataType: 'json',
		            url: ajax_object.ajaxurl,
		            data: { 
		            	'action': 'tmprelease', //calls wp_ajax_nopriv_ajaxlogin
		                'username': $('form#tempReleaseBooking #username').val(),
		                'password': $('form#tempReleaseBooking #password').val(),
		                'tmpId': $tmpId,
		                'tmpItemId': $itemId},
		            success: function(data){
		                if (data.status == 'success'){
							$("#message").empty().show().html(show_message(data.message, data.status)).delay(10000).hide(0);
							$("html, body").animate({ scrollTop: 0 }, "slow");
							$('#tmprelease').modal('hide');
							$('.item_'+$tmpId).remove();
							$( "#trid_"+$tmpId ).removeClass( "release");
		                }else {
							$("#errormessage").empty().show().html(show_message(data.message, data.status)).delay(10000).hide(0);
		                }
		                $('#tempReleaseConfirm').prop("disabled",false);
		            }
		        });
		 }
	});
});

function populateProduct(selectedProd) {
	var category = jQuery("#slctCat").val();
	jQuery("#slctProd").find('option').remove();
	jQuery("#slctProd").append("<option value=''>All</option>");
	try {
		jQuery.ajax({
			url: ajax_object.ajaxurl,
			type: 'POST',
			dataType: 'json',
			data: {
				'action': 'getproducts',
				'category': category
			},
			success: function (data) {
				var products = data.products;
				if ( products.length < 1 ) {
					jQuery(".selectProdDiv").hide();
				} else {
					jQuery(".selectProdDiv").show();
				}
				for (var i = 0; i < products.length; ++i) {
					var option;
					if (selectedProd != "" && selectedProd == products[i].product_id) {
						option = "<option selected value='" +products[i].product_id+ "'>"+products[i].name +"</option>";
					} else {
						option = "<option value='" +products[i].product_id+ "'>"+products[i].name +"</option>";
					}
					jQuery('#slctProd').append(option);
					if (selectedProd != "" ) {
						jQuery("#slctProd option").each(function(){
							if (jQuery(this).val() == selectedProd)
								jQuery(this).attr("selected","selected");
						});
					}
				}
			}
		});
	} catch(e) {
		console.log("failed");
	}
}

function show_message(message, type){
	var html= "";
	html += "<div class='row'>";
	html += 	"<div class='col-md-12 col-sm-12'>";
	html += 		"<div class='alert alert-"+type+" alert-dismissible' role='alert'>";
	html +=				message;
	html += 		"<button type='button' class='close' data-dismiss='alert'>&times;</button>";
	html += 		"</div>"
	html += 	"</div>"
	html += "</div>"
	return html;
}

