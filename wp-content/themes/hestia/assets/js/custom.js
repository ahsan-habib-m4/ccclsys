jQuery(document).ready(function($) {
    $('[data-toggle="tooltip"]').tooltip();
    
     $(".logoutTooltips").tooltip({
        html: true
    }); 
    
	$(".addSubFeeBtn").on("click",function() {
		if ($("#addSubscriptionfeeform").valid()) {
			
			// Todo
		}
	});

	$( "#addMemberform" ).validate({
		rules: {
			batch: {
				required: true,
				digits: true,
				maxlength: 3
			},
			cadetno: {
				required: true,
				digits: true,
				maxlength: 5
			},
			txtmemberId: {
				required: true,
				digits: true,
				maxlength: 5
			},
            txtPhone: {
                required: true,
                minlength: 6
            }
		}
	});
	
	
	$(".addMemberBtn").on("click",function() {
		if ($("#addMemberform").valid()) {
			
			// Todo
		}
	});
	
	$("#profPicUpBtn").on("click", function() {
		$("#photo").click();
	})
	
	$('.dateFields').datepicker({
		autoclose: true,
		format: "dd-M-yyyy",
		endDate: '+0d'
	});

	$('.tmpdateFields').datepicker({
		autoclose: true,
		format: "dd-M-yyyy"
	});
	
	$(".btnGroupAddon").on("click", function () {
		$(this).siblings( ".dateFields" ).datepicker("show");
	});

	$(".showHidePass").on('click', function(event) {
		if($(this).siblings(".txt-input").attr("type") == "text"){
			$(this).siblings(".txt-input").attr('type', 'password');
			$(this).children().addClass( "fa-eye" );
			$(this).children().attr('title', 'Show password');
			$(this).children().removeClass( "fa-eye-slash" );
		} else if ($(this).siblings(".txt-input").attr("type") == "password"){
			$(this).siblings(".txt-input").attr('type', 'text');
			$(this).children().removeClass( "fa-eye" );
			$(this).children().attr('title', 'Hide password');
			$(this).children().addClass( "fa-eye-slash" );
		}
	});

	$(".close").on("click",function(){
		$(".alert.alert-dismissible").fadeOut();
	})
	
	$(".getPassBtn").on("click", function() {
		if ($("#getPassForm").valid()) {
			
			// Todo
		}
	})
	
	$(".findMemberBtn").on("click", function() {
		if ($("#findMember").valid()) {
			
			// Todo
		}
	})

	$( "#changePassword" ).validate({
		rules: {
			oldpassword: {
				required: true
			},
			newpassword: {
				required: true,
                minlength: 6
			},
			confnewpassword: {
				required: true,
                minlength: 6,
				equalTo : "#newpassword"
			}
		},
		messages: {
			confnewpassword: "New password and Confirm password are not matching",
		}
	});
	
	$("#srcByMbrIdForm").validate();
	$("#receivePaymentForm, #salesReportForm").validate();
	
    var g_quarterSelection = $("#quarterSelection").val();
	$(".paycheck").on("click", function() {
		var totalAvAmount = 0;
		var totalAmount = 0;
		$(this).parent().prevAll(".form-check").children(".paycheck").prop("checked", true);
		$(this).parent().nextAll(".form-check").children(".paycheck").prop("checked", false);
		calculateTotalAmount ();
		setQuarterSelection(g_quarterSelection);
	})
	$(".advpaycheck").on("click", function() {
		$(".duepaycheck").prop("checked", true);
		calculateTotalAmount ();
		setQuarterSelection(g_quarterSelection);
	})
	$(".duepaycheck").on("click", function() {
		$(".advpaycheck").prop("checked", false);
		calculateTotalAmount ();
		setQuarterSelection(g_quarterSelection);
	})
    
    $("#reportForm").validate();
    $("#reportForm").on("submit", function() {
         $("#searchByAnniversaryMemberReportForm").valid();
    })
    
    $(".photoCancelBtn").on("click", function() {
        $("#photo").val('');
        $("#profileImage").attr("src", $("#orginPhoto").val());
        $(this).hide();
        $(".photoUpdateBtn").hide();
    })
    
    $("#srcByMbrIdBtn").on("click", function(){
        $("#srcByMbrIdForm").valid();
    })
	
    $("#txtmemberId").on("blur", function(){
        var txtmemberId = $(this).val();
        if (txtmemberId != "") txtmemberId = pad(txtmemberId,5);
        $(this).val(txtmemberId);
    })
    $("#cadetno").on("blur", function(){
        var cadetno = $(this).val();
        if (cadetno != "") cadetno = pad(cadetno,5);
        $(this).val(cadetno);
    })
    $("#batch").on("blur", function(){
        var batch = $(this).val();
        if (batch != "") batch = pad(batch,3);
        $(this).val(batch);
    })
    
     $("#username").on("blur", function(){
        var username = $(this).val();
        if (username != "") username = pad(username,5);
        $(this).val(username);
    })
    
     $("#userLogin").on("blur", function(){
        var userLogin = $(this).val();
        if (userLogin != "") userLogin = pad(userLogin,5);
        $(this).val(userLogin);
    })
    
    $("#mmbrId").on("blur", function(){
        var mmbrId = $(this).val();
        if (mmbrId != "") mmbrId = pad(mmbrId,5);
        $(this).val(mmbrId);
    })
    
    $("#srcByMbrId").on("blur", function(){
        var srcByMbrId = $(this).val();
        if (srcByMbrId != "") srcByMbrId = pad(srcByMbrId,5);
        $(this).val(srcByMbrId);
    })
    
    $(".childnone").on("click", function(){
        var childno = $(this).attr("data-childno");
        $("#child"+childno+"name").val("");
        $("#child"+childno+"dob").val("");
    });
    
    $(".auditreportbtn").on("click",function() {
        if ($("#auditLogForm").valid()) {
            
            // Todo
        }
    });
    
    $("#auditLogForm").validate();
    $("#auditreportbtn").on("submit", function() {
         $("#auditLogForm").valid();
    })
    
    $("#slctMbrType").on("change", function() {
        if($('option:selected', this).attr('data-paysub') == "N") {
            $("#paidupto option[value='N/A']").prop('selected', true);
        } else {
            $("#paidupto option").prop('selected', false);
        }
    });
    
    
    $('.edateFields').datepicker({
		autoclose: true,
		format: "dd-M-yyyy"
	});
	
	$(".btnGroupAddon").on("click", function () {
		$(this).siblings( ".edateFields" ).datepicker("show");
	});
	
	$(".tokens").on("blur", function() {
		var wrapper = $('.tokensFields');
		var oldTokenCount = $(".token").length;
		var newTotalToken = $(this).val();
		if (oldTokenCount < newTotalToken) {
			var counter = oldTokenCount + 1;
			for ( counter; counter <= newTotalToken ; counter++) {
				$(wrapper).append(
					"<label for='tokens' class='col-md-3 col-form-label text-right token token"+counter+"'>Token "+counter+"</label>" +
					"<div class='col-md-8 tokenInputContainer token"+counter+"'>" +
						"<div class='form-group is-empty tokenInputsCont'>" +
							"<input type='text' class='form-control inputField tokenInputs' required id='tokens"+counter+"' name='tokens[]'>" +
						"</div>" +
					"</div>"
				);
			}
		} else {
			var counter = oldTokenCount;
			for ( counter; counter > newTotalToken; counter--) {
				$(".token"+counter).remove();
			}
		}
	});
	
	$(".addMoreBtn").on("click", function(e){
		e.preventDefault();
		
		var row = "<tr>" +
				"<td><div class='form-group is-empty'><input type='text' class='form-control inputField passCatg' required></div></td>" +
				"<td><div class='form-group is-empty'><input type='text' class='form-control inputField person' required></div></td>" +
				"<td><div class='form-group is-empty'><input type='text' class='form-control inputField passPrice' required></div></td>" +
				"<td><div class='form-group is-empty'><input type='text' class='form-control inputField quantiy' required></div></td>" +
				"<td class='text-right'><a href='#' class='removeRow'><i class='fa fa-minus'></i></a></td></tr>";
		var wrapper = $('#itemsTable');
		$(wrapper).append(row);
		modifyItemsTable();
	});
	
	$(".removeRow").live("click", function (e) {
		e.preventDefault();
		$(this).parent("td").parent("tr").remove();
		modifyItemsTable();
	});
	
	var validator = $( "#addEditProductform").validate({
        rules: {
        	startDate: {
        		smallerThan: "#endDate"
            },
            endDate: {
                greaterThan: "#startDate"
            }
        }
    });
    $.validator.addMethod('greaterThan', function(value, element) {
        var dateFrom = $("#startDate").val();
        var dateTo = $('#endDate').val();
        if (moment(dateFrom, "DD-MMM-YYYY") > moment(dateTo, "DD-MMM-YYYY")) {
        	return false;
        } else {
        	return true;
        }
    },"End date can not be earlier than start date");
    
    $.validator.addMethod('smallerThan', function(value, element) {
        var dateFrom = $("#startDate").val();
        var dateTo = $('#endDate').val();
        if (moment(dateFrom, "DD-MMM-YYYY") > moment(dateTo, "DD-MMM-YYYY")) {
        	return false;
        } else {
        	return true;
        }
    },"Start date can not be later than end date");
    
	jQuery.validator.addClassRules({
		person: {
			required: true,
			number: true,
			maxlength: 2
		},
		price: {
			required: true,
			number: true
		},
		quantiy: {
			required: true,
			number: true,
			maxlength: 3
		},
		morningShift: {
			required: true,
			number: true
		},
		afternoonShift: {
			required: true,
			number: true
		},
		eveningShift: {
			required: true,
			number: true
		},
		vatamount: {
			required: true,
			number: true,
			maxlength: 5
		},
		serviceCharge: {
			required: true,
			number: true,
			maxlength: 5
		},
		passPrice: {
			required: true,
			number: true
		}
	});

    $("#addEditProductform").on("submit", function() {
         $("#addEditProductform").valid();
    });
    var today = new Date();
    if (!$(".startDate").val()) {
    	$(".startDate").datepicker("update", today);
    }
    if (!$(".endDate").val()) {
    	$(".endDate").datepicker("update", moment(today).add(7, 'days').format('DD-MM-yy'));
    }
    $(".endDate").on("change", function() {
	    $(this).valid();
	    $("#startDate").valid();
    })
     $(".startDate").on("change", function() {
	    $(this).valid();
	    $("#endDate").valid();
    })
    
    $(".addMoreSerivceBtn").on("click", function(e) {
    	e.preventDefault();
		var row = "<tr>" +
				"<td><div class='form-group is-empty'><input type='text' class='form-control inputField passCatg' required></div></td>" +
				"<td><div class='form-group is-empty'><input type='text' class='form-control inputField passPrice' required></div></td>" +
				"<td class='text-right'><a href='#' class='removeServiceRow'><i class='fa fa-minus'></i></a></td></tr>";
		var wrapper = $('#serviceItemsTable');
		$(wrapper).append(row);
		
		modifyServiceItemsTable();
	});
    
    $(".removeServiceRow").live("click", function (e) {
		e.preventDefault();
		$(this).parent("td").parent("tr").remove();
		modifyServiceItemsTable();
	});
    
    $(".paymentOption").on("change", function() {
    	paymentOptionToggle();
    });
    paymentOptionToggle();
    $("#clearAllProduct").on("click",function() {
		$(".addProductBtn").show();
		$(".updateProductBtn").hide();
		$(".inputField").val("");
		$(".trdt").empty();
		$('#productId').prop('readonly', false);
		$("#task").val("add");
	});

    $(".availSelFields").on("change", function() {
    	$("#isChange").val(1);
    });
	$( "#temItemDetails" ).validate({
		rules: {
			password: {
				required: true
			}
		}
	});
});

function paymentOptionToggle(){
    if(jQuery('.paymentOption option:selected').val() == "Fixed") {
        jQuery(".paymentAmount").show();
    } else {
    	jQuery(".paymentAmount").hide();
    }
}

function emiOptionToggle(){
    if(jQuery('.emiOption option:selected').val() == "1") {
        jQuery(".emiPayMerchant").show();
    } else {
    	jQuery(".emiPayMerchant").hide();
    }
}

function modifyItemsTable() {
	var rowCount = 1;
	jQuery('#itemsTable > tbody  > tr').not(":first").each(function() {
		jQuery(this).find("input.passCatg").attr({"id":"passCatg"+rowCount, "name" : "passCatg[]"});
		jQuery(this).find("input.person").attr({"id":"person"+rowCount, "name" : "person[]"});
		jQuery(this).find("input.passPrice").attr({"id":"passPrice"+rowCount, "name" : "passPrice[]"});
		jQuery(this).find("input.quantiy").attr({"id":"quantiy"+rowCount, "name" : "quantiy[]"});
		rowCount++;
	});
}

function modifyServiceItemsTable() {
	var rowCount = 1;
	jQuery('#serviceItemsTable > tbody  > tr').not(":first").each(function() {
		jQuery(this).find("input.passCatg").attr({"id":"passCatg"+rowCount, "name" : "passCatg[]"});
		jQuery(this).find("input.passPrice").attr({"id":"passPrice"+rowCount, "name" : "passPrice[]"});
		rowCount++;
	});
}

function setQuarterSelection(g_quarterSelection) {
	var lastChecked = jQuery(".paycheck:checked:last").val();
    if (lastChecked) {
	   jQuery("#quarterSelection").val(lastChecked);
    } else {
        jQuery("#quarterSelection").val(g_quarterSelection);
    }
}

function createMemberId() {
	var college = jQuery("#college").val();
	var batch = jQuery("#batch").val().trim();
	var cadetno = jQuery("#cadetno").val().trim();
	var slctMbrType = jQuery("#slctMbrType").val().trim();
	slctMbrType = slctMbrType.substring(0, 2);
	var txtmemberId = jQuery("#txtmemberId").val().trim();
	
	if (batch != "") batch = pad(batch,3);
	if (cadetno != "") cadetno = pad(cadetno,5);
	if (txtmemberId != "") txtmemberId = pad(txtmemberId,5);
	
	var fullid = college + "-" + batch + "-" + cadetno + "-" + slctMbrType + "-" + txtmemberId;
	
	jQuery("#mmbrfulId").val(fullid);
	jQuery(".fullId").text(fullid);
}

function pad (str, max) {
	str = str.toString();
	return str.length < max ? pad("0" + str, max) : str;
}

function previewFile(input){
	var file = jQuery("input[type=file]").get(0).files[0];
	if(file){
		var reader = new FileReader();
		reader.onload = function(){
			jQuery("#profileImage").attr("src", reader.result);
		}
		reader.readAsDataURL(file);
	}
    jQuery(".photoCancelBtn, .photoUpdateBtn").show();
}

function numberWithCommas(x) {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function calculateTotalAmount (){
	var totalAmount = 0;
	var totalAvAmount = 0;
	var dueAmmount = 0;
	dueAmmount = jQuery(".dueAmmount").val();
	jQuery( ".paycheck:checked" ).each(function( index ) {
		totalAvAmount = totalAvAmount + parseFloat(jQuery(this).siblings("label").children(".amount").text().replace(",", ""));
	});
	
	totalAmount = parseFloat(dueAmmount) + parseFloat(totalAvAmount);
    totalAmount = totalAmount.toFixed(2);
    jQuery(".totalAmount").text(numberWithCommas(totalAmount));
    jQuery("#paymentAmount").text(numberWithCommas(totalAmount));
    jQuery("#eblTotalAmount").val(totalAmount);
	if (totalAmount > 0) {
		jQuery(".processPaymentBtn").prop("disabled", false);
		jQuery(".receivePaymentBtn").prop("disabled", false);
	} else {
		jQuery(".processPaymentBtn").prop("disabled", true);
		jQuery(".receivePaymentBtn").prop("disabled", true);
	}
}

function releaseTempItems($obj, $item, $tmpId) {
	var product= jQuery($obj).attr("product")+', '+jQuery($obj).attr("item");
	jQuery('#temItemDetails').empty().text(product);
	jQuery('#tmpItemId').empty().val($item);
	jQuery('#tmpId').empty().val($tmpId);
	
	jQuery('#tmprelease').modal('show');
	
}