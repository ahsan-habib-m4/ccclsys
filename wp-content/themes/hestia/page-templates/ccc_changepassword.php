<?php /* Template Name: cccl_change_password */ ?>

<?php 
get_header(); 
add_filter('show_admin_bar', '__return_false');
do_action( 'hestia_before_single_page_wrapper' );
?>
<form id="changePassword" method="post">
	<div class="<?php echo hestia_layout(); ?>">
		<div class="container">
			<div class="row">&nbsp;</div>
			<div id="message"></div>
			<div class="row">
				<div class="change-pass-div">
					<div class="input-group">
						<div class="form-group">
							<input type="password" placeholder="Old Password" name="oldpassword" id="oldpassword"  required class="form-control txt-input">
							<span class="input-group-addon showHidePass"><i title="Show password" class="fa fa-eye"></i></span>	
						</div>
					</div>
					<div class="input-group">
						<div class="form-group">
							<input type="password" placeholder="New Password" name="newpassword" id="newpassword"  required class="form-control txt-input">
							<span class="input-group-addon showHidePass"><i title="Show password" class="fa fa-eye"></i></span>	
						</div>
						
					</div>
					<div class="input-group">
						<div class="form-group">
							<input type="password" placeholder="Confirm New Password" name="confnewpassword" id="confnewpassword"  required class="form-control txt-input">
							<span class="input-group-addon showHidePass"><i title="Show password" class="fa fa-eye"></i></span>		
						</div>
					</div>
					<div class="input-group text-right">
						<input class="submit_button btn btn-primary " type="submit" value="Change password" name="changepass" id="changepass">
						<input type=hidden name="task" value="changePass">
						<?php wp_nonce_field( 'change-password-nonce', 'security' );?>
					</div>
				</div>
			</div>	
		</div>
	</div>
</form>
<?php get_footer(); ?>