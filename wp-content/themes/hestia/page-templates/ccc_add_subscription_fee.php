<?php /* Template Name: cccl_add_subscription_fee */ ?>


<?php get_header(); 
add_filter('show_admin_bar', '__return_false');
do_action( 'hestia_before_single_page_wrapper' );
$user = wp_get_current_user();
?>
<div class="<?php echo hestia_layout(); ?>">
    <div class="container add_subscription_fee_div">
    	<div class="row">
    		<div class="col-md-12">
    		<?php 
    		if($_POST['task'] == "addSubFee") {
    			$error = add_Subscription_fee();
    			if(!empty($error)) {?>
    			<div class="alert alert-danger alert-dismissible" role="alert">
    			  <?php echo $error;?>
                    <button type="button" class="close">
                    	<span aria-hidden="true">&times;</span>
                    </button>
    			</div>			
    			<?php 
    			}else {
    			?>
    			<div class="alert alert-success alert-dismissible" role="alert">
    			  	Record Inserted Successfully !!
                    <button type="button" class="close">
                    	<span aria-hidden="true">&times;</span>
                    </button>
    			</div>
    		<?php 				
    			}
    		}elseif ($_POST['task'] == "update") {
    			$error = update_Subscription_fee();
    			if(!empty($error)) {?>
    			<div class="alert alert-danger alert-dismissible" role="alert">
                    <?php echo $error;?>
                    <button type="button" class="close">
                    	<span aria-hidden="true">&times;</span>
                    </button>
    			</div>			
    			<?php 
    			}else {
    			?>
    			<div class="alert alert-success alert-dismissible" role="alert">
                    Record Updated Successfully !!
                    <button type="button" class="close">
                    	<span aria-hidden="true">&times;</span>
                    </button>
    			</div>
    		<?php 				
    			}
    		}
    		if(!empty($error)) {
    			$sub_year = $_POST["slctYear"];
    			$sub_quarter = $_POST["slctQuarter"];
    			$subcription_fee = $_POST["sbcrpnfee"];
    			$late_fee = $_POST["latefee"];
    			$abrod_fee = $_POST["abrdfee"];
    			$task = $_POST["task"];
    			$dataid = $_POST["id"];
    			
    		}
    		?>
    		</div>
    	</div>
    	
    	
    	<div class="row">
			<div class=" col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
				<div class="row">
					<div class="card card-form">
						<div class="content">
							<div class="row">
								<div class="textwidget">
									<form id="addSubscriptionfeeform" method="post">
                                		<div class="row">
                                			<div class="col-md-8 col-md-offset-2">
                                                <div class="row">
                                                    <label for="slctYear" class="col-md-4 col-form-label">Select Year</label>
                                                    <div class="col-md-8">
                                                      <select class="form-control inputField" id="slctYear" name="slctYear" required onchange="createMemberId()">
                                                      	  <option value="">Required</option>
                                                          <?php 
                                                          $curYear = date("Y");
                                                          $from = $curYear - 5;
                                                          $to = $curYear + 5;
                                                          for ( $i = $from; $i <= $to; $i++) {?>
                                                           <option value="<?php echo $i;?>" <?php echo $sub_year == $i ? "Selected" : ''?>><?php echo $i;?></option>;
                                                          <?php }
                                                          ?> 
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label for="slctQuarter" class="col-md-4 col-form-label">Select Quarter</label>
                                                    <div class="col-md-8">
                                                      	<select class="form-control inputField" id="slctQuarter" name="slctQuarter" required>
                                        					<option value="">Required</option>
                                        					<option value="Q1" <?php echo $sub_quarter == 'Q1' ? "Selected" : ''?>>Q1</option>
                                        					<option value="Q2" <?php echo $sub_quarter == 'Q2' ? "Selected" : ''?>>Q2</option>
                                        					<option value="Q3" <?php echo $sub_quarter == 'Q3' ? "Selected" : ''?>>Q3</option>
                                        					<option value="Q4" <?php echo $sub_quarter == 'Q4' ? "Selected" : ''?>>Q4</option>
                                        				</select>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label for="sbcrpnfee" class="col-md-4 col-form-label">Subscription fee</label>
                                                    <div class="col-md-8">
                                                      <input type="text" class="form-control inputField" id="sbcrpnfee" name="sbcrpnfee" value="<?php echo $subcription_fee;?>" placeholder="Required" required autocomplete="off">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label for="latefee" class="col-md-4 col-form-label">Late fee (%)</label>
                                                    <div class="col-md-8">
                                                      <input type="text" class="form-control inputField" id="latefee" name="latefee" value="<?php echo $late_fee;?>" placeholder="Required" required autocomplete="off">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label for="abrdfee" class="col-md-4 col-form-label">Abroad fee (%)</label>
                                                    <div class="col-md-8">
                                                      <input type="text" class="form-control inputField" id="abrdfee" name="abrdfee" value="<?php echo $abrod_fee;?>" placeholder="Required" required autocomplete="off">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label for="password" class="col-md-4 col-form-label">Password</label>
                                                    <div class="col-md-8">
					                                  <input type="password" class="form-control inputField" id="password" name="password" value="" placeholder="Required" required autocomplete="off">
                                                    </div>
                                                </div>
                                                 <div class="row">
                                                    <div class="col-md-12 text-right">
                                                      <button type="submit" class="btn btn-primary mb-2 addSubFeeBtn" style="<?php echo $error ?  ($task == 'addSubFee') ? 'display: inline-block' : 'display: none' : '';?>">Add</button>
                                                      <button type="submit" class="btn btn-primary mb-2 updateSubFeeBtn" style="<?php echo $error ? ($task =='update') ? 'display: inline-block' : 'display: none' : 'display: none';?>">Update</button>
                                                      <button type="button" class="btn btn-secondary mb-2" id="clearSubFee">Clear</button>
                                                      <input type="hidden" value="<?php echo $error ? $task :'addSubFee'?>" name="task" id="task">
                                                      <input type="hidden" value="<?php echo $error && $task == 'update' ? $dataid : '';?>" name="id" id="dataid">
                                                      <input type="hidden" value="<?php echo $user->ID;?>" name="user_id">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                	</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    	
    	<div class="row get-pass-sec">
        	<div class="col-md-12 col-sm-12 col-xs-12 headerDiv">
             	<h2 class="h2">All Subscription Fees</h2>
             	<hr>
         	</div>
        </div>
    	<div class="row">
    		<div class="col-md-12 col-sm-12 col-xs-12 tableDiv">
        		<table id="subscriptionTable" class="table table-striped datatable">
                    <thead>
                        <tr>
                            <th>Year</th>
                            <th>Quarter</th>
                            <th>Subscription fee</th>
                            <th>Late fee (%)</th>
                            <th>Abroad fee (%)</th>
                            <th class="no-sort"></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                    $results = get_Subscription_fee_list();
                    if(count($results)>0) {
                    	foreach ($results as $value) {?>
                        <tr>
                            <td><?php echo $value->year;?></td>
                            <td><?php echo $value->quater;?></td>
                            <td>
                            	<?php echo number_format($value->subscription_fee,2); ?>
                            </td>
                            <td><?php echo $value->late_fee;?></td>
                            <td><?php echo $value->abroad_fee;?></td>
                            <td dataid="<?php echo $value->id;?>"><a href="#">Edit</a></td>
                        </tr>           	
                   	<?php }
                    }
                   	?>
                   	
                    </tbody>
                </table> 
            </div>
    	
    	</div>
        
    </div>
</div>
<?php get_footer(); ?>