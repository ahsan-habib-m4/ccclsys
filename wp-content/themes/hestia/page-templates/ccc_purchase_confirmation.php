<?php /* Template Name: cccl_purchase_confirmation */ ?>
<?php 
if(empty($_SESSION["receipt_number"])) wp_redirect( home_url());
$purchase_details = get_purchase_list_by_receipt_number ($_SESSION["receipt_number"]);
get_header(); 
add_filter('show_admin_bar', '__return_false');
do_action( 'hestia_before_single_page_wrapper' );
if ($_POST['task'] == "print_ticket") {
	$file = $_SESSION["receipt_number"].".pdf";
	$html = generate_purchase_ticket ($_SESSION["receipt_number"], "server");
	$pdf_type = $_POST['pdf_type'];
	pdf_generator ($html, $file, $pdf_type);
}
?>
<div class="<?php echo hestia_layout(); ?>">
	<div class="container purchase_confirmation_page">
	<div class="row">&nbsp;</div>
	<?php 
	echo show_custom_message("Your purchase has been completed successfully. Details has been sent to your email address.", "success");
	echo purchase_details_html ($purchase_details, $purchase_details->category);
	if ($purchase_details->category == constant("EVENT") || $purchase_details->category ==  constant("SERVICE")) {
	?>
	<div class="row purchase-details-div">
		<div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0">
			<form method="post">
				<input type="submit" value="Print ticket(s)" class="pull-right">
				<input type="hidden" value="D" name="pdf_type">
				<input type="hidden" value="print_ticket" name="task">
			</form>
		</div>
	</div>
	<?php echo generate_purchase_ticket ($_SESSION["receipt_number"]);
	}
	?>
	</div>
</div>
<?php get_footer(); ?>
