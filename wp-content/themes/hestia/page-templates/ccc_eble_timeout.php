<?php /* Template Name: cccl ebl timeout*/ ?>
<?php
@session_start();
require get_template_directory()."/inc/ebl/configuration.php";
require get_template_directory()."/inc/ebl/skypay.php";
get_header();
add_filter('show_admin_bar', '__return_false');
do_action( 'hestia_before_single_page_wrapper');
$order_id = $_GET["order"];
if(empty($order_id)) return;
update_payment_status("TIMEOUT", $order_id, "Payment timeout");
?>
<div class="<?php echo hestia_layout(); ?> verticallyCenterContents">
	<div class="container paymentStatus-cont">
		<div class="row completed">
			<div class="col-md-12 text-center align-middle">
				<i class="far fa-frown successIcon"></i>
				<h2 class="h2">Payment Timeout for Order# <?php echo $_GET["order"]?></h2>
				<h4 class="h4">If you have any query please notify info@cccl.com.bd or +88028831896</h4>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>