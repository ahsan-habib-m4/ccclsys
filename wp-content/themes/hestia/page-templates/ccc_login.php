<?php
/**
 * Template Name: cccl_login
 *
 * The template for login
 *
 * @package Hestia
 * @since Hestia 1.0
 */

global $post;
$post_slug = $post->post_name;
/**
 * Don't display page header if header layout is set as classic blog.
 */
?>

<?php 
if (is_user_logged_in()) {
	$user = wp_get_current_user();
	if ( $user->roles{0}  == 'contributor' || $user->roles{0}  == 'author' ) {
		$redirecturl = get_permalink(33);
	}else {
		$redirecturl = get_permalink(31);
	}
	wp_redirect($redirecturl);
	exit;
} else {
get_header();
?>
<div class="<?php echo hestia_layout(); ?>">
	<div class="container">
		<div class="row-fluid logo-signin">
			<a href="http://cccl.com.bd" target="_blank"><img alt="cccl logo" src="<?php echo get_stylesheet_directory_uri()?>/assets/img/cccllogo-short.png"></a>
		</div>
		<div class="row-fluid">
			<form id="login" action="login" method="post" autocomplete="off">
				<fieldset>
					<?php if ($post_slug == "login") {?>
					<h1 class="form-signin-heading h1">Member Login</h1>
					<?php } else {?>
					<h1 class="form-signin-heading h1">Manager/Operator Login</h1>
					<?php }
					if ( filter_input( INPUT_GET, 'changePass' ) === 'success' ) : 
						echo show_custom_message("Password changed successfully. Please login your new password.", "success");
					endif ?>
					<p class="status"></p>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-user"></i></span>
						<div class="form-group">
							<?php if ($post_slug == "login") {?>
							<input type="text" placeholder="Member ID" id="username"  name="username" class="form-control sib-email-area form-control" required autofocus>
							<?php } else { ?>
							<input type="text" placeholder="Username" id="username"  name="username" class="form-control txt-input" required autofocus>
							<?php }?>
						</div>
					</div>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-key"></i></span>
						<div class="form-group"><input type="password" placeholder="Password" name="password" id="password" required class="form-control txt-input"> </div>
					</div>
					<div class="form-check pull-right">
						<input class="form-check-input" type="checkbox" name="rememberme" id="rememberme" >
						<label class="form-check-label" for="rememberme">
							 Remember me
						</label>
					</div>
					<div class="input-group loginbuttons">
						<input class="submit_button btn btn-primary pull-right" type="submit" value="Login" name="submit">
					<?php if ($post_slug == "login") {?>
						<a href="<?php echo get_permalink(67)?>" class="text-left">Sign up/ Get Password</a>
						<?php } else {?>
						<a href="<?php echo get_permalink(70)?>" class="text-left">Signup / Get Password</a>
						<?php } ?>
						<?php wp_nonce_field( 'ajax-login-nonce', 'security' );?>
					</div>
				</fieldset>
			</form>
		</div>	
	</div>
</div>
<?php 
	get_footer();
} 
?>
