<?php /* Template Name: cccl R2M page */ ?>
<?php

$sKey = $_GET ['secret'];
$mid = $_GET ['memberid'];

if ($sKey != "" || $mid != "") {
	echo json_encode(array('loggedin'=>false, 'message'=>__('Wrong username or password.')));
	return;
}

$sKey = $_POST ['secret'];
$mid = $_POST ['memberid'];
$secret =  SECRET_KEY;
if($sKey == SECRET_KEY && $mid != "") {
	r2m_login($mid);
} else {
	echo json_encode(array('loggedin'=>false, 'message'=>__('Wrong username or password.')));
}

?>
