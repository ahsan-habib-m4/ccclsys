<?php /* Template Name: cccl ssl success*/
require get_template_directory()."/inc/ssl/lib/SslCommerzNotification.php";
use SslCommerz\SslCommerzNotification;
	$sslc = new SslCommerzNotification();	
	$tran_id = $_POST['tran_id'];
	$amount =  $_POST['amount'];
	$currency =  $_POST['currency'];
	$validated = $sslc->orderValidate($_POST['tran_id'], $_POST['amount'], $_POST['currency'], $_POST);
	if ($validated) {
		$responseArray = $_POST;
		$responseArray["id"] = $_POST['tran_id'];
		$responseArray["device.ipAddress"] =  get_the_user_ip ();
		$responseArray["result"] = "SUCCESS";
		$responseArray["payment_type"] = "SSL";
		$responseArray["description"] = $_POST['value_a'];
		$responseArray["bank_tran_id"] = $_POST['bank_tran_id'];
		update_payment($responseArray, $responseArray["value_b"], $responseArray["value_c"]);
//		wp_redirect( get_permalink(210));
	}else {
		echo "Invalid transaction ";
	}

?>
