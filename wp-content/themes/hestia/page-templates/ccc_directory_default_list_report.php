<?php /* Template Name: cccl_directory_default_list_report */ ?>


<?php get_header(); 
add_filter('show_admin_bar', '__return_false');
do_action( 'hestia_before_single_page_wrapper' );
$user = wp_get_current_user();
?>
<div class="<?php echo hestia_layout(); directoryReportMainCont ?>">
    <div class="container directory_report">
    	<form id="reportForm" method="post">
            <div class="row">
            	<div class="col-md-6 col-xs-12 col-sm-12 col-md-offset-3">
                     <div class="card card-form membershipReportFormDiv">
    					<div class="content">
    						<div class="row">
    							<div class="col-md-3">
    								<label for="College" class="col-form-label">College</label>
    							</div>
    							<div class="col-md-9">
    								<select class="form-control inputField" id="college" name="college">
    									<option value="">Select</option>
    									<?php $college_list = get_college_list();
    									foreach ($college_list as $value) {
    										$selected = $_POST['college'] == $value->serial_no ? "Selected" : "" ;
    									?>
    									<option value="<?php echo $value->serial_no?>" <?php echo $selected; ?>><?php echo $value->college_name?></option>	
    									<?php 
    									}
    									?>
    								</select>
    							</div>
    						</div>
    						<div class="row">
    							<div class="col-md-3">
    								<label for="hscyear" class=" col-form-label">Hsc year</label>
    							</div>
    							<div class="col-md-9">
    								<select class="form-control inputField basicInfo" id="hscyear" name="hscyear">
        								<option value="">Select</option>
        								<?php 
                                          for( $i=1940; $i<=date("Y"); $i++ ) {
                                              $selected = $_POST['hscyear'] == $i ? "Selected" : "" ;
                                          	?>
                                         <option value="<?php echo $i?>" <?php echo $selected;?>><?php echo $i;?></option>";
                                          <?php }
                                          ?> 
    								</select>
    							</div>
    						</div>
    						<div class="row margintop">
    							<div class="col-md-12 text-right">
    								<button type="submit" class="btn btn-primary defaultMemberReportBtn">Search</button>
    							</div>
    						</div>
    					</div>
    				</div>
				</div>
            </div>
    	</form>
    	
    	<div class="row get-pass-sec">
        	<div class="col-md-12">
             	<h2 class="h2">Member List</h2>
             	<hr>
         	</div>
        </div>
    	<div class="row">
    		<div class="col-md-12">
    		<table id="membershipReportTable" class="table table-striped datatable">
                <thead>
                    <tr>
                        <th>Thumbnail</th>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Cadet Name</th>
                        <th>College</th>
                        <th>HSC</th>
                    </tr>
                </thead>
                <tbody>
                     <?php 
                     $results = get_member_list();
                        if(count($results)>0) {
                        	foreach ($results as $value) {?>
                            <tr>
                                <td>
                                	<?php if ($value->image != "") { ?>
                                		<img src="<?php echo get_template_directory_uri() . '/assets/img/member_image/' . $value->image; ?>" alt="profile image" class="rounded profileImage tableImage">
                                	<?php } else { ?>
                                		<img src="<?php echo get_template_directory_uri() . '/assets/img/avatar.jpg' ?>" alt="profile image" class="rounded profileImage tableImage">
                                	<?php } ?>
								</td>
                                <td>
                                	<?php 
                                        $fullid = explode("-",$value->member_full_id);
                                        echo $fullid[4];
                                    ?>
                                </td>
                                <td><?php echo $value->display_name;?></td>
                                <td><?php echo $value->cadet_name;?></td>
                                <td><?php echo $value->college_name;?></td>
                                <td><?php echo $value->hsc_year;?></td>
                            </tr>           	
                       	<?php }
                        }
                       	?>
                </tbody>
            </table> 
            </div>
    	</div>
    </div>
</div>
<?php get_footer(); ?>

<script>
jQuery(document).ready(function($) {
	var membershipReportTable = $('#membershipReportTable').DataTable({
		<?php 
		$user = wp_get_current_user();
		if ( $user->roles{0}  == 'contributor' || $user->roles{0}  == 'author' || $user->roles{0}  == 'administrator') {
		?>
		dom: "<'row'<'col-md-3 col-xs-4'l><'col-md-8 col-xs-5'f><'col-md-1 col-xs-3'B>>" +
    		"<'row'<'col-md-12'tr>>" +
    		"<'row'<'col-md-8 col-xs-6'i><'col-sm-4 col-xs-6'p>>",
        buttons: [
			{ extend: 'csv',className: "btn btn-primary", text: '<i class="fas fa-download"></i>' }
        ],
        <?php } ?>
		rowReorder: {
			selector: 'td:nth-child(3)'
		},		
		responsive: true,
		order : [ [ 1, 'asc' ]],
		"columnDefs" : [  {
			"targets" : 'no-sort',
			"orderable" : false
		} ],
		"pageLength": 50
	});
	
});
</script>