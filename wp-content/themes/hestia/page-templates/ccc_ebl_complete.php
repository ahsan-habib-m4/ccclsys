<?php /* Template Name: cccl ebl complete*/ ?>
<?php
if ($_SESSION["payment_type"] == "EBL"){
	require get_template_directory()."/inc/ebl/configuration.php";
	require get_template_directory()."/inc/ebl/skypay.php";
	$errorMessage = "";
	$errorCode = "";
	$gatewayCode = "";
	$result = "";
	$responseArray = array();
	$resultIndicator =  (isset($_GET["resultIndicator"])) ? $_GET["resultIndicator"] : "";
	$eblskypay = $_SESSION['eblskypay'];
	$result = "Payment Falied";
	if( !empty($eblskypay['successIndicator']) && ($eblskypay['successIndicator'] == $resultIndicator) ) {
		$skypay = new skypay($configArray);
		$responseArray = $skypay->RetrieveOrder($_GET["order"]);
		if(($responseArray["amount"] == $responseArray["totalAuthorizedAmount"]) && ($responseArray["amount"] == $responseArray["totalCapturedAmount"])) {
			$result = "Payment Completed";
			if ( is_user_logged_in()) {
				$user = wp_get_current_user();
				$member_id = $user->user_login;
			}else {
				$list = explode("/", $responseArray["description"]);
				$member_id = $list[0];
			}
			update_payment($responseArray, $member_id, $_SESSION["paid_till"]);
		}
	}
}
get_header();
add_filter('show_admin_bar', '__return_false');
do_action( 'hestia_before_single_page_wrapper' );
?>
<div class="<?php echo hestia_layout(); ?> verticallyCenterContents">
	<div class="container paymentStatus-cont">
		<div class="row completed">
			<div class="col-md-12 text-center align-middle">
				<i class="far fa-thumbs-up successIcon"></i>
				<h2 class="h2">Thanks for the payment</h2>
				<h3 class="h3">Payment transaction is complete and receipt has been emailed</h3>
				<h4 class="h4">If you have any query please notify info@cccl.com.bd or +88028831896</h4>
			</div>
		</div>
	</div>
</div>	
<?php get_footer(); ?>