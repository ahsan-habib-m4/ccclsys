<?php /* Template Name: cccl_temporary_confirmation */ ?>
<?php 
if(empty($_SESSION["receipt_number"])) wp_redirect( home_url());
get_header(); 
add_filter('show_admin_bar', '__return_false');
do_action( 'hestia_before_single_page_wrapper' );
?>
<div class="<?php echo hestia_layout(); ?>">
	<div class="container purchase_confirmation_page">
	<div class="row">&nbsp;</div>
	<?php echo show_custom_message("Purchased product has been hold. Details has been sent to members email address.", "success");?>
		<div class="row purchase-details-div">
			<div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0">
				<?php 
					echo $purchase_details = venue_temp_html($_SESSION["receipt_number"]);	
			 	?>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
