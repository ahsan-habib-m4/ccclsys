<?php /* Template Name: cccl_membership_report */ ?>


<?php get_header(); 
add_filter('show_admin_bar', '__return_false');
do_action( 'hestia_before_single_page_wrapper' );
$user = wp_get_current_user();

global $post;
$post_slug = $post->post_name;
?>
<div class="<?php echo hestia_layout(); ?>">
    <div class="container add_collection_report">
    	<?php if($post_slug != "due-report") { ?>
        	<form id="membershipReportForm" method="post" autocomplete="off">
                <div class="row">
                	<div class="col-md-8 col-xs-12 col-sm-12 col-md-offset-2">
                         <div class="card card-form-horizontal membershipReportFormDiv">
        					<div class="content">
        						<div class="row">
        							<div class="textwidget">
        								<label for="clctnFrom" class="col-md-3 col-form-label">Membership ID</label>
                                        <div class="col-md-4">
                                            <input type="text" class=" form-control " name="srcByMbrId" id="srcByMbrId" value="<?php echo $_POST["srcByMbrId"]?>" required="required" placeholder="Last five digits of Member ID">
                                        </div>
                                         <div class="col-md-4 text-center">
                                         	<button type="submit" class="btn btn-primary mb-2 membershipReportBtn">Search</button>
                                         </div>
        							</div>
        						</div>
        					</div>
        				</div>
    				</div>
                </div>
        	</form>
    	<?php } ?>
    	
    	<div class="row get-pass-sec">
        	<div class="col-md-12">
             	<h2 class="h2"><?php echo $post_slug == "due-report" ? "Due List" : "Member List"?></h2>
             	<hr>
         	</div>
        </div>
    	<div class="row">
    		<div class="col-md-12">
    		<table id="membershipReportTable" class="table table-striped datatable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>College</th>
						<th>Paid till</th>
                        <th>Due amount</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>Phone</th>
                        <th>Email</th>
                    </tr>
                </thead>
                <tbody>
                	<?php 
                     $results = get_membership_report_on_Payment();	
                        if(count($results)>0) {
                        	foreach ($results as $value) {
                        	        $dueamount = get_due_amount($value);
                        	        if ($post_slug == "due-report" and $dueamount < 1 ) continue;
                     ?>
                        <tr>
                            <td><?php echo $value->user_login;?></td>
                            <td><?php echo $value->display_name;?></td>
                            <td><?php echo $value->college_sort_code;?></td>
                            <td>
                            	<?php echo $value->payment_upto == "N/A" ? $value->payment_upto : ($value->subcription_required == 'N' ? 'N/A' : substr($value->payment_upto,0,4)."-".substr($value->payment_upto,4,6)); ?>
                            </td>
                            <td><?php echo number_format($dueamount,2); ?></td>
                            <td><?php echo $value->member_type;?></td>
                            <td><?php echo $value->status;?></td>
                            <td><?php echo $value->mobile_number;?></td>
                            <td><?php echo $value->user_email;?></td>
                        </tr>
                    <?php }
                        }
                       	?>
                </tbody>
            </table> 
            </div>
    	</div>
    </div>
</div>
<?php get_footer(); ?>

<script>
jQuery(document).ready(function($) {
	var membershipReportTable = $('#membershipReportTable').DataTable({
		<?php 
		$user = wp_get_current_user();
		if ( $user->roles{0}  == 'contributor' || $user->roles{0}  == 'author' || $user->roles{0}  == 'administrator') {
		?>
		dom: "<'row'<'col-md-3 col-xs-4'l><'col-md-8 col-xs-5'><'col-md-1 col-xs-3'B>>" +
    "<'row'<'col-md-12'tr>>" +
    "<'row'<'col-sm-6 col-xs-12'i><'col-sm-6 col-xs-12'p>>",
        buttons: [
			{ extend: 'csv',className: "btn btn-primary", text: '<i class="fas fa-download"></i>' }
        ],
        <?php } ?>
		rowReorder: {
			selector: 'td:nth-child(3)'
		},		
		responsive: true,
		order : [ [ 0, 'asc' ]],
		"columnDefs" : [  {
			"targets" : 'no-sort',
			"orderable" : false
		},
		 {
	        targets: 4,
	        className: 'text-right'
	    },
	    {
	        targets: [3,4,5],
	        className: 'text-nowrap'
	    }],
	    "pageLength": 50
	});
	
});
</script>