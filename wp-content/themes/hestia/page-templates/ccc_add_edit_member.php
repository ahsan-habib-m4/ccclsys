<?php /* Template Name: cccl_add_edit_member */ ?>

<?php 
@session_start();
global $post;
$post_slug = $post->post_name;
$readonlyMode = false;
$editMode = false;
$onlyView = false;
if ($post_slug == "profile") {
	$readonlyMode = true;
}elseif ($post_slug == "edit-member"){
	$editMode = true;
}
if ($_POST['task'] == "addMember") {
	$data = prepare_member_data();
	$error = add_member($data);
	if (empty($error)) {
		$_SESSION["add_success"] = "success";
		wp_redirect( get_permalink(151)."?mi=".$_POST['txtmemberId']);
	}
}elseif ($_POST['task'] == "searchMember") {
	$user_details = get_user_list($_POST["srcByMbrId"]);
}elseif ($_POST['task'] == "editMember") {
	$errorEdit = edit_member();
	$user_details = get_user_list($_POST["srcByMbrId"]);
} elseif ($_GET['task'] == "viewmemberprofile") {
	$user_details = get_user_list($_GET["srcByMbrId"]);
	$onlyView = true;
	$readonlyMode = true;
}elseif( $post_slug == "edit-member" && $_GET['mi']){
	$user_details = get_user_list($_GET['mi']);
}
get_header(); 
add_filter('show_admin_bar', '__return_false');
do_action( 'hestia_before_single_page_wrapper' );

if($readonlyMode || $error || $user_details) {
	if(empty($user_details)){
		$user = wp_get_current_user();
		$user_details = get_user_list($user->user_login);
	}
	$college = $error ? $data["college"]: $user_details->college;
	$cadet_no = $error ? $data["cadet_no"]: $user_details->cadet_no;
	$cadet_name = $error ? $data["cadet_name"]: $user_details->cadet_name;
	$member_full_id = $error ? $data["member_full_id"]: $user_details->member_full_id;
	$status = $error ? $data["status"]: $user_details->status;
	$batch = $error ? $data["batch"]: $user_details->batch;
	$type = $error ? $data["type"]: $user_details->type;
	$country_phone_code = $error ? $data["country_phone_code"]: $user_details->country_phone_code;
	$mobile_number = $error ? $data["mobile_number"]: $user_details->mobile_number;
	$postal_address = $error ? $data["postal_address"]: $user_details->postal_address;
	$profession = $error ? $data["profession"]: $user_details->profession;
	$organization = $error ? $data["organization"]: $user_details->organization;
	$designation = $error ? $data["designation"]: $user_details->designation;
	$specialization = $error ? $data["specialization"]: $user_details->specialization;
	$office_address = $error ? $data["office_address"]: $user_details->office_address;
	$home_address = $error ? $data["home_address"]: $user_details->home_address;
	$date_of_birth = $error ? custom_date("d-M-Y", $data["date_of_birth"]) : custom_date("d-M-Y", $user_details->date_of_birth);
	$blood_group = $error ? $data["blood_group"]: $user_details->blood_group;
	$hsc_year = $error ? $data["hsc_year"]: $user_details->hsc_year;
	$anniversary = $error ? custom_date("d-M-Y", $data["anniversary"]) : custom_date("d-M-Y", $user_details->anniversary);
	$spouse = $error ? $data["spouse"]: $user_details->spouse;
	$spouse_occupation = $error ? $data["spouse_occupation"]: $user_details->spouse_occupation;
	$child1 = $error ? $data["child1"]: $user_details->child1;
	$child1_name = $error ? $data["child1_name"]: $user_details->child1_name;
	$child1_date_of_birth = $error ? custom_date("d-M-Y", $data["child1_date_of_birth"]) : custom_date("d-M-Y", $user_details->child1_date_of_birth);
	$child2 = $error ? $data["child2"]: $user_details->child2;
	$child2_name = $error ? $data["child2_name"]: $user_details->child2_name;
	$child2_date_of_birth = $error ? custom_date("d-M-Y", $data["child2_date_of_birth"]): custom_date("d-M-Y", $user_details->child2_date_of_birth);
	$m_user_email = $error ? $_POST["txtemail"]: $user_details->user_email;
	$member_name = $error ? $_POST["txtname"]: $user_details->display_name;
	$m_user_login = $error ? $_POST["txtmemberId"]: $user_details->user_login;
	$m_id = $error ? $_POST["id"]: $user_details->ID;
	$payment_upto = $error ? $data["payment_upto"]: $user_details->payment_upto;
}
	$image= $user_details->image ? get_template_directory_uri() . '/assets/img/member_image/'.$user_details->image.'?v='.time() : get_template_directory_uri() . '/assets/img/avatar.jpg';

?>

	<div class="<?php echo hestia_layout(); ?>">
		<div class="container addMemberScreen">
		<div id="message"></div>
		<?php 
		if($_POST['task'] == "addMember") {
			if(!empty($error)) {
				echo show_custom_message($error, "warning");
			}else{
				echo show_custom_message("Member record inserted successfully ", "success");
			}
		}elseif ($_POST['task'] == "searchMember"){
			if(!count($user_details)>0) {
				echo show_custom_message("Member details not found", "warning");
			}
		}elseif ($_POST['task'] == "editMember"){
			if(!empty($errorEdit)) {
				echo show_custom_message($errorEdit, "warning");
			}else{
				echo show_custom_message("Member record updated successfully", "success");
			}
		}
		if ( $post_slug == "edit-member") { 
			if ($_SESSION["add_success"] == "success") {
				echo show_custom_message("Member record inserted successfully ", "success");
				unset($_SESSION["add_success"]);
			}
		?>
		<form method="post" id="srcByMbrIdForm" autocomplete="off">
			<div class="row">
				<div class="col-md-3 col-md-3 col-sm-4 col-xs-12"></div>
				<div class=" col-md-9 col-sm-12 col-xs-12">
					<div class="row sectionDiv">
						<div class="row">
							<div class="card card-form-horizontal searchMemberInEdit">
								<div class="content">
									<div class="row">
										<div class="textwidget">
											<div class="col-md-8 col-sm-12 col-xs-12">
												<div class="input-group">
													<label for="srcByMbrId" class="col-md-4 col-sm-12 col-xs-12 col-form-label">Member ID</label>
													<div class="form-group is-empty col-md-8 col-sm-12 col-xs-12">
														<input type="text" class=" form-control " name="srcByMbrId" id="srcByMbrId" value="<?php echo $_POST["srcByMbrId"]? $_POST["srcByMbrId"] : $_GET["mi"]?>" required="required" placeholder="Last five digits of Member ID">
													</div>
												</div>
											</div>
											<div class="col-md-4 col-sm-12 col-xs-12">
												<button type="submit" class="btn btn-primary btn-block sib-default-btn srcByMbrIdBtn" id="srcByMbrIdBtn">Search</button>
												<input type="hidden" name="task" value="searchMember"> 
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
		<?php } ?>
	
		<form id="addMemberform" method="post" enctype="multipart/form-data" autocomplete="off">
			<div class="row">
				<?php wp_nonce_field( 'update-member-nonce', 'security' );?>
				<div class="col-md-3 col-sm-4 col-xs-12 imageAndButtonCont">
					<div class="row text-center imageHolder">
						<img src="<?php echo $image ?>" alt="profile image" id="profileImage" class="rounded profileImage" title="Profile image">
						<input class="d-none photoInfo" type="file" id="photo" name="photo" onchange="previewFile(this);" >	
						<input class="d-none photoInfo" type="hidden" id="orginPhoto" name="orginPhoto" value="<?php echo $image; ?>" >	
						<div class="profPicUpBtn" id="profPicUpBtn" title="Upload profile image" <?php if($onlyView) echo "style='display:none;'"?>><i class="fa fa-camera"></i> </div>
					</div>
					<?php if ($readonlyMode) { ?>
						<div class="row text-center buttonHoldet">
							<button class="btn-outline-primary photoUpdateBtn" type="button"><span>Save</span> <i class="far fa-save"></i></button>
							<button class="btn-outline-primary photoCancelBtn" type="button"><span>Cancel</span> <i class="fas fa-undo"></i></i></button>
						</div>
					<?php } ?>
					<?php if ($readonlyMode || $onlyView) { ?>
						<div class="row text-center infoHolder">
							<p><strong><?php echo $member_name;?></strong></p>
							<p><?php echo $member_full_id;?></p>
							
							<?php
							$member_type_list = get_member_type_list();
							foreach ($member_type_list as $value) {
							    if( $type == $value->code) $memberType = $value->member_type;
							}
							?>
							<p><?php echo $status;?> <strong><?php echo $memberType; ?></strong> Member</p>
						</div>
					<?php } ?>
				</div>
				<div class="col-md-9 col-sm-8 col-xs-12">
					<?php if (!$readonlyMode && !$onlyView) { ?>
					<div class="row sectionDiv">
						<div class="row">
							<h2 class="h2">Membership Information</h2>
						</div>
						<div class="row">
							<label for="College" class="col-md-3 col-form-label">College</label>
							<div class="col-md-7">
								<select class="form-control inputField" id="college" name="college" required onchange="createMemberId()" <?php if ($readonlyMode) echo "disabled";?>>
									<option value="">Required</option>
									<?php $college_list = get_college_list();
									foreach ($college_list as $value) {
										$selected = $college == $value->serial_no ? "Selected" : "" ;
									?>
									<option value="<?php echo $value->serial_no?>" <?php echo $selected;?>><?php echo $value->college_name?></option>	
									<?php 
									}
									?>
								</select>
							</div>
						</div>
						
                         <div class="row">
                            <label for="cadetno" class="col-md-3 col-form-label">Cadet No</label>
                            <div class="col-md-7">
                              <input type="text" class="form-control inputField" id="cadetno" name="cadetno" placeholder="Cadet no" value="<?php echo $cadet_no;?>" onblur="createMemberId()" <?php if ($readonlyMode) echo "disabled";?>>
                            </div>
                        </div>

                        <div class="row">
							<label for="batch" class="col-md-3 col-form-label">Batch</label>
							<div class="col-md-7">
								<input type="text" class="form-control inputField" id="batch" name="batch" onblur="createMemberId()" value="<?php echo $batch;?>" placeholder="Required" <?php if ($readonlyMode) echo "disabled";?>>
							</div>
						</div>
                        
                        <div class="row">
                            <label for="slctMbrType" class="col-md-3 col-form-label">Member Type</label>
                            <div class="col-md-7">
                              	<select class="form-control inputField" id="slctMbrType" name="slctMbrType" required onchange="createMemberId()" <?php if ($readonlyMode) echo "disabled";?>>
									<option value="">Required</option>
									<?php
									$member_type_list = get_member_type_list();
									foreach ($member_type_list as $value) {
										$selected = $type == $value->code ? "Selected" : "" ;
									?>
									<option data-paysub="<?php echo $value->subcription_required; ?>" value="<?php echo $value->code; ?>" <?php echo $selected;?>><?php echo $value->member_type?></option>	
									<?php 
									}
									?>
                            	</select>
                            </div>
                        </div>
                        
                        <div class="row">
                            <label for="txtmemberId" class="col-md-3 col-form-label">Member ID</label>
                            <div class="col-md-7">
                              <input type="text" class="form-control inputField" id="txtmemberId" name="txtmemberId" value="<?php echo $m_user_login?>" placeholder="Required" required onblur="createMemberId()" <?php if ($readonlyMode || $editMode) echo "disabled";?>>
                            </div>
                        </div>
                        
                        <div class="row">
                            <label for="mmbrfulId" class="col-md-3 col-form-label">Member Full ID</label>
                            <div class="col-md-7">
                              <input type="text" class="form-control inputField" id="mmbrfulId" name="mmbrfulId" value="<?php echo $member_full_id;?>" placeholder="Required" required placeholder="" readonly <?php if ($readonlyMode) echo "disabled";?>>
                            </div>
                        </div>
                        
                        <div class="row">
                            <label for="slctMbrStts" class="col-md-3 col-form-label">Member Status</label>
                            <div class="col-md-7">
                              	<select class="form-control inputField" id="slctMbrStts" name="slctMbrStts" required <?php if ($readonlyMode) echo "disabled";?>>
                                    <option value="">Required</option>
                                    <option value="Active" <?php echo $status == "Active" ? "Selected" : "";?> >Active</option>
                                    <option value="Abroad" <?php echo $status == "Abroad" ? "Selected" : "";?>>Abroad</option>
                                    <option value="Discontinued" <?php echo $status == "Discontinued" ? "Selected" : "";?>>Discontinued</option>
                                    <option value="Suspended" <?php echo $status == "Suspended" ? "Selected" : "";?>>Suspended</option>
                                    <option value="Expired" <?php echo $status == "Expired" ? "Selected" : "";?>>Expired</option>
                                    <option value="User" <?php echo $status == "User" ? "Selected" : "";?>>User</option>
                                </select>
                            </div>
                        </div>
                        <?php if(!$onlyView && !$readonlyMode ) { ?>
                        <div class="row">
                            <label for="paidupto" class="col-md-3 col-form-label">Paid Till</label>
                            <div class="col-md-7">
                              	<select class="form-control inputField" id="paidupto" name="paidupto" required <?php if ($readonlyMode || $editMode) echo "disabled";?>>
                                    <option value="">Required</option>
                                    <option value="N/A" <?php if($payment_upto == "N/A") echo "Selected"?>>N/A</option>
                                    <?php 
                                    $quarte_list = get_Subscription_fee_list();
                                    foreach ($quarte_list as $value) {
                                        $selected = $payment_upto == $value->quarter_year ? "Selected" : "" ;
									?>
									<option value="<?php echo $value->quarter_year; ?>" <?php echo $selected;?>><?php echo get_quarter_details($value->quarter_year);?></option>
									<?php 
									}
									?>
                                </select>
                            </div>	
                        </div>
                        <?php } ?>
                        
					</div>
					<?php } ?>
					<div class="row sectionDiv" id="contactInformation">
						<div class="row">
							<h2 class="h2">Contact Information 
								<?php if ($readonlyMode && !$onlyView) { ?>
									<button class="editbtn pull-right btn-outline-primary" type="button" data-mode="edit" data-section="contactInfo"><span>Edit</span> <i class="far fa-edit"></i></button>
									<input type="hidden" name="task" value="contact_update">
								<?php } ?>
							</h2>
						</div>
						<div class="row">
							<label for="cadetname" class="col-md-3 col-form-label">Cadet Name</label>
							<div class="col-md-7">
								<input type="text" class="form-control inputField contactInfo" id="cadetname" name="cadetname" required value="<?php echo $cadet_name;?>" placeholder="Required" <?php if ($readonlyMode) echo "disabled";?>>
							</div>
						</div>
						<div class="row">
                            <label for="txtname" class="col-md-3 col-form-label">Full Name</label>
                            <div class="col-md-7">
                              <input type="text" class="form-control inputField contactInfo" id="txtname" name="txtname"  value="<?php echo $member_name;?>" placeholder="Required" required <?php if ($readonlyMode) echo "disabled";?>>
                            </div>
                        </div>
						<div class="row">
							<label for="txtCountryCode" class="col-md-3 col-form-label">Phone (Mobile)</label>
							<div class="col-md-7">
                                <input type="text" class="form-control inputField phonenumber contactInfo" id="txtPhone" name="txtPhone" value="<?php echo str_replace(' ', '', $mobile_number);?>" placeholder="+8801752XXXXXX" data-toggle="tooltip" data-placement="top" title="Mobile Number with Country Code e.g. +8801752XXXXXX" required <?php if ($readonlyMode) echo "disabled";?>>
                            </div>
                        </div>
                        
                        <div class="row">
                            <label for="txtemail" class="col-md-3 col-form-label">Email Address</label>
                            <div class="col-md-7">
                              <input type="email" class="form-control inputField contactInfo" id="txtemail" name="txtemail" value="<?php echo $m_user_email;?>" autocomplete="off" placeholder="Required" required <?php if ($readonlyMode) echo "disabled";?>>
                            </div>
                        </div>
                        
                        <div class="row">
                            <label for="postalAddrs" class="col-md-3 col-form-label">Postal Address</label>
                            <div class="col-md-7">
                                <select class="form-control inputField contactInfo" id="postalAddrs" name="postalAddrs" required <?php if ($readonlyMode) echo "disabled";?>>
                                    <option value="">Required</option>
                                    <option value="Home" <?php echo $postal_address == "Home" ? "Selected" : "";?>>Home Address</option>
                                    <option value="Office" <?php echo $postal_address == "Office" ? "Selected" : "";?>>Office Address</option>
                                </select>
                            </div>
                        </div>
					</div>
					<div class="row sectionDiv">
						<div class="row">
							<h2 class="h2">Basic Information 
								<?php if ($readonlyMode && !$onlyView) { ?>
									<button class="editbtn pull-right btn-outline-primary" type="button" data-mode="edit" data-section="basicInfo"><span>Edit</span> <i class="far fa-edit"></i></button>
								<?php } ?>
							</h2>
						</div>
						<div class="row">
							<label for="profession" class="col-md-3 col-form-label">Profession</label>
							<div class="col-md-7">
								<select class="form-control inputField basicInfo" id="profession" name="profession" required <?php if ($readonlyMode) echo "disabled";?>>
									<option value="">Required</option>
									<?php
									$profession_list = get_profession_list();
									foreach ($profession_list as $value) {
										$selected = $profession == $value->id ? "Selected" : "" ;
									?>
									<option value="<?php echo $value->id;?>" <?php echo $selected;?>><?php echo $value->profession_name;?></option>
									<?php 
									}
									?>
                                </select>
                            </div>
                        </div>
                        
                        <div class="row">
                            <label for="organization" class="col-md-3 col-form-label">Organization</label>
                            <div class="col-md-7">
                              <input type="text" class="form-control inputField basicInfo" id="organization" name="organization" value="<?php echo $organization;?>" placeholder="Required" required <?php if ($readonlyMode) echo "disabled";?> data-toggle="tooltip" data-placement="top" title="E.g: Bangladesh Army, Bangladesh Navy">
                            </div>
                        </div>
                        
                        <div class="row">
                            <label for="designation" class="col-md-3 col-form-label">Designation</label>
                            <div class="col-md-7">
                              <input type="text" class="form-control inputField basicInfo" id="designation" name="designation" value="<?php echo $designation;?>" placeholder="Required" required <?php if ($readonlyMode) echo "disabled";?> data-toggle="tooltip" data-placement="top" title="E.g: Major, Captain, Managing Director,CEO">
                            </div>
                        </div>
                        
                        <div class="row">
                            <label for="specialization" class="col-md-3 col-form-label">Specialization</label>
                            <div class="col-md-7">
                              <input type="text" class="form-control inputField basicInfo" id="specialization" name="specialization" value="<?php echo $specialization;?>" placeholder="Required" required <?php if ($readonlyMode) echo "disabled";?> data-toggle="tooltip" data-placement="top" title="E.g: Marketing Expert,Cyber Operations Specialist">
                            </div>
                        </div>
                        
                        <div class="row">
                            <label for="officeAddr" class="col-md-3 col-form-label">Office Address</label>
                            <div class="col-md-7">
                              <textarea class="form-control inputField basicInfo" id="officeAddr" name="officeAddr" rows="2" required placeholder="Required" <?php if ($readonlyMode) echo "disabled";?> autocomplete="off"><?php echo $office_address;?></textarea>
                            </div>
                        </div>
                        
                        <div class="row">
                            <label for="homeAddr" class="col-md-3 col-form-label">Home Address</label>
                            <div class="col-md-7">
                              <textarea class="form-control inputField basicInfo" id="homeAddr" name="homeAddr"  rows="2" required placeholder="Required" <?php if ($readonlyMode) echo "disabled";?> autocomplete="off"><?php echo $home_address;?></textarea>
                            </div>
                        </div>
                        
                        <div class="row">
                            <label for="specialization" class="col-md-3 col-form-label">Date of Birth</label>
                            <div class="col-md-7">
                                <input type="text" class=" inputField dateFields basicInfo" id="dateOfBirth" name="dateOfBirth" value="<?php echo $date_of_birth;?>" required placeholder="Required" <?php if ($readonlyMode) echo "disabled";?> aria-label="Input group example" aria-describedby="btnGroupAddon">
                                <div class="input-group-append btnGroupAddon datepickerIcon" <?php if ($readonlyMode) echo "style='display: none;'";?>>
                                  <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                            </div>
                        </div>
                        
						<div class="row">
							<label for="bloodgrp" class="col-md-3 col-form-label">Blood Group</label>
							<div class="col-md-7">
								<select class="form-control inputField basicInfo" id="bloodgrp" name="bloodgrp" required <?php if ($readonlyMode) echo "disabled";?>>
                                    <option value="">Required</option>
                                    <option value="A+" <?php echo $blood_group == "A+" ? "Selected" : "";?>>A+</option>
                                    <option value="A-" <?php echo $blood_group == "A-" ? "Selected" : "";?>>A-</option>
                                    <option value="B+" <?php echo $blood_group == "B+" ? "Selected" : "";?>>B+ </option>
                                    <option value="B-" <?php echo $blood_group == "B-" ? "Selected" : "";?>>B-</option>
                                    <option value="O+" <?php echo $blood_group == "O+" ? "Selected" : "";?>>O+</option>
                                    <option value="O-" <?php echo $blood_group == "O-" ? "Selected" : "";?>>O-</option>
                                    <option value="AB+" <?php echo $blood_group == "AB+" ? "Selected" : "";?>>AB+</option>
                                    <option value="AB-" <?php echo $blood_group == "AB-" ? "Selected" : "";?>>AB-</option>
								</select>
                            </div>
                        </div>

						<div class="row">
							<label for="hscyear" class="col-md-3 col-form-label">HSC Year</label>
							<div class="col-md-7">
								<select class="form-control inputField basicInfo" id="hscyear" name="hscyear" required <?php if ($readonlyMode) echo "disabled";?>>
								<option value="">Required</option>
								<?php 
								$to_year = date("Y") - 8;
								for($i = HSC_FROM_YR; $i <= $to_year; $i++) {
									$selected = $hsc_year == $i ? "Selected" : "" ;
								?>
								<option value="<?php echo $i?>" <?php echo $selected;?>><?php echo $i;?></option>";
								<?php }
								?> 
								</select>
							</div>
						</div>
					</div>
					<div class="row sectionDiv">
						<div class="row">
							<h2 class="h2">Family Information 
								<?php if ($readonlyMode && !$onlyView) { ?> 
									<button class="editbtn pull-right btn-outline-primary" type="button" data-mode="edit" data-section="familyInfo"><span>Edit</span> <i class="far fa-edit"></i></button>
								<?php } ?>
							</h2>
						</div>
						
						<div class="row">
							<label for="spouse" class="col-md-3 col-form-label">Spouse Name</label>
							<div class="col-md-7">
								<input type="text" class="form-control inputField familyInfo" id="spouse" name="spouse" value="<?php echo $spouse;?>" <?php if ($readonlyMode) echo "disabled";?>>
							</div>
						</div>
						
						<div class="row">
							<label for="spouseOccu" class="col-md-3 col-form-label">Occupation</label>
							<div class="col-md-7">
								<input type="text" class="form-control inputField familyInfo" id="spouseOccu" name="spouseOccu" value="<?php echo $spouse_occupation;?>" <?php if ($readonlyMode) echo "disabled";?>>
							</div>
						</div>
						
						<div class="row">
							<label for="anniversary" class="col-md-3 col-form-label">Anniversary</label>
							<div class="col-md-7">
								<?php 
    							if($onlyView){
    							    $anniversary = custom_date("d F", $anniversary);
    							}
    							?>
								<input type="text" class="inputField dateFields familyInfo" id="anniversary" name="anniversary" value="<?php echo $anniversary;?>" <?php if ($readonlyMode) echo "disabled";?> aria-label="Input group example" aria-describedby="btnGroupAddon">
								<div class="input-group-append btnGroupAddon datepickerIcon" <?php if ($readonlyMode) echo "style='display: none;'";?>>
									<div class="input-group-text"><i class="fa fa-calendar"></i></div>
								</div>
							</div>
						</div>
                        
                        <div class="row">
                            <label for="child1" class="col-md-3 col-form-label radioboxlbl">Child 1</label>
                            <div class="col-md-7 margintop2 form-group">
                            	<div class="form-radio radio">
                                  <input class="form-check-input familyInfo radio__input" type="radio" name="child1" id="child1son" value="Son" <?php echo $child1 == 'Son' ? 'checked' : '';?>  <?php if ($readonlyMode) echo "disabled";?>>
                                  <label class="form-check-label radio__label" for="child1son">Son</label>
                                </div>
                                <div class="form-radio radio">
                                  <input class="form-check-input familyInfo radio__input" type="radio" name="child1" id="child1daughter" value="Daughter" <?php echo $child1 == 'Daughter' ? 'checked' : '';?> <?php if ($readonlyMode) echo "disabled";?>>
                                  <label class="form-check-label radio__label" for="child1daughter">Daughter</label>
                                </div>
                                <div class="form-radio radio">
                                  <input class="form-check-input familyInfo radio__input childnone" data-childno="1" type="radio" name="child1" id="child1none" value="" <?php echo $child1 == '' ? 'checked' : '';?> <?php if ($readonlyMode) echo "disabled";?>>
                                  <label class="form-check-label radio__label" for="child1none">NA</label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <label for="child1name" class="col-md-3 col-form-label childLbl">&#160;&#160;Name</label>
                            <div class="col-md-7">
                              <input type="text" class="form-control inputField familyInfo" id="child1name" name="child1name" value="<?php echo $child1_name;?>" <?php if ($readonlyMode) echo "disabled";?>>
                            </div>
                        </div>
                        
                        <div class="row">
                            <label for="child1dob" class="col-md-3 col-form-label childLbl">&#160;&#160;Date of Birth</label>
                            <div class="col-md-7">
                            	<input type="text" class="inputField dateFields familyInfo" id="child1dob" name="child1dob" value="<?php echo $child1_date_of_birth;?>" aria-label="Input group example" aria-describedby="btnGroupAddon" <?php if ($readonlyMode) echo "disabled";?> >
                                <div class="input-group-append btnGroupAddon datepickerIcon" <?php if ($readonlyMode) echo "style='display: none;'";?>>
                                  <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <label for="child2" class="col-md-3 col-form-label radioboxlbl">Child 2</label>
                            <div class="col-md-7 margintop2 form-group">
                           		<div class="form-radio radio">
                                  <input class="form-check-input familyInfo radio__input" type="radio" name="child2" id="child2son" value="Son"  <?php echo $child2 == 'Son' ? 'checked' : '';?>  <?php if ($readonlyMode) echo "disabled";?>>
                                  <label class="form-check-label radio__label" for="child2son">Son</label>
                                </div>
                                <div class="form-radio radio">
                                  <input class="form-check-input familyInfo radio__input" type="radio" name="child2" id="child2daughter" value="Daughter"  <?php echo $child2 == 'Daughter' ? 'checked' : '';?> <?php if ($readonlyMode) echo "disabled";?>>
                                  <label class="form-check-label radio__label" for="child2daughter">Daughter</label>
                                </div>
                                <div class="form-radio radio">
                                  <input class="form-check-input familyInfo radio__input childnone" data-childno="2"  type="radio" name="child2" id="child2none" value="" <?php echo $child2 == '' ? 'checked' : '';?> <?php if ($readonlyMode) echo "disabled";?>>
                                  <label class="form-check-label radio__label" for="child2none">NA</label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <label for="child2name" class="col-md-3 col-form-label childLbl">&#160;&#160;Name</label>
                            <div class="col-md-7">
                              <input type="text" class="form-control inputField familyInfo" id="child2name" name="child2name" value="<?php echo $child2_name;?>" <?php if ($readonlyMode) echo "disabled";?>>
                            </div>
                        </div>
                        
                        <div class="row">
                            <label for="child2dob" class="col-md-3 col-form-label childLbl">&#160;&#160;Date of Birth</label>
                            <div class="col-md-7">
                            	<input type="text" class="inputField dateFields familyInfo" id="child2dob" name="child2dob" value="<?php echo $child2_date_of_birth;?>" aria-label="Input group example" aria-describedby="btnGroupAddon" <?php if ($readonlyMode) echo "disabled";?>>
                                <div class="input-group-append btnGroupAddon datepickerIcon" <?php if ($readonlyMode) echo "style='display: none;'";?>>
                                  <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                            </div>
                        </div>
    				</div>
                    
                    <div class="row" <?php if ($readonlyMode) echo "style='display: none;'";?>>
                    	<div class="col-md-12 text-center">
                    		<button type="submit" class="btn btn-primary addMemberBtn">Submit</button>
                    		<input type=hidden name="task" value="<?php echo $editMode ? 'editMember':'addMember'?>">
                    		<input type=hidden name="id" value="<?php echo $m_id;?>">
                    		<input type=hidden name="srcByMbrId" value="<?php echo $m_user_login;?>">
                    	</div>
                    </div>
				</div>
			</div>
			<?php echo create_loader();?>
	</form>
</div>

<?php get_footer(); ?>
