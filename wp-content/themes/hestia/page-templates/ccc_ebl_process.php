<?php /* Template Name: cccl ebl process*/ 
use SslCommerz\SslCommerzNotification;
$_SESSION["payment_type"] = $_POST['paymentType'];
$_SESSION["category"] = $_POST['category'];
if($_POST['category'] == constant("PAYMENT")) {
	$_SESSION["comment"] = !empty($_POST['comment']) ? $_POST['comment'] : '';
}else {
	$_SESSION["comment"] = "";
}
if (!is_user_logged_in()) {
	$user = get_user_by( 'login', $_POST['user_login'] );
}else {
	$user = wp_get_current_user();
}

if ($_POST['category'] == constant("SUBSFEE")){
	$error = check_quarter_status($_POST['paid_till'], $user->user_login);
} else {
	$error = check_items_status ($_POST['order']['description']);
}
if ($_SESSION["payment_type"] == "EBL") {
	require get_template_directory()."/inc/ebl/configuration.php";
	require get_template_directory()."/inc/ebl/skypay.php";
	if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['order']['amount'])) {
		if (empty($error)) {
			$order_id = create_invoice($user->user_login, $_POST['order']['amount']);
			$skypay = new skypay($configArray);
			$orderArray["order"]["id"]= $order_id;
			$orderArray["order"]["amount"]= $_POST['order']['amount'];
			$orderArray["order"]["description"]= $_POST['order']['description'];
			$orderArray["order"]["currency"]= EBL_ORDER_CURRENCY;
			$orderArray["interaction"]["cancelUrl"]= get_permalink(EBL_CALCEL_PAGE_ID)."?order=$order_id";
			$orderArray["interaction"]["returnUrl"]= get_permalink(EBL_RETURN_PAGE_ID)."?order=$order_id";
			$orderArray["interaction"]["timeoutUrl"]= get_permalink(EBL_TIMEOUT_PAGE_ID)."?order=$order_id";
			$orderArray["interaction"]["timeout"]= EBL_TIMEOUT;
			$orderArray["interaction"]["operation"]= EBL_OPERATION;
			$orderArray["interaction"]["merchant"]["name"]= EBL_MARCHANT_NAME;
			$orderArray["interaction"]["merchant"]["logo"]= EBL_MARCHANT_LOGO;
			$orderArray["interaction"]["displayControl"]["billingAddress"]= EBL_BILLING_ADDRESS;
			$orderArray["interaction"]["displayControl"]["orderSummary"]= EBL_ORDER_SUMMARY;
			$responseArray = $skypay->Checkout($orderArray);
		}
	}
} elseif ($_SESSION["payment_type"] == "SSL") {
	require_once(get_template_directory()."/inc/ssl/lib/SslCommerzNotification.php");
	if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['order']['amount'])) {
		if (empty($error)) {
			$user_details = get_user_list($user->user_login);
			$post_data = array();
			$post_data['total_amount'] = $_POST['order']['amount'];
			$post_data['currency'] = EBL_ORDER_CURRENCY;
			$post_data['tran_id'] = create_invoice($user->user_login, $_POST['order']['amount']);
			# CUSTOMER INFORMATION
			$post_data['cus_name'] = $user->display_name;
			$post_data['cus_email'] = $user->user_email;
			$post_data['cus_add1'] = $user_details->home_address;
			$post_data['cus_city'] = "";
			$post_data['cus_country'] = "";
			$post_data['cus_phone'] = $user_details->mobile_number;
			# SHIPMENT INFORMATION
			$post_data['shipping_method'] = "NO";
			$post_data['product_name'] = $_POST['order']['description'];
			$post_data['product_category'] = $_POST['category'];
			$post_data['product_profile'] = "general";
			# OPTIONAL PARAMETERS
			$post_data['value_a'] = $_POST['order']['description'];
			$post_data['value_b'] = $user->user_login;
			$post_data['value_c'] = $_POST['paid_till'];
			#EMI 
			if ($_POST['EMI'] == 1) {
				$post_data['emi_option'] = $_POST['EMI'];
				$post_data['emi_allow_only'] = 1;
				$post_data['multi_card_name'] = "visacard,mastercard,amexcard";
			}
			$sslcomz = new SslCommerzNotification();
			$result = $sslcomz->makePayment($post_data, 'hosted');
		}
	}
}
get_header();
add_filter('show_admin_bar', '__return_false');
do_action( 'hestia_before_single_page_wrapper' );
?>
<div class="<?php echo hestia_layout(); ?> verticallyCenterContents">
	<div class="container paymentStatus-cont">
		<div class="row completed">
			<div class="col-md-12 text-center align-middle">
				<i class="far fa-frown successIcon"></i>
				<h2 class="h2"><?php echo !empty($error) ? $error : 'Payment error, Please try again.'?></h2>
				<h4 class="h4">If you have any query please notify info@cccl.com.bd or +88028831896</h4>
				<?php 
				if($_SESSION["payment_type"] == "EBL"){				
					empty($error) ? $skypay->pr($responseArray): "";
				}elseif ($_SESSION["payment_type"] == "SSL") {
					echo !empty($error) ? $error: "";
				}
				?>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>