<?php /* Template Name: cccl_sales_by_category_report */ ?>
<?php get_header(); 
add_filter('show_admin_bar', '__return_false');
do_action( 'hestia_before_single_page_wrapper' );
get_the_ID() == TEMP_REPORT ? $is_tmp_rpt = true : $is_tmp_rpt = false;
$dtclass = $is_tmp_rpt  ? " tmpdateFields" : " dateFields";
if ($is_tmp_rpt) {
	$saleFrom = $_POST['saleFrom'] ? $_POST['saleFrom'] : date("d-M-Y") ;
	$saleTo = $_POST['saleTo'] ? $_POST['saleTo'] : date('d-M-Y', strtotime('+1 month'));
} else {
	$saleFrom = $_POST['saleFrom'] ? $_POST['saleFrom'] : date('d-M-Y', strtotime('-7 days')) ;
	$saleTo = $_POST['saleTo'] ? $_POST['saleTo'] : date("d-M-Y");
}
?>
<div class="<?php echo hestia_layout(); ?>">
    <div class="container sales_by_cat_report">
    	<div class="row search_form">
			<div class=" col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
				<div class="card card-form">
					<form id=salesReportForm method="post">
						<div class="row-fluid">
							<label for="slctCat" class="col-md-3 col-form-label">Select Category</label>
							<div class="col-md-9">
								<select class="form-control inputField slctCat" id="slctCat" name="slctCat" onchange="populateProduct()">
									<option value="">All</option>
									<option <?php if ( $_POST['slctCat'] == constant("VENUE") ) echo "Selected"; ?>  value="<?php echo constant("VENUE") ?>"><?php echo constant("VENUE") ?></option>
									<?php if(!$is_tmp_rpt) {?>
									<option <?php if ( $_POST['slctCat'] == constant("SERVICE") ) echo "Selected"; ?> value="<?php echo constant("SERVICE") ?>"><?php echo constant("SERVICE") ?></option>
									<option <?php if ( $_POST['slctCat'] == constant("EVENT") ) echo "Selected"; ?>  value="<?php echo constant("EVENT") ?>"><?php echo constant("EVENT") ?></option>
									<option <?php if ( $_POST['slctCat'] == constant("PAYMENT") ) echo "Selected"; ?>  value="<?php echo constant("PAYMENT") ?>"><?php echo constant("PAYMENT") ?></option>
									<?php }?>
								</select>
							</div>
						</div>
						<div class="row-fluid selectProdDiv">
							<label for="slctProd" class="col-md-3 col-form-label">Select Product</label>
							<div class="col-md-9">
								<select class="form-control inputField" id="slctProd" name="slctProd" >
									<option value="All"></option>
								</select>
							</div>
						</div>
						
						<div class="row-fluid">
							<label for="saleFrom" class="col-md-3 col-form-label">Select <?php echo $is_tmp_rpt ? " Booking " : ""?> Date</label>
							<div class="col-md-4">
								<input type="text" class=" inputField <?php echo $dtclass;?> " id="saleFrom" name="saleFrom" value="<?php echo $saleFrom ; ?>" aria-label="Input group example" aria-describedby="btnGroupAddon">
								<div class="input-group-append btnGroupAddon datepickerIcon">
									<div class="input-group-text"><i class="fa fa-calendar"></i></div>
								</div>
							</div>
							<div class="col-md-1">
								<p for="saleTo" class=" col-form-label text-center ">To</p>
							</div>
							
							<div class="col-md-4">
								<input type="text" class=" inputField <?php echo $dtclass;?> " id="saleTo" name="saleTo" value="<?php echo $saleTo; ?>" aria-label="Input group example" aria-describedby="btnGroupAddon">
								<div class="input-group-append btnGroupAddon datepickerIcon">
									<div class="input-group-text"><i class="fa fa-calendar"></i></div>
								</div>
							</div>
						</div>
						<div class="row-fluid">
						    <div class="col-md-12 text-right">
						      <button type="submit" class="btn btn-primary mb-2 btnSearch">Search</button>
						    </div>
						</div>
						
					</form>
				</div>
			</div>
		</div>
    	<div id="message"></div>
    	<div class="row get-pass-sec">
        	<div class="col-md-12">
             	<h2 class="h2"><?php echo $is_tmp_rpt ? "Temporary Booking" : "Sales";?> Report</h2>
             	<hr>
         	</div>
        </div>
    	<div class="row">
    		<div class="col-md-12">
    		<table id="salesReportTable" class="table table-striped datatable" style="display: none;">
                <thead>
                    <tr>
                    	<th>Date</th>
                    	<th>Category</th>
                        <th>Sub-Category</th>
                        <th>Ticket</th>
						<th>Quantity</th>
						<th>Person</th>
						<th class="text-right">Service Charge</th>
						<th class="text-right">VAT</th>
                        <th class="text-right">Amount</th>
                        <th class="text-right">Discount</th>
                        <th class="text-right">Total</th>
                        <th>Member ID</th>
                        <th>Name</th>
                        <?php if ($is_tmp_rpt) {?>
                        <th class="no-sort"></th>
                        <?php }?>
                        
                    </tr>
                </thead>
                <tbody>
                	<?php 
	                	$results = get_sales_report_data($is_tmp_rpt);
	                	$url = get_permalink(TEMP_RELEASE);
                        if (count($results)>0) {
                        	foreach ($results as $value) {
                        		if ($value->product_category == constant('VENUE') && !empty($value->type)) {
                        			$sft = array("M" => "Morning", "A" => "Afternoon", "E" => "Evening" );
                        			$lt = explode('#', $value->type);
                        			$type = custom_date('d-M-Y', $lt[0]).' '. $sft[$lt[1]];
                        		} else {
                        			$type = $value->type;
                        		}
                     ?>
                        <tr class=<?php echo $is_tmp_rpt && $value->comment == "Hold" ? "release" : "";?> id="<?php echo 'trid_'.$value->id?>">
                        	<td>
                        		<?php if ($value->purchase_date != "") echo date("d-M-Y H:i:s", strtotime($value->purchase_date));?>
                        	</td>
                            <td><?php echo $value->product_category;?></td>
                            <td class="product"><?php echo $value->product_name;?></td>
                            <td class="product_item"><?php echo $type;?></td>
                            <td><?php echo $value->quantity;?></td>
                            <td><?php echo $value->person;?></td>
                            <td class="text-right padding-right"><?php echo number_format($value->ser_charge,2);?></td>
                            <td class="text-right padding-right"><?php echo number_format($value->vat_amount,2);?></td>
                            <td class="text-right padding-right"><?php echo number_format($value->t_amount,2);?></td>
                            <td class="text-right padding-right"><?php echo number_format($value->discount_amount,2);?></td>
                            <td class="text-right padding-right"><?php echo number_format($value->ser_charge + $value->vat_amount + $value->t_amount - $value->discount_amount,2); ?></td>
                            <td><?php echo $value->user_login;?></td>
                            <td><?php echo $value->display_name;?></td>
                            <?php 
                            if ($is_tmp_rpt) {
                            	$link = "<a class='btn btn-primary pull-left item_$value->id' product='$value->product_name' item='$type' onclick=releaseTempItems(this,'$value->item_id','$value->id') >Release</a>"
                            ?>
                            <td><?php echo $value->comment == "Hold" ? $link : ""?> </td>
                            <?php }?>
                        </tr>
                    	<?php 
                        		}
                        	}
                       	?>
                </tbody>
            </table> 
            </div>
    	</div>
    </div>
</div>
<?php 
echo create_modal("tmprelease", "Temporary booking release");
get_footer(); ?>

<script>
jQuery(document).ready(function($) {
	populateProduct('<?php echo $_POST['slctProd']; ?>');
	
	var salesReportTable = $('#salesReportTable').DataTable({
		<?php 
		$user = wp_get_current_user();
		if ( $user->roles{0}  == 'contributor' || $user->roles{0}  == 'author' || $user->roles{0}  == 'administrator') {
		?>
		dom: "<'row'<'col-md-3 col-xs-4'l><'col-md-8 col-xs-5'><'col-md-9 col-xs-4'B>>" +
	    "<'row'<'col-md-12'tr>>" +
	    "<'row'<'col-sm-6 col-xs-12'i><'col-sm-6 col-xs-12'p>>",
        buttons: [
      			{ extend: 'csv',className: "btn btn-primary", text: '<i class="fas fa-download"></i>' },
              ],
        <?php } ?>
		responsive: true,
		order : [],
		"columnDefs" : [  {
			"targets" : 'no-sort',
			"orderable" : false
		}],
	    "pageLength": 50,
		"initComplete": function() {
			$("#salesReportTable").show();
		}
	});
	
});
</script>
