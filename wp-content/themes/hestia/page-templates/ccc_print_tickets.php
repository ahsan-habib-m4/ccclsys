<?php /* Template Name: cccl_print_tickets */ ?>
<?php get_header(); 
add_filter('show_admin_bar', '__return_false');
do_action( 'hestia_before_single_page_wrapper' );
if ($_POST['task']== "print_ticket"){
	print_tickets ();
}
?>
<div class="<?php echo hestia_layout(); ?>">
    <div class="container sales_by_cat_report">
    	<div class="row search_form">
			<div class=" col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
				<div class="card card-form">
					<form id=salesReportForm method="post">
						<div class="row-fluid">
							<label for="slctCat" class="col-md-3 col-form-label">Select Category</label>
							<div class="col-md-9">
								<select class="form-control inputField slctCat" id="slctCat" name="slctCat" onchange="populateProduct()">
									<option value="SE">All</option>
									<option <?php if ( $_POST['slctCat'] == constant("SERVICE") ) echo "Selected"; ?> value="<?php echo constant("SERVICE") ?>"><?php echo constant("SERVICE") ?></option>
									<option <?php if ( $_POST['slctCat'] == constant("EVENT") ) echo "Selected"; ?>  value="<?php echo constant("EVENT") ?>"><?php echo constant("EVENT") ?></option>
								</select>
							</div>
						</div>
						<div class="row-fluid selectProdDiv">
							<label for="slctProd" class="col-md-3 col-form-label">Select Product</label>
							<div class="col-md-9">
								<select class="form-control inputField" id="slctProd" name="slctProd" >
									<option value="All"></option>
								</select>
							</div>
						</div>
						<div class="row-fluid">
							<label for="mmbrId" class="col-md-3 col-form-label">Member ID</label>
							<div class="col-md-9">
								<input type="text" class="form-control inputField" id="mmbrId" name="mmbrId" value="<?php echo $_POST['mmbrId']?>">
							</div>
						</div>
						<div class="row-fluid">
							<label for="saleFrom" class="col-md-3 col-form-label">Select Date</label>
							<div class="col-md-4">
								<input type="text" class=" inputField dateFields " id="saleFrom" name="saleFrom" value="<?php echo $_POST['saleFrom'] ? $_POST['saleFrom'] : date('d-M-Y', strtotime('-7 days')) ; ?>" aria-label="Input group example" aria-describedby="btnGroupAddon">
								<div class="input-group-append btnGroupAddon datepickerIcon">
									<div class="input-group-text"><i class="fa fa-calendar"></i></div>
								</div>
							</div>
							<div class="col-md-1">
								<p for="saleTo" class=" col-form-label text-center ">To</p>
							</div>
							
							<div class="col-md-4">
								<input type="text" class=" inputField dateFields " id="saleTo" name="saleTo" value="<?php echo $_POST['saleTo'] ? $_POST['saleTo'] : date("d-M-Y"); ?>" aria-label="Input group example" aria-describedby="btnGroupAddon">
								<div class="input-group-append btnGroupAddon datepickerIcon">
									<div class="input-group-text"><i class="fa fa-calendar"></i></div>
								</div>
							</div>
						</div>
						<div class="row-fluid">
						    <div class="col-md-12 text-right">
						      <button type="submit" class="btn btn-primary mb-2 btnSearch">Search</button>
						    </div>
						</div>
						
					</form>
				</div>
			</div>
		</div>
    	
    	<div class="row get-pass-sec">
        	<div class="col-md-12">
             	<h2 class="h2">Print tickets</h2>
             	<hr>
         	</div>
        </div>
    	<div class="row">
    		<div class="col-md-12">
    		<table id="collectionReportTable" class="table table-striped datatable">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Receipt no</th>
                        <th>Amount</th>
                        <th>Category</th>
                        <th>Sub-Category</th>
                        <th>Member ID</th>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                $results = get_membership_collection_report("SE");
                if(count($results)>0) {
                	foreach ($results as $value) {?>
                    <tr>
                        <td>
                        	<?php if($value->payment_date != "") echo date("d-M-Y H:i:s", strtotime($value->payment_date));?>
                        </td>
                        <td><?php echo $value->receipt_number;?></td>
                        <td class="padding-right">
                        	<?php echo number_format($value->amount,2); ?>
                        </td>
                        <td><?php echo $value->category;?></td>
                    	<td><?php echo $value->product_name;?></td>
                        <td>
                        	<?php 
                                $fullid = explode("-",$value->member_full_id);
                                echo $fullid[4];
                            ?>
                        </td>
                        <td><?php echo $value->member_name;?></td>
                    </tr>
               	<?php }
                }
               	?>
               	
                </tbody>
            </table> 
            </div>
    	</div>
    </div>
</div>
<?php get_footer(); ?>

<script>
jQuery(document).ready(function($) {
	populateProduct('<?php echo $_POST['slctProd']; ?>');
	var membershipReportTable = $('#collectionReportTable').DataTable({
		<?php 
		$user = wp_get_current_user();
		if ( $user->roles{0}  == 'contributor' || $user->roles{0}  == 'author' || $user->roles{0}  == 'administrator') {
		?>
		dom: "<'row'<'col-md-3 col-xs-4'l><'col-md-8 col-xs-5'f><'col-md-1 col-xs-3'B>>" +
			 "<'row'<'col-md-12'tr>>" +
			 "<'row'<'col-md-8 col-xs-6'i><'col-sm-4 col-xs-6'p>>",
        buttons: [
			{ className: "dt-button btn btn-primary buttons-csv buttons-html5", text: '<i class="fas fa-print" title="Print Ticket(S)"></i>',
                action: function ( e, dt, node, conf ) {
                	var slctCat = $("#slctCat").val();
                	var slctProd = $("#slctProd").val();
                	var mmbrId = $("#mmbrId").val();
                	var saleFrom = $("#saleFrom").val();
                	var saleTo = $("#saleTo").val();
                    var newForm = jQuery('<form>', {
                        'method': 'post'
                    }).append(jQuery('<input>', {
                        'name': 'slctCat',
                        'value': slctCat,
                        'type': 'hidden'
                    })).append(jQuery('<input>', {
                        'name': 'slctProd',
                        'value': slctProd,
                        'type': 'hidden'
                    })).append(jQuery('<input>', {
                        'name': 'mmbrId',
                        'value': mmbrId,
                        'type': 'hidden'
                    })).append(jQuery('<input>', {
                        'name': 'saleFrom',
                        'value': saleFrom,
                        'type': 'hidden'
                    })).append(jQuery('<input>', {
                        'name': 'saleTo',
                        'value': saleTo,
                        'type': 'hidden'
                    })).append(jQuery('<input>', {
                        'name': 'task',
                        'value': 'print_ticket',
                        'type': 'hidden'
                    }));
                    newForm.hide().appendTo("body").submit()
                }
			}
        ],
        <?php } ?>
		responsive: true,
		"pageLength": 50,
		order : [],
		"columnDefs" : [  {
			"targets" : 'no-sort',
			"orderable" : false
		},
		 {
	        targets: 2,
	        className: 'text-right'
	    } ]
	});
	
});
</script>
