<?php /* Template Name: cccl_audit_report */ ?>


<?php get_header(); 
add_filter('show_admin_bar', '__return_false');
do_action( 'hestia_before_single_page_wrapper' );
global $post;
$post_slug = $post->post_name;
$category = "";
if($post_slug == "directory-audit") {
    $category = MEMBER;
} else if ($post_slug == "subscription-audit") {
    $category = SUBSFEE;
}
$user = wp_get_current_user();
?>
<div class="<?php echo hestia_layout(); directoryReportMainCont ?> auditLogReportCont">
    <div class="container directory_report">
    	<form id="auditLogForm" method="post" autocomplete="off">
            <div class="row">
            	<div class="col-md-10 col-md-offset-1 col-xs-12 col-sm-12">
                     <div class="card card-form-horizontal collectionReportFormDiv">
    					<div class="content">
    						<div class="row">
    							<div class="textwidget">
    								<label for="clctnFrom" class="col-md-2 col-form-label">Audit From</label>
                                    <div class="col-md-3">
                                        <input type="text" class=" inputField dateFields" id="auditFrom" name="auditFrom" value="<?php echo $_POST['auditFrom'] ? $_POST['auditFrom'] :  date('d-M-Y', strtotime('-7 days')) ; ?>" required placeholder="Required" aria-label="Input group example" aria-describedby="btnGroupAddon">
                                        <div class="input-group-append btnGroupAddon datepickerIcon">
                                        	<div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                    <label for="clctnTo" class="col-md-2 col-form-label text-center">To</label>
                                    <div class="col-md-3">
                                        <input type="text" class=" inputField dateFields" id="auditTo" name="auditTo" value="<?php echo  $_POST['auditTo'] ? $_POST['auditTo'] : date("d-M-Y"); ?>" required placeholder="Required" aria-label="Input group example" aria-describedby="btnGroupAddon">
                                        <div class="input-group-append btnGroupAddon datepickerIcon">
                                        	<div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                     <div class="col-md-2 text-center">
                                     	<button type="submit" class="btn btn-primary mb-2 auditreportbtn">Search</button>
                                     </div>
    							</div>
    						</div>
    					</div>
    				</div>
				</div>
            </div>
    	</form>
    	
    	<div class="row get-pass-sec">
        	<div class="col-md-12">
             	<h2 class="h2">Audit Log</h2>
             	<hr>
         	</div>
        </div>
    	<div class="row">
    		<div class="col-md-12">
    		<table id="auditReportTable" class="table table-striped datatable">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Category</th>
                        <th>User</th>
						<th>For</th>
                        <th>Changes</th>
                    </tr>
                </thead>
                <tbody>
                     <?php 
                     $results = get_audit_log($category);
                        if(count($results)>0) {
                        	foreach ($results as $value) {?>
                            <tr>
                                <td>
                                	<?php if($value->created_on != "") echo date("d-M-Y H:i:s", strtotime($value->created_on));?>
								</td>
                                <td>
                                	<?php echo $value->category;?>
                                </td>
                                <td>
                                	<?php echo $value->user_id;?>
                                </td>
                                <td>
                                	<?php echo $value->member_id;?>
                                </td>
                                <td><?php echo $value->note;?></td>
                            </tr>           	
                       	<?php }
                        }
                       	?>
                </tbody>
            </table> 
            </div>
    	</div>
    </div>
</div>
<?php get_footer(); ?>

<script>
jQuery(document).ready(function($) {
	var auditReportTable = $('#auditReportTable').DataTable({
		rowReorder: {
			selector: 'td:nth-child(3)'
		},		
		responsive: true,
		order : [],
		"columnDefs" : [  {
			"targets" : 'no-sort',
			"orderable" : false
		} ],
		"pageLength": 50
	});
	
});
</script>