<?php /* Template Name: ccc_payment_history */ ?>


<?php get_header(); 
add_filter('show_admin_bar', '__return_false');
do_action( 'hestia_before_single_page_wrapper' );
$user = wp_get_current_user();
?>
<div class="<?php echo hestia_layout(); ?>">
    <div class="container add_collection_report">
    	<div class="row get-pass-sec">
        	<div class="col-md-12">
             	<h2 class="h2">Payment History</h2>
             	<hr>
         	</div>
        </div>
    	<div class="row">
    		<div class="col-md-12">
    		<table id="collectionReportTable" class="table table-striped datatable">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Receipt no</th>
                        <th>Amount</th>
                        <th>Payment type</th>
                        <th>Received by</th>
                        <th>Payment reference</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                $results = get_membership_payment_history();
                if(count($results)>0) {
                	foreach ($results as $value) {?>
                    <tr>
                        <td>
                        	<?php if($value->payment_date != "") echo date("d-M-Y", strtotime($value->payment_date));?>
                        </td>
                        <td><?php echo $value->receipt_number;?></td>
                        <td>
                        	&#x9f3; <?php echo number_format($value->amount,2); ?>
                        </td>
                        <td><?php echo $value->payment_type;?></td>
                        <td><?php echo $value->receiver_name;?></td>
                        <td><?php echo $value->payment_reference;?></td>
                    </tr>
               	<?php }
                }
               	?>
               	
                </tbody>
            </table> 
            </div>
    	</div>
    </div>
</div>
<?php get_footer(); ?>


<script>
jQuery(document).ready(function($) {
	var membershipReportTable = $('#collectionReportTable').DataTable({
		rowReorder: {
			selector: 'td:nth-child(3)'
		},		
		responsive: true,
		order : [],
		"pageLength": 50,
		"columnDefs" : [  {
			"targets" : 'no-sort',
			"orderable" : false
		},
		 {
	        targets: 2,
	        className: 'text-right'
	    } ]
	});
	
});
</script>