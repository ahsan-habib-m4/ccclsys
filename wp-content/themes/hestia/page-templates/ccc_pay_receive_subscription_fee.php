<?php /* Template Name: ccc_pay_receive_subscription_fee */ ?>

<?php 
get_header(); 
add_filter('show_admin_bar', '__return_false');
do_action( 'hestia_before_single_page_wrapper' );
$user = wp_get_current_user();
global $post;
$user_details= array();
$post_slug = $post->post_name;
if ($_POST['task'] == "searchMember") {
	$user_details = get_user_list($_POST["userLogin"]);
}elseif ($_POST['task'] == "receivePayment") {
	$error = receive_payment();
}
?>
<div class="<?php echo hestia_layout(); ?>">
	<div class="container pay_subscription_fee_div ">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 divwarpper col-xs-12 ">
				<?php if ($post_slug == "receive-payment") { ?>
				<?php 
				if ($_POST['task'] == "searchMember"){
					if(!count($user_details)>0) {
						echo show_custom_message("Member details not found", "warning");
					}
				}elseif ($_POST['task'] == "receivePayment") {
					if(empty($error)) {
						echo show_custom_message("Payment information saved successfully", "success");
						$user_details = get_user_list($_POST["userLogin"]);
					}else{
						echo show_custom_message($error, "warning");
					}
				}
				?>
				<div class="row">
					<div class="col-md-12">
						<form id="searchMemberById" method="post" autocomplete="off">
							<div class="card card-form-horizontal">
								<div class="content">
									<div class="row">
										<div class="textwidget">
											<div class="col-sm-8">
												<div class="input-group">
													<span class="input-group-addon"></span>
													<div class="form-group is-empty">
														<input type="text" class=" form-control" id="userLogin" name="userLogin" value="<?php echo $_POST["userLogin"]?>" maxlength="5" required="required" placeholder="Last five digits of Member ID">
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<input type="submit" class="btn btn-primary btn-block sib-default-btn" name="submit" value="Search">
												<input type="hidden" name="task" value="searchMember">
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<?php if(count($user_details)>0) {?>	
				<div class="row member-info">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-3 col-xs-4 col-sm-2"><label>ID:</label></div>
							<div class="col-md-8 col-xs-7 col-sm-9"><span class="highlight memberId"><?php echo $user_details->member_full_id?></span></div>
						</div>
						<div class="row">
							<div class="col-md-3 col-xs-4 col-sm-2"><label>Name: </label></div>
							<div class="col-md-8 col-xs-7 col-sm-9"><span class="highlight memberName"><?php echo $user_details->display_name?></span></div>
						</div>
						<div class="row">
							<div class="col-md-3 col-xs-4 col-sm-2"><label>Type:</label></div>
							<div class="col-md-8 col-xs-7 col-sm-9"><span class="highlight memberType"><?php echo $user_details->member_type?></span></div>
						</div>
						<div class="row">
							<div class="col-md-3 col-xs-4 col-sm-2"><label>Status:</label></div>
							<div class="col-md-8 col-xs-7 col-sm-9"><span class="highlight memberType"><?php echo $user_details->status?></span></div>
						</div>
					</div>
				</div>
 					<?php }?>
				<?php } 
				if (($_POST['task'] == "searchMember" || $_POST['task'] == "receivePayment") && count($user_details)>0) {
					$result = member_payment_details($user_details->user_login);
				}else{
					$result = member_payment_details($user->user_login);
				}
				if(count($user_details)>0 || $post_slug == "pay-subscription-fee") {
					if($result["due_amount"] <= 0) {
						$quarter_list= get_advanced_quarter_list($result["payment_upto"]);
					}else{
						$quarter_list= get_advanced_quarter_list();
					}
					if ($_POST['task'] != "searchMember"  && !count($user_details)>0) {
						$user_details= get_user_list($user->user_login);
					}
					$quarter_map = array("Q1"=> "Jan - Mar","Q2"=> "Apr - Jun","Q3"=> "Jul - Sep","Q4"=> "Oct - Dec");
					if($user_details->subcription_required == 'N' || !isMemberCanPay ($user_details)) {
				?>
				<div class="row">
					<div class="col-md-12">
					<h3><?php echo "Subscription is not applicable for "; echo $user_details->member_type?> member</h3>
					</div>
				</div>
				<?php 
					}else{
						$_SESSION["paid_till"] = $user_details->payment_upto;
					?>
				<div class="row subFeeSttsDiv">
					<div class="col-md-12">
						<h2 class="h3 paidUpto"><i class="fas fa-check-circle text-success"></i> Subscription is paid up to <span class="quarter"><?php echo $result["quarter"]?></span> of <span class="year"><?php echo $result["year"]?></span></h2>
						<h3 class="h3 currentDue"><i class="fas fa-exclamation-circle text-warning"></i> Current due is <span class="currency">BDT</span> <span class="amount"><?php echo number_format($result["due_amount"], 2);?></span></h3>
					</div>
				</div>
				<?php 
				if ($result["due_amount"] > 0) {
				$due_list = get_due_amount_break_down_list($user_details)?>
				<div class="row">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-12">
        						<h4 class="h4">Due payment</h4>
        					</div>
        				</div>
        				<div class="row">
        					<div class="col-md-12 checkboxesdiv">
        					<?php
        					foreach ($due_list as $quarter => $value) {
        						if($user_details->status == "Active" || $user_details->status == "Suspended" || $user_details->status == "User"){
        							$due_fee = number_format($value["general_fee"], 2);
        						}elseif ($user_details->status == "Abroad"){
        							$due_fee = number_format($value["abroad_fee"], 2);
        						}else{
        							$due_fee = 0;
        						}
        						$due_year = substr($quarter,0,4);
        						$due_quarter = substr($quarter,-2);
        					?>
							<div class="form-check">
								<input class="form-check-input paycheck duepaycheck" type="checkbox" value="<?php echo $quarter?>" id="<?php echo $quarter;?>" name="<?php echo $quarter;?>" autocomplete="off" checked="checked">
								<label class="form-check-label" for="<?php echo $quarter;?>">
									<?php echo $due_year;?> - <?php echo $due_quarter;?> ( <?php echo $quarter_map[$due_quarter] ?> ) : <span class="currency">BDT</span> <span class="amount"><?php echo $due_fee;?></span> 
								</label>
							</div>
        					<?php 
        					 }
        					?>
        					</div>
        				</div>
        			</div>
        		</div>
				<br>
				<?php }?>
				<?php if(!empty($quarter_list)) {?>
				<div class="row">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-12">
        						<h4 class="h4">Advance payment</h4>
        					</div>
        				</div>
        				<div class="row">
        					<div class="col-md-12 checkboxesdiv">
        					<?php
							foreach ($quarter_list as $value) {
        						if($user_details->status == "Active" || $user_details->status == "Suspended" || $user_details->status == "User"){
        							$subscription_fee = number_format($value->subscription_fee, 2);
        						}elseif ($user_details->status == "Abroad"){
        							$subscription_fee = number_format(($value->subscription_fee * $value->abroad_fee)/100, 2);
        						}else{
        							$subscription_fee= 0;
        						}
        					?>
							<div class="form-check">
								<input class="form-check-input paycheck advpaycheck" type="checkbox" value="<?php echo $value->quarter_year?>" id="<?php echo $value->quarter_year?>" name="<?php echo $value->quarter_year?>" autocomplete="off">
								<label class="form-check-label" for="<?php echo $value->quarter_year?>">
									<?php echo $value->year?> - <?php echo $value->quater?> ( <?php echo $quarter_map[$value->quater] ?> ) : <span class="currency">BDT</span> <span class="amount"><?php echo $subscription_fee;?></span> 
								</label>
							</div>
        					<?php 
        					 }
        					?>
        					</div>
        				</div>
        			</div>
        		</div>

        		<br>
        		<div class="row totalamountdiv">
        			<div class="col-md-12">
        				<h4 class="h4"><strong>Total Amount</strong> <span class="currency">BDT</span> <span class="amount totalAmount"><?php echo !empty($_POST["order[amount]"]) ? number_format($_POST["order[amount]"], 2) : number_format($result["due_amount"], 2);?></span></h4>
        				<input type="hidden" class="dueAmmount" value="0">
        			</div>
        		</div>
        		<?php }?>
				<?php 
					}
				}?>
				<?php if ( (!empty($quarter_list) || $result["due_amount"]>0) && $post_slug == "pay-subscription-fee" && $user_details->subcription_required != 'N' && ($user_details->status != 'Expired')) { 
					$current_month= date("m");
					$current_quarter = get_corrent_quarter ($current_month);
				?>
        		<div class="row">
        			<div class="col-md-12">
        				<div class="row">
        					<div class="col-md-12">
        						<h4 class="h4">Select payment gateway</h4>
        					</div>
        				</div>
        				<form action="<?php echo get_permalink(208)?>" method="post" autocomplete="off">
        					<div class="row">	
        						<div class="col-md-12 checkboxesdiv">
        							<div class="form-radio radio">
        								<input class="form-check-input radio__input" type="radio" name="paymentType" id="eblpayment" value="EBL" checked>
        								<label class="form-check-label radio__label" for="eblpayment">
        									Visa / Master
        								</label>
        							</div>
        							<div class="form-radio radio">
        								<input class="form-check-input radio__input" type="radio" name="paymentType" id="sslpayment" value="SSL">
        								<label class="form-check-label radio__label" for="sslpayment">
        									Others
        								</label>
        							</div>
        							<button class="processPaymentBtn" type="submit" <?php echo $result["due_amount"] <=0 ? 'disabled' : ''?> >Pay Now</button>
        							<input type="hidden" name="order[amount]" id="eblTotalAmount" value="<?php echo $result["due_amount"];?>"/>
									<input type="hidden" name="order[description]" id="quarterSelection" value="<?php echo $result["due_amount"] > 0 ? $current_quarter:''?>">
									<input type="hidden" name="receiveType" value="<?php echo constant('PAYMENT_ONLINE')?>">
									<input type="hidden" name="category" value="<?php echo constant("SUBSFEE")?>">
									<input type="hidden" name="paid_till" value="<?php echo $user_details->payment_upto;?>">
        						</div>
        					</div>
        				</form>
        			</div>
        		</div>
        		<?php } ?>
        		<?php 
        		if ((!empty($quarter_list) || $result["due_amount"] > 0) && $post_slug == "receive-payment" && count($user_details)>0 && $user_details->subcription_required != 'N' && isMemberCanPay ($user_details)) { 
					$current_month= date("m");
	        		$current_quarter = get_corrent_quarter ($current_month);
        		?>
        		<div class="row">
        			<div class="col-md-12">
        				<form class="form-horizontal receivePaymentForm" id="receivePaymentForm" method="post" autocomplete="off">
                            <input type="hidden" class="form-control inputField" id="paymentAmount" name="paymentAmount" value="<?php echo number_format($result["due_amount"], 2);?>" readonly>
                            <div class="row">
                                <label for="paymentType" class="col-md-3 col-form-label">Payment Type</label>
                                <div class="col-md-4">
                                  	<select class="form-control inputField" id="paymentType" name="paymentType" required >
                                  		<option value="">Required</option>
                                  		<?php foreach (get_paymnet_type_list() as $value) {?>
                                  		<option value="<?php echo $value->id?>"><?php echo $value->name?></option>
                                  		<?php }?>
                                  	</select>
                                </div>
                            </div>
                            <div class="row">
                                <label for="paymentRef" class="col-md-3 col-form-label">Payment Reference</label>
                                <div class="col-md-4">
                                  <input type="text" class="form-control inputField" id="paymentRef" name="paymentRef" autocomplete="off">
                                </div>
                            </div>
                            <div class="row">
                                <label for="paymentRef" class="col-md-3 col-form-label">Password</label>
                                <div class="col-md-4">
                                  <input type="password" class="form-control inputField" id="password" name="password" required autocomplete="off">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
									<button class="receivePaymentBtn pull-right" type="submit" <?php echo $result["due_amount"] <= 0 ? 'disabled' : ''?>>Submit</button>
	       							<input type="hidden" name="amount" id="eblTotalAmount" value="<?php echo $result["due_amount"];?>"/>
									<input type="hidden" name="description" id="quarterSelection" value="<?php echo $result["due_amount"] > 0 ? $current_quarter:''?>">
	                                <input type="hidden" name="user_id" value="<?php echo $user_details->ID?>">
	                                <input type="hidden" name="userLogin" value="<?php echo $user_details->user_login?>">
	                                <input type="hidden" name="receiveType" value="<?php echo constant('PAYMENT_RECEIVED')?>">
	                                <input type="hidden" name="category" value="<?php echo constant("SUBSFEE")?>">
	                                <input type="hidden" name="task" value="receivePayment">
                                </div>
                            </div>
    					</form>
					</div>
        		</div>
        		<?php } ?>
        	</div>
        </div>
	</div>
</div>
<?php get_footer(); ?>