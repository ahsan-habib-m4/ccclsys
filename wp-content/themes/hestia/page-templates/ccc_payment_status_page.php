<?php /* Template Name: cccl_payment_status_page */ ?>

<?php 
get_header(); 
add_filter('show_admin_bar', '__return_false');
do_action( 'hestia_before_single_page_wrapper' );
?>
<div class="<?php echo hestia_layout(); ?> verticallyCenterContents">
	<div class="container paymentStatus-cont">
		<div class="row completed">
			<div class="col-md-12 text-center align-middle">
				<i class="far fa-thumbs-up successIcon"></i>
				<h2 class="h2">Thanks for the payment</h2>
				<h3 class="h3">Your Transaction is complete and receipt has been emailed</h3>
				<h4 class="h4">If you have anay query please notify info@cccl.com.bd or +88028831896</h4>
			</div>
		</div>
	</div>
	
	<div class="container paymentStatus-cont">
		<div class="row completed">
			<div class="col-md-12 text-center align-middle">
				<i class="far fa-frown successIcon"></i>
				<h2 class="h2">Payment Failed</h2>
				<h4 class="h4">If you have anay query please notify info@cccl.com.bd or +88028831896</h4>
			</div>
		</div>
	</div>
	
	<div class="container paymentStatus-cont">
		<div class="row completed">
			<div class="col-md-12 text-center align-middle">
				<i class="far fa-frown successIcon"></i>
				<h2 class="h2">Payment Canceled</h2>
				<h4 class="h4">If you have anay query please notify info@cccl.com.bd or +88028831896</h4>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>