<?php /* Template Name: cccl_getpassword */ ?>

<?php 
get_header(); 
add_filter('show_admin_bar', '__return_false');
do_action( 'hestia_before_single_page_wrapper' );
global $post;
$post_slug = $post->post_name;
$haveUser = false;
$sendPass = false;
if($_POST['task'] == "searchmember") {
	$result = get_user_list($_POST['userLogin']);
	if(count($result)>0) {
		$phoneno = substr($_POST['phoneno'],-10);
		$orgno = substr($result->mobile_number,-10);
		if ($phoneno == $orgno) $haveUser = true;
	}
}elseif ($_POST['task'] == "sendPass" && !empty($_POST['userLogin'])) {
	$sendPass = true;
}
?>
<div class="<?php echo hestia_layout(); ?>">
	<div class="container getpassword-cont">
		<div class="row">
			<h2 class="ip-addr h2 text-center text-warning">"Your IP address is <?php echo get_the_user_ip();?>"</h2>
			<br><br>
		</div>
		<div class="row">
			<div class="col-md-6 col-md-offset-3 getPassInnerCont col-sm-8 col-sm-offset-2">
        		<?php 
        		if ( $post_slug == "signup-get-password") {
        		    $loginPageUrl = home_url();
        		} else {
        		    $loginPageUrl = get_permalink(65);
        		}
        		if(!$haveUser && !$sendPass && $_POST['task'] == "searchmember") {
        			echo show_custom_message("Record not found", "warning");
        		}elseif ($sendPass && $_POST['task'] == "sendPass") {
        			if($_POST['receiveBy'] == "Email"){
	        			if(send_or_change_password($_POST['userLogin'])) {
	        				echo show_custom_message("Password has been sent your mail address.", "success");
	        			}else{
	        				echo show_custom_message("Password not sent successfully. Please try again.", "warning");
	        			}
        			}elseif ($_POST['receiveBy'] == "Sms"){
        				if(send_password_phone($_POST['userLogin'])) {
        					echo show_custom_message("Password has been sent your mobile phone.", "success");
        				}else{
        					echo show_custom_message("Password not sent successfully. Please try again.", "warning");
        				}
        			}
        		}
        		if ( $post_slug == "signup-get-password") { ?>
        		<form class="form-inline" id="findMember" method="post">
        			<div class="row">
        				<label for="userid" class="col-md-4 col-form-label col-sm-4">Membership ID</label>
        				<div class="col-md-5 col-sm-6">
        					<input type="text" class="form-control txt-input" id="userLogin" maxlength="5" name="userLogin" required placeholder="Required" value="<?php echo $_POST['userLogin']; ?>">
        				</div>
        			</div>
        			<div class="row">
        				<label for="userid" class="col-md-4 col-form-label col-sm-4">Phone number</label>
        				<div class="col-md-5 col-sm-6">
        					<input type="text" class="form-control inputField" id="phoneno" name="phoneno" placeholder="Phone no" required value="<?php echo $_POST['phoneno']; ?>" >
        				</div>
        			</div>
        			<div class="row">
        				<div class="col-md-9 col-sm-10">
        					<button type="submit" class="btn btn-primary col-mb-2 pull-right findMemberBtn">Find</button>
        					<?php if ($sendPass){ ?>
        					<a class="btn btn-primary pull-right" href="<?php echo home_url(); ?>">Back to LOGIN</a>
        					<?php } ?>
        					<input type=hidden name="task" value="searchmember">
        				</div>
        			</div>
        		</form>
        		<?php if($haveUser && !$sendPass) {
        			$mMobile = $result->mobile_number;
        			if (strlen($mMobile) <= 11 ) {
        				$maskedNo = substr($mMobile, 0, 4) . "****" . substr($mMobile, 8);
        			} else {
        				$maskedNo = substr($mMobile, 0, 7) . "****" . substr($mMobile, 12);
        			}
        			$email = $result->user_email;
        			$email = explode("@", $email);
        			$email_part = $email[0];
        			$domain_part = $email[1];
        			$mask_email = mask($email_part, 1, 1)."@". $domain_part;
				?>
        		<div class="row member-info">
        			<div class="col-md-12">
        				<label>Full ID: <span class="highlight memberId"><?php echo $result->member_full_id;?></span></label>
        				<label>Name: <span class="highlight memberName"><?php echo $result->display_name;?></span></label>
        				<label>Phone: <span class="highlight memberPhone"><?php echo $maskedNo?></span></label>
        				<label>Email: <span class="highlight memberPhone"><?php echo $mask_email?></span></label>
        				
        			</div>
        		</div>
        		<?php }?>
        		<?php } else { ?>
        		<form id="findMember"  method="post">
        			<div class="row">
        				<label for="userid" class="col-md-5 col-form-label col-sm-5">User name</label>
        				<div class="col-md-5 col-sm-6">
        					<input type="text" class="form-control txt-input" id="userLogin" name="userLogin" maxlength="20" required placeholder="Required">
        				</div>
        			</div>
        			<div class="row horizontalRadioDiv">
        				<label for="receiveBy" class="col-md-5 col-form-label col-sm-5 radioboxlbl">Received password By<small>Your registered Email or Phone number</small></label>
        				<div class="col-md-5 col-sm-6 margintop">
        					<div class="form-radio radio">
        						<input class="form-check-input radio__input" type="radio" name="receiveBy" id="receiveBy1" value="Email" checked>
        						<label class="form-check-label radio__label" for="receiveBy1">Email</label>
        					</div>
        					<div class="form-radio radio">
        						<input class="form-check-input radio__input" type="radio" name="receiveBy" id="receiveBy2" value="Sms">
        						<label class="form-check-label radio__label" for="receiveBy2">SMS</label>
        					</div>
        				</div>
        			</div>
        			<div class="row">
    					<div class="col-md-10 margintop col-sm-11">
    						<button type="submit" class="btn btn-primary confirm-btn getPassBtn pull-right">Confirm</button>
    						<a class="btn btn-primary pull-right" href="<?php echo get_permalink(65);?>">Back to LOGIN</a>
    						<input type=hidden name="task" value="sendPass">
    					</div>
        			</div>
        		</form>	
        		<?php } ?>
        		 <?php if($haveUser && !$sendPass) {?>
        		 	<?php if ($result->status == "Discontinued") { ?>
        		 		<div class="row">
            				<div class="col-md-12">
        						<p class="text-warning">You are not allowed to login. Please contact CCCL office.</p>
        					</div>
    					</div>
					<?php } else {?>
            		 <form id="formSendPass" method="post">
            			<div class="row horizontalRadioDiv">
            				<label for="receiveBy" class="col-md-5 col-form-label radioboxlbl col-sm-5">Received password By<small>Your registered Email or Phone number</small></label>
            				<div class="col-md-5 margintop col-sm-5">
            					<div class="form-radio radio">
            						<input class="form-check-input radio__input" type="radio" name="receiveBy" id="receiveBy1" value="Email" checked>
            						<label class="form-check-label radio__label" for="receiveBy1">Email</label>
            					</div>
            					<div class="form-radio radio">
            						<input class="form-check-inpu radio__input" type="radio" name="receiveBy" id="receiveBy2" value="Sms">
            						<label class="form-check-label radio__label" for="receiveBy2">SMS</label>
            					</div>
            				</div>
            			</div>
            			<div class="row">
        					<div class="col-md-8 col-md-offset-1 col-sm-9 col-sm-offset-1">
        						<button type="submit" class="btn btn-primary confirm-btn getPassBtn pull-right">Confirm</button>
        						<a class="btn btn-primary pull-right" href="<?php echo home_url(); ?>">Back to LOGIN</a>
        						<input type="hidden" value="<?php echo $result->user_login?>" name="userLogin">
        						<input type="hidden" value="<?php echo $result->country_phone_code.$result->mobile_number;?>" name="phone">
        						<input type=hidden name="task" value="sendPass">
        					</div>
            			</div>
            		 </form>
        		 	<?php } ?>
        		<?php }?>	
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>