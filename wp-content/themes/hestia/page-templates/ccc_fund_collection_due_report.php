<?php /* Template Name: ccc_fund_collection_due_report */ ?>


<?php get_header(); 
add_filter('show_admin_bar', '__return_false');
do_action( 'hestia_before_single_page_wrapper' );
$user = wp_get_current_user();

global $post;
$post_slug = $post->post_name;
$list = get_products_by_category('Payment', true);
?>
<div class="<?php echo hestia_layout(); ?>">
    <div class="container add_collection_report">
        <form id="membershipReportForm" method="post" autocomplete="off">
    		<div class="row search_form">
				<div class=" col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
					<div class="card card-form">
						<div class="row-fluid">
							<label for="slctCat" class="col-md-3 col-form-label">Select Product</label>
							<div class="col-md-9">
								<select class="form-control inputField slctCat" id="slctProd" name="slctProd" required="required">
									<option value="">Requird</option>
									<?php foreach ($list as $value) {?>
									<option value="<?php echo $value->product_id?>" <?php echo $_POST["slctProd"] == $value->product_id ? 'Selected' : ''?>><?php echo $value->name?></option>
									<?php }?>
								</select>
							</div>
						</div>
							<div class="row-fluid selectProdDiv">
								<label for="slctProd" class="col-md-3 col-form-label">Membership ID</label>
								<div class="col-md-9">
									<input type="text" class=" form-control " name="srcByMbrId" id="srcByMbrId" value="<?php echo $_POST["srcByMbrId"]?>" placeholder="Last five digits of Member ID">
								</div>
							</div>
							<div class="row-fluid">
								<div class="col-md-12 text-right">
									<button type="submit" class="btn btn-primary mb-2 collectionReportBtn">Search</button>
								</div>
							</div>
					</div>	
				</div>
			</div>	
		</form>
    	<div class="row get-pass-sec">
        	<div class="col-md-12">
             	<h2 class="h2">Due Collection Status</h2>
             	<hr>
         	</div>
        </div>
    	<div class="row">
    		<div class="col-md-12">
    		<table id="membershipReportTable" class="table table-striped datatable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Due amount</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>Phone</th>
                        <th>Email</th>
                    </tr>
                </thead>
                <tbody>
                	<?php 
                	$pid = $_POST['slctProd'];
                	if (!empty($pid)) {
                     $results = get_membership_report_on_Payment();	
                     $product = get_product_details_by_pid ($pid);
                        if (count($results)>0) {
                        	foreach ($results as $value) {
								//if ($dueamount < 1) continue;
								if (!checkMemberStatus($value)) {
									$dueamount = 0;
								} else {
									$dueamount = $product->payment_amount - get_due_development_fee ($pid, $value->user_login);
								}
                     ?>
                        <tr>
                            <td><?php echo $value->user_login;?></td>
                            <td><?php echo $value->display_name;?></td>
                            <td><?php echo number_format($dueamount,2); ?></td>
                            <td><?php echo $value->member_type;?></td>
                            <td><?php echo $value->status;?></td>
                            <td><?php echo $value->mobile_number;?></td>
                            <td><?php echo $value->user_email;?></td>
                        </tr>
                    <?php }
                        }
                	}
                       	?>
                </tbody>
            </table> 
            </div>
    	</div>
    </div>
</div>
<?php get_footer(); ?>

<script>
jQuery(document).ready(function($) {
	var membershipReportTable = $('#membershipReportTable').DataTable({
		<?php 
		$user = wp_get_current_user();
		if ( $user->roles{0}  == 'contributor' || $user->roles{0}  == 'author' || $user->roles{0}  == 'administrator') {
		?>
		dom: "<'row'<'col-md-3 col-xs-4'l><'col-md-8 col-xs-5'><'col-md-1 col-xs-3'B>>" +
    "<'row'<'col-md-12'tr>>" +
    "<'row'<'col-sm-6 col-xs-12'i><'col-sm-6 col-xs-12'p>>",
        buttons: [
			{ extend: 'csv',className: "btn btn-primary", text: '<i class="fas fa-download"></i>' }
        ],
        <?php } ?>
		rowReorder: {
			selector: 'td:nth-child(3)'
		},		
		responsive: true,
		order : [ [ 0, 'asc' ]],
		"columnDefs" : [  {
			"targets" : 'no-sort',
			"orderable" : false
		},
		 {
	        targets: 2,
	        className: 'text-right'
	    },
	    {
	        targets: [3,4,5],
	        className: 'text-nowrap'
	    }],
	    "pageLength": 50
	});
	
});
</script>