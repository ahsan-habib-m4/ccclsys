<?php /* Template Name: cccl_directory_report */ ?>



<?php get_header(); 
add_filter('show_admin_bar', '__return_false');
do_action( 'hestia_before_single_page_wrapper' );
$user = wp_get_current_user();
?>
<div class="loading">
	<div class='uil-ring-css' style="transform:scale(0.79);">
		<div></div>
	</div>
</div>
<div class="<?php echo hestia_layout(); directoryReportMainCont ?>">
	
    <div class="container directory_report">
    	<form id="reportForm" method="post" autocomplete="off">
            <div class="row">
            	<div class="col-md-12 col-xs-12 col-sm-12">
                     <div class="card card-form membershipReportFormDiv">
    					<div class="content">
    						<div class="row">
    							<div class="col-md-4">
    								<div class="row">
    									<div class="col-md-5">
            								<label for="mmbrId" class="col-form-label">ID</label>
            							</div>
            							<div class="col-md-7">
            								<input type="text" class="form-control inputField" id="mmbrId" name="mmbrId" value="<?php echo $_POST['mmbrId'] ?>" >
            							</div>
    								</div>
    							</div>
    							<div class="col-md-4">
    								<div class="row">
    									<div class="col-md-5">
            								<label for="fullname" class="col-form-label">Full Name</label>
            							</div>
            							<div class="col-md-7">
            								<input type="text" class="form-control inputField" id="fullname" name="fullname" value="<?php echo $_POST['fullname'] ?>" >
            							</div>
    								</div>
    							</div>
    							<div class="col-md-4">
    								<div class="row">
    									<div class="col-md-5">
            								<label for="cadetname" class="col-form-label">Cadet Name</label>
            							</div>
            							<div class="col-md-7">
            								<input type="text" class="form-control inputField" id="cadetname" name="cadetname" value="<?php echo $_POST['cadetname'] ?>">
            							</div>
    								</div>
    							</div>
    						</div>
    						<div class="row">
    							<div class="col-md-4">
    								<div class="row">
    									<div class="col-md-5">
            								<label for="slctMbrType" class="col-form-label">Type</label>
            							</div>
            							<div class="col-md-7">
            								<select class="form-control inputField" id="slctMbrType" name="slctMbrType">
            									<option value="">Select</option>
            									<?php
            									$member_type_list = get_member_type_list();
            									foreach ($member_type_list as $value) {
            									    $selected = $_POST['slctMbrType'] == $value->code ? "Selected" : "" ;
            									?>
            									<option value="<?php echo $value->code?>"  <?php echo $selected;?>><?php echo $value->member_type?></option>	
            									<?php 
            									}
            									?>
                                        	</select>
            							</div>
    								</div>
    							</div>
    							<div class="col-md-4">
    								<div class="row">
    									<div class="col-md-5">
            								<label for="slctMbrStts" class="col-form-label">Status</label>
            							</div>
            							<div class="col-md-7">
            								<select class="form-control inputField" id="slctMbrStts" name="slctMbrStts">
                                                <option value="">Select</option>
                                                <option value="Active" <?php echo $_POST['slctMbrStts'] == "Active" ? "Selected" : "";?> >Active</option>
                                                <option value="Abroad" <?php echo $_POST['slctMbrStts'] == "Abroad" ? "Selected" : "";?> >Abroad</option>
                                                <option value="Discontinued" <?php echo $_POST['slctMbrStts'] == "Discontinued" ? "Selected" : "";?> >Discontinued</option>
                                                <option value="Suspended" <?php echo $_POST['slctMbrStts'] == "Suspended" ? "Selected" : "";?> >Suspended</option>
                                            </select>
            							</div>
    								</div>
    							</div>
    							<div class="col-md-4">
    								<div class="row">
    									<div class="col-md-5">
            								<label for="txtPhone" class="col-form-label">Phone</label>
            							</div>
            							<div class="col-md-7">
            								<input type="text" class="form-control inputField phonenumber contactInfo" id="txtPhone" name="txtPhone" value="<?php echo $_POST['txtPhone'] ?>">
            							</div>
    								</div>
    							</div>
    						</div>
    						<div class="row">
    							<div class="col-md-4">
    								<div class="row">
    									<div class="col-md-5">
            								<label for="txtemail" class="col-form-label">Email</label>
            							</div>
            							<div class="col-md-7">
            								<input type="email" class="form-control inputField contactInfo" id="txtemail" name="txtemail" value="<?php echo $_POST['txtemail'] ?>">
            							</div>
    								</div>
    							</div>
    							<div class="col-md-4">
    								<div class="row">
    									<div class="col-md-5">
            								<label for="College" class="col-form-label">College</label>
            							</div>
            							<div class="col-md-7">
            								<select class="form-control inputField" id="college" name="college" >
            									<option value="">Select</option>
            									<?php $college_list = get_college_list();
            									foreach ($college_list as $value) {
            									    $selected = $_POST['college'] == $value->serial_no ? "Selected" : "" ;
            									?>
            									<option value="<?php echo $value->serial_no?>" <?php echo $selected;?>><?php echo $value->college_name?></option>	
            									<?php 
            									}
            									?>
            								</select>
            							</div>
    								</div>
    							</div>
    							<div class="col-md-4">
    								<div class="row">
    									<div class="col-md-5">
            								<label for="batch" class="col-form-label">Batch</label>
            							</div>
            							<div class="col-md-7">
            								<input type="text" class="form-control inputField" id="batch" name="batch" value="<?php echo $_POST['batch']; ?>">
            							</div>
    								</div>
    							</div>
    						</div>
    						<div class="row">
    							<div class="col-md-4">
    								<div class="row">
    									<div class="col-md-5">
            								<label for="bloodgrp" class="col-form-label">Blood Group</label>
            							</div>
            							<div class="col-md-7">
            								<select class="form-control inputField basicInfo" id="bloodgrp" name="bloodgrp">
                                                <option value="">Select</option>
                                                <option value="A+" <?php echo $_POST['bloodgrp'] == "A+" ? "Selected" : "";?>>A+</option>
                                                <option value="A-" <?php echo $_POST['bloodgrp'] == "A-" ? "Selected" : "";?>>A-</option>
                                                <option value="B+" <?php echo $_POST['bloodgrp'] == "B+" ? "Selected" : "";?>>B+ </option>
                                                <option value="B-" <?php echo $_POST['bloodgrp'] == "B-" ? "Selected" : "";?>>B-</option>
                                                <option value="O+" <?php echo $_POST['bloodgrp'] == "O+" ? "Selected" : "";?>>O+</option>
                                                <option value="O-" <?php echo $_POST['bloodgrp'] == "O-" ? "Selected" : "";?>>O-</option>
                                                <option value="AB+" <?php echo $_POST['bloodgrp'] == "AB+" ? "Selected" : "";?>>AB+</option>
                                                <option value="AB-" <?php echo $_POST['bloodgrp'] == "AB-" ? "Selected" : "";?>>AB-</option>
            								</select>
            							</div>
    								</div>
    							</div>
    							<div class="col-md-4">
    								<div class="row">
    									<div class="col-md-5">
            								<label for="profession" class="col-form-label">Profession</label>
            							</div>
            							<div class="col-md-7">
            								<select class="form-control inputField basicInfo" id="profession" name="profession">
            									<option value="">Select</option>
            									<?php
            									$profession_list = get_profession_list();
            									foreach ($profession_list as $value) {
            									    $selected = $_POST['profession'] == $value->id ? "Selected" : "" ;
            									?>
            									<option value="<?php echo $value->id;?>" <?php echo $selected;?>><?php echo $value->profession_name;?></option>
            									<?php 
            									}
            									?>
        									</select>
            							</div>
    								</div>
    							</div>
    							<div class="col-md-4">
    								<div class="row">
    									<div class="col-md-5">
            								<label for="organization" class="col-form-label">Organization</label>
            							</div>
            							<div class="col-md-7">
            								<input type="text" class="form-control inputField basicInfo" id="organization" name="organization" value="<?php echo $_POST['organization']; ?>">
            							</div>
    								</div>
    							</div>
    						</div>
    						<div class="row">
    							<div class="col-md-4">
    								<div class="row">
    									<div class="col-md-5">
            								<label for="designation" class="col-form-label">Designation</label>
            							</div>
            							<div class="col-md-7">
            								<input type="text" class="form-control inputField basicInfo" id="designation" name="designation" value="<?php echo $_POST['designation']; ?>">
            							</div>
    								</div>
    							</div>
    							<div class="col-md-4">
    								<div class="row">
    									<div class="col-md-5">
            								<label for="specialisation" class="col-form-label">Specialisation</label>
            							</div>
            							<div class="col-md-7">
            								<input type="text" class="form-control inputField basicInfo" id="specialisation" name="specialisation" value="<?php echo $_POST['specialisation']; ?>">
            							</div>
    								</div>
    							</div>
    							<div class="col-md-4 searchFormDob">
    								<div class="row">
    									<div class="col-md-5">
            								<label for="dateOfBirth" class=" col-form-label">DoB</label>
            							</div>
            							<div class="col-md-7">
            								<input type="text" class=" inputField dateFields basicInfo" id="dateOfBirth" name="dateOfBirth" aria-describedby="btnGroupAddon" value="<?php echo $_POST['dateOfBirth']; ?>" >
                                            <div class="input-group-append btnGroupAddon datepickerIcon">
                                              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
            							</div>
    								</div>
    							</div>
    						</div>
    						<div class="row">
    							<div class="col-md-4 searchFormDob">
    								<div class="row">
    									<div class="col-md-5">
            								<label for="profession" class="col-form-label">DoA</label>
            							</div>
            							<div class="col-md-7">
            								<input type="text" class="inputField dateFields familyInfo" id="anniversary" name="anniversary" aria-describedby="btnGroupAddon" value="<?php echo $_POST['anniversary']; ?>">
            								<div class="input-group-append btnGroupAddon datepickerIcon">
            									<div class="input-group-text"><i class="fa fa-calendar"></i></div>
            								</div>
            							</div>
    								</div>
    							</div>
    							<div class="col-md-4 ">
    								<div class="row">
    									<div class="col-md-5">
            								<label for="postalAddrs" class="col-form-label">Postal Address</label>
            							</div>
            							<div class="col-md-7">
            								<select class="form-control inputField contactInfo" id="postalAddrs" name="postalAddrs">
                                                <option value="">Select</option>
                                                <option value="Home" <?php echo $_POST['postalAddrs'] == "Home" ? "Selected" : "";?>>Home Address</option>
                                                <option value="Office" <?php echo $_POST['postalAddrs'] == "Office" ? "Selected" : "";?>>Office Address</option>
                                            </select>
            							</div>
    								</div>
    							</div>
    							<div class="col-md-4">
    								<div class="row">
    									<div class="col-md-5">
            								<label for="homeAddr" class=" col-form-label">HSC year</label>
            							</div>
            							<div class="col-md-7">
            								<select class="form-control inputField basicInfo" id="hscyear" name="hscyear">
            								<option value="">Select</option>
            								<?php 
                                              for($i=1940; $i<=date("Y"); $i++) {
                                                  $selected = $_POST['hscyear'] == $i ? "Selected" : "" ;
                                              	?>
                                             <option value="<?php echo $i?>" <?php echo $selected; ?>><?php echo $i;?></option>";
                                              <?php }
                                              ?> 
            								</select>
            							</div>
    								</div>
    							</div>
    						</div>
    						<div class="row">
    							<div class="col-md-4">
    								<div class="row">
    									<div class="col-md-5">
            								<label for="homeAddr" class=" col-form-label">Home Address</label>
            							</div>
            							<div class="col-md-7">
            								<textarea class="form-control inputField basicInfo" id="homeAddr" name="homeAddr"  rows="2" ><?php echo $_POST['homeAddr'];?></textarea>
            							</div>
    								</div>
    							</div>
								<div class="col-md-4">
    								<div class="row">
    									<div class="col-md-5">
            								<label for="officeAddr" class=" col-form-label">Office Address</label>
            							</div>
            							<div class="col-md-7">
            								<textarea class="form-control inputField basicInfo" id="officeAddr" name="officeAddr" rows="2"><?php echo $_POST['officeAddr'];?></textarea>
            							</div>
    								</div>
    							</div>
    						</div>
    						<div class="row margintop">
    							<div class="col-md-12 text-right">
    								<button type="submit" class="btn btn-primary defaultMemberReportBtn">Search</button>
    							</div>
    						</div>
    					</div>
    				</div>
				</div>
            </div>
    	</form>
    	
    	<div class="row get-pass-sec">
        	<div class="col-md-12">
             	<h2 class="h2">Member List</h2>
             	<hr>
         	</div>
        </div>
        <?php 
        $user = wp_get_current_user();
        $roles = ( array ) $user->roles;
        ?>
    	<div class="row">
    		<div class="col-md-12">
    		<div class="processing-div text-center">
    			<p>Data Loading ..</p>
    		</div>
    		<table id="membershipReportTable" class="table table-striped datatable" style="display: none;">
                <thead>
                    <tr>
                    	<?php if ($roles[0] == "subscriber") { ?>
                        	<th>Photo</th>
                            <th>ID</th>
                            <th>Cadet Name</th>
                            <th>Name</th>
                            <th>College</th>
                            <th>HSC Year</th>
                            <th>Blood Group</th>
                            <th>Profession</th>
                            <th>Organization</th>
                            <th>Designation</th>
                            <th>Specialization</th>
                            <th>DoB</th>
                            <th>DoA</th>
                            <th>Phone</th>
                            <th>Email</th>
                             <th class="no-sort"></th>
                         <?php } else {?>
                         	<th>Photo</th>
                            <th>ID</th>
                            <th>Type</th>
                            <th>Status</th>
                            <th>Name</th>
                            <th>Cadet Name</th>
                            <th>College</th>
                            <th>HSC Year</th>
                            <th>Blood Group</th>
                            <th>DoB</th>
                            <th>Profession</th>
                            <th>Organization</th>
                            <th>Designation</th>
                            <th>Specialization</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>DoA</th>
                            <th>Full ID</th>
                            <th>Postal Address</th>
                            <th class="no-sort"></th>
                         <?php }?>
                    </tr>
                </thead>
                <tbody>
                     <?php 
                     $results = get_member_list_by_all();
                        if(count($results)>0) {
                        	foreach ($results as $value) {?>
                        	<?php if ($roles[0] == "subscriber") { ?>
                            <tr>
                            	<td>
                                	<?php if ($value->image != "") { ?>
                                		<img data-src="<?php echo get_template_directory_uri() . '/assets/img/member_image/' . $value->image; ?>" alt="profile image" class="rounded profileImage tableImage lazy">
                                	<?php } else { ?>
                                		<img data-src="<?php echo get_template_directory_uri() . '/assets/img/avatar.jpg' ?>" alt="profile image" class="rounded profileImage tableImage lazy">
                                	<?php } ?>
                                </td>
                                <td>
                                	<?php 
                                        $fullid = explode("-",$value->member_full_id);
                                        echo $fullid[4];
                                    ?>
                                </td>
                                <td><?php echo $value->cadet_name;?></td>
                                <td><?php echo $value->display_name;?></td>
                                <td><?php echo $value->college_sort_code;?></td>
                                <td><?php echo $value->hsc_year;?></td>
                                <td><?php echo $value->blood_group;?></td>
                                <td><?php echo $value->profession_name;?></td>
                                <td><?php echo $value->organization;?></td>
                                <td><?php echo $value->designation;?></td>
                                <td><?php echo $value->specialization;?></td>
                                <td><?php if($value->date_of_birth != "" && $value->date_of_birth != "0000-00-00") echo date("d-M-Y", strtotime($value->date_of_birth));?></td>
                                <td>
                                	<?php if($value->anniversary != "" && $value->anniversary != "0000-00-00") echo date("d F", strtotime($value->anniversary)); ?>
                                </td>
                                <td><?php echo $value->mobile_number;?></td>
                                <td><?php echo $value->user_email;?></td>
                                <td dataid="<?php echo $fullid[4]; ?>"><a class="btn btn-primary pull-left"  href="<?php echo get_permalink(244); ?>?srcByMbrId=<?php echo $fullid[4]; ?>&task=viewmemberprofile">View Profile</a></td>
                            </tr>
                            <?php } else { ?>
                            <tr>
                            	<td>
                                	<?php if ($value->image != "") { ?>
                                		<img data-src="<?php echo get_template_directory_uri() . '/assets/img/member_image/' . $value->image; ?>" alt="profile image" class="rounded profileImage tableImage lazy">
                                	<?php } else { ?>
                                		<img data-src="<?php echo get_template_directory_uri() . '/assets/img/avatar.jpg' ?>" alt="profile image" class="rounded profileImage tableImage lazy">
                                	<?php } ?>
                                </td>
                                <td>
                                	<?php 
                                        $fullid = explode("-",$value->member_full_id);
                                        echo $fullid[4];
                                    ?>
                                </td>
                                <td><?php echo $value->member_type;?></td>
                                <td><?php echo $value->status;?></td>
                                <td><?php echo $value->display_name;?></td>
                                <td><?php echo $value->cadet_name;?></td>
                                <td><?php echo $value->college_sort_code;?></td>
                                <td><?php echo $value->hsc_year;?></td>
                                <td><?php echo $value->blood_group;?></td>
                                <td class="text-nowrap"><?php if($value->date_of_birth != "" && $value->date_of_birth != "0000-00-00") echo date("d-M-Y", strtotime($value->date_of_birth));?></td>
                                <td><?php echo $value->profession_name;?></td>
                                <td><?php echo $value->organization;?></td>
                                <td><?php echo $value->designation;?></td>
                                <td><?php echo $value->specialization;?></td>
                                <td><?php echo $value->mobile_number;?></td>
                                <td><?php echo $value->user_email;?></td>
                                <td><?php if($value->anniversary != "" && $value->anniversary != "0000-00-00") echo date("d-M-Y", strtotime($value->anniversary)); ?></td>
                                <td><?php echo $value->member_full_id;?></td>
                                <td>
								<?php 
                                	    if ($value->postal_address == "Home"){
                                	       echo $value->home_address;
                                    	} else {
                                    	    echo $value->office_address;
                                    	}
                                	?>
								</td>
                                <td dataid="<?php echo $fullid[4]; ?>"><a class="btn btn-primary pull-left"  href="<?php echo get_permalink(244); ?>?srcByMbrId=<?php echo $fullid[4]; ?>&task=viewmemberprofile">View Profile</a></td>
                            </tr>
                            
                            <?php }?>
                       	<?php }
                        }
                       	?>
                </tbody>
            </table> 
            </div>
    	</div>
    </div>
</div>
<?php get_footer(); ?>

<script>
jQuery(document).ready(function($) {
	$('.lazy').Lazy({
        scrollDirection: 'vertical',
        effect: 'fadeIn',
        visibleOnly: true,
        onError: function(element) {
            console.log('error loading ' + element.data('src'));
        }
    });
	var membershipReportTable = $('#membershipReportTable').DataTable({
		<?php 
		$user = wp_get_current_user();
		if ( $user->roles{0}  == 'contributor' || $user->roles{0}  == 'author' || $user->roles{0}  == 'administrator') {
		?>
		dom: "<'row'<'col-md-3 col-xs-4'l><'col-md-8 col-xs-5'f><'col-md-1 col-xs-3'B>>" +
    		"<'row'<'col-md-12'tr>>" +
			"<'row'<'col-sm-6 col-xs-12'i><'col-sm-6 col-xs-12'p>>",
        buttons: [
			{ extend: 'csv',className: "btn btn-primary", text: '<i class="fas fa-download"></i>' }
        ],
        <?php } ?>
		rowReorder: {
			selector: 'td:nth-child(3)'
		},		
		responsive: true,
		order : [ [ 1, 'asc' ]],
		"columnDefs" : [  {
			"targets" : 'no-sort',
			"orderable" : false
		},{
	        targets: 5,
	        className: 'text-nowrap'
	    } ],
		"pageLength": 50,
		"initComplete": function() {
		   jQuery(".loading, .processing-div").hide();
		   $("#membershipReportTable").show();
		  }
	});
	membershipReportTable.rowReorder.disable();
	if ( membershipReportTable.responsive.hasHidden() ) {
		$('#membershipReportTable tbody').on( 'click', 'a.viewprofilebtn', function (event) {
            event.preventDefault();
			var data = membershipReportTable.row( $(this).parents('tr').prev()[0] ).data();
			var origTr = $(this).parents('tr').prev()[0];
			var dataid = $(origTr).children(":last").attr("dataid");
		});	
	}
	else {
		$('#membershipReportTable tbody').on( 'click', 'a.viewprofilebtn', function (event) {
            event.preventDefault();
			var data = membershipReportTable.row( $(this).parents('tr') ).data();
			var dataid = $(this).parents("td").attr("dataid");
		});	
	}
	
});
</script>