<?php /* Template Name: cccl_create_edit_product */ ?>
<?php 
global $post;
$post_slug = $post->post_name;
$isEvent = false;
$isVenue = false;
$isService = false;
$isPayment = false;
$page_id = $current_page_id;
if ($post_slug == "create-venue") {
	$isVenue = true;
	$category = constant("VENUE");
} else if ( $post_slug == "create-event" ) {
	$isEvent = true;
	$category = constant("EVENT");
} else if ( $post_slug == "create-service" ) {
	$isService = true;
	$category = constant("SERVICE");
} else if ( $post_slug == "create-payment" ) {
	$isPayment = true;
	$category = constant("PAYMENT");
}
$product = prepare_product_list();
$task = $_POST['task'];
$items = array();
$tokens = array();
$current_date = $_POST["setAvailability"] ? custom_date("Y-m-d",$_POST["setAvailability"]) : date('Y-m-d');
if ($task == "add" && !empty($product)) {
	$error = create_product($product);
	if (empty($error)) {
		wp_redirect( get_permalink($page_id)."?pid=".$product['product_id']);
	}
} elseif ($task == "update" && !empty($product)){
	$error = update_product($product);
	if (empty($error)) {
		$product_details = get_product_details_by_pid($product["product_id"]);
		if($isVenue){
			$items = get_items_availability ($current_date, $product["product_id"]);
		}else{
			$items = get_items_by_product_id ($product["product_id"]);
			$tokens = get_tokens_by_product_id ($product["product_id"]);
		}
		$pid = $product["product_id"];
	}
	
} elseif (!empty($_REQUEST['pid'])) {
	$pid = $_REQUEST['pid'];
	$product_details = get_product_details_by_pid($pid);
	if ($isVenue) {
		$items = get_items_availability ($current_date, $pid);
	} else {
		$items = get_items_by_product_id ($pid);
		$tokens = get_tokens_by_product_id ($pid);
	}
} else {
	if ($isVenue) {
		$items = get_items_availability ($current_date, $pid);
	} else {
		$items[]["tmp"] = 0;
	}
	
}
if (!empty($error) || !empty($product_details)) {
	$product_id = $error ? $product["product_id"] : $product_details->product_id;
	$product_name = $error ? $product["name"] : $product_details->name;
	$start_date = $error ? $product["start_date"] : custom_date("d-M-Y", $product_details->start_date);
	$end_date = $error ? $product["end_date"] : custom_date("d-M-Y", $product_details->end_date);
	$location = $error ? $product["location"] : $product_details->location;
	$description = $error ? $product["description"] : $product_details->description;
	$vat = $error ? $product["vat"] : $product_details->vat;
	$service_charge = $error ? $product["serviceCharge"] : $product_details->service_charge;
	$promo_code = $error ? $product["promo_code"] : $product_details->promo_code;
	$event_date = $error ? $product["event_date"] : custom_date("d-M-Y", $product_details->event_date);
	$promo_valid_till = $error ? $product["promo_valid_till"] : custom_date("d-M-Y", $product_details->promo_valid_till);
	$discount = $error ? $product["discount"] : $product_details->discount;
	$payment_option = $error ? $product["payment_option"] : $product_details->payment_option;
	$payment_amount = $error ? $product["payment_amount"] : $product_details->payment_amount;
	$venue_morning_price = $error ? $product["venue_morning_price"] : $product_details->venue_morning_price;
	$venue_afternoon_price = $error ? $product["venue_afternoon_price"] : $product_details->venue_afternoon_price;
	$venue_evening_price = $error ? $product["venue_evening_price"] : $product_details->venue_evening_price;
	$emi_option = $error ? $product["emi_option"] : $product_details->emi_option;
}
get_header(); 
add_filter('show_admin_bar', '__return_false');
do_action( 'hestia_before_single_page_wrapper' );

?>
<div class="<?php echo hestia_layout(); ?>">
	<div class="container addEditProducts">
		<div class="row">
		<div>&nbsp;</div>
		<?php 
		if (!empty($error)) {
			echo show_custom_message($error, "warning");
		} elseif($task == "add" ||  $_GET['pid']) {
			echo show_custom_message("Product information saved successfully", "success");
		} elseif ($task == "update") {
			echo show_custom_message("Product information update successfully", "success");
		}	
		?>
		</div>
		<form method="post" action="<?php echo get_permalink($page_id);?>" id="srcByProductIdForm" autocomplete="off" class="margin-top">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 sectionDiv">
					<div class="card card-form-horizontal searchMemberInEdit">
						<div class="content">
							<div class="row">
								<div class="textwidget">
									<div class="col-md-9 col-sm-12 col-xs-12">
										<div class="input-group">
											<label for="srcByProductId" class="col-md-4 col-sm-12 col-xs-12 col-form-label text-right text-sm-left">Select</label>
											<div class="form-group is-empty col-md-8 col-sm-12 col-xs-12">
											<?php $product_list = get_product_list_by_category($category);
											?>
												<select class="form-control inputField" id="srcByProductId" name="pid">
												<option value=""></option>
												<?php foreach ($product_list as $product_val) {
													$selected = $pid == $product_val->product_id ? "Selected" : "" ;
												?>	
												<option value="<?php echo $product_val->product_id;?>"  <?php echo $selected?>><?php echo $product_val->name;?></option>
												<?php 	
												}?>
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-3 col-sm-12 col-xs-12">
										<button type="submit" class="btn btn-primary btn-block sib-default-btn srcByProductIdBtn" id="srcByProductIdBtn">Search</button>
										<input type="hidden" name="task" value="srcByProduct"> 
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
		
		<form method="post" action="<?php echo get_permalink($page_id);?>" id="addEditProductform" autocomplete="off">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1  col-xs-10 col-xs-offset-1 sectionDiv">
					<div class="row">
						<label for="productname" class="col-md-3 col-form-label text-right">Name</label>
						<div class="col-md-8">
							<input type="text" class="form-control inputField" id="productname" name="productname" placeholder="Required" value="<?php echo $product_name; ?>" required placeholder="Required" autocomplete="off">
						</div>
					</div>
					<div class="row">
						<label for="productId" class="col-md-3 col-form-label text-right">ID</label>
						<div class="col-md-8">
							<input type="text" class="form-control inputField" id="productId" name="productId" placeholder="Required" value="<?php echo $product_id;?>" required placeholder="Required" <?php echo !empty($pid) ? "readonly='readonly'" : ""?>>
						</div>
					</div>
					<div class="row">
						<label for="startDate" class="col-md-3 col-form-label text-right">Sales Start</label>
						<div class="col-md-8">
							<input type="text" class="inputField edateFields startDate" id="startDate" name="startDate"  value="<?php echo $start_date;?>" Required placeholder="Required" aria-label="Input group example" aria-describedby="btnGroupAddon">
							<div class="input-group-append btnGroupAddon datepickerIcon">
								<div class="input-group-text"><i class="fa fa-calendar"></i></div>
							</div>
						</div>
					</div>
					
					<div class="row">
						<label for="endDate" class="col-md-3 col-form-label text-right">Sales End</label>
						<div class="col-md-8">
							<input type="text" class="inputField edateFields endDate" id="endDate" name="endDate" value="<?php echo $end_date;?>" Required placeholder="Required" aria-label="Input group example" aria-describedby="btnGroupAddon">
							<div class="input-group-append btnGroupAddon datepickerIcon">
								<div class="input-group-text"><i class="fa fa-calendar"></i></div>
							</div>
						</div>
					</div>
					
					<?php if (!$isPayment && !$isVenue) { ?>
						<div class="row">
							<label for="location" class="col-md-3 col-form-label text-right">Location</label>
							<div class="col-md-8">
								<input type="text" class="form-control inputField" id="location" name="location" value="<?php echo $location;?>" required placeholder="Required">
							</div>
						</div>
						<?php } ?>
						<div class="row">
							<label for="description" class="col-md-3 col-form-label text-right">Description</label>
							<div class="col-md-8">
								<textarea class="form-control inputField " id="description" name="description" rows="2" required placeholder="Required" autocomplete="off"><?php echo $description;?></textarea>
							</div>
						</div>
					<?php if ($isEvent) { ?>
						<div class="row">
							<label for="eventDate" class="col-md-3 col-form-label text-right">Event Date</label>
							<div class="col-md-8">
								<input type="text" class="inputField edateFields" id="eventDate" name="eventDate" value="<?php echo $event_date;?>" Required placeholder="Required" aria-label="Input group example" aria-describedby="btnGroupAddon">
								<div class="input-group-append btnGroupAddon datepickerIcon">
									<div class="input-group-text"><i class="fa fa-calendar"></i></div>
								</div>
							</div>			
						</div>
						<div class="row">
							<label for="prommoCode" class="col-md-3 col-form-label text-right">Promo Code</label>
							<div class="col-md-8">
								<input type="text" class="form-control inputField" id="prommoCode" name="prommoCode" value="<?php echo $promo_code;?>">
							</div>			
						</div>
						<div class="row">
							<label for="promoValidTill" class="col-md-3 col-form-label text-right">Valid Till</label>
							<div class="col-md-8">
								<input type="text" class="inputField edateFields " id="promoValidTill" name="promoValidTill" value="<?php echo $promo_valid_till;?>" aria-label="Input group example" aria-describedby="btnGroupAddon">
								<div class="input-group-append btnGroupAddon datepickerIcon">
									<div class="input-group-text"><i class="fa fa-calendar"></i></div>
								</div>
							</div>
						</div>
						<div class="row">
							<label for="discount" class="col-md-3 col-form-label text-right">Discount (%)</label>
							<div class="col-md-8">
								<input type="text" class="form-control inputField" id="discount" name="discount" value="<?php echo $discount?>" max="100">
							</div>
						</div>
						
						<div class="row">
							<label for="items" class="col-md-3 col-form-label text-right">Items</label>
							<div class="col-md-8">
								<table id="itemsTable" class="table table-striped">
									<colgroup>
										<col width="35%">
										<col width="20%">
										<col width="20%">
										<col width="30%">
										<col width="5%">
									</colgroup>
									<tr>
										<th>Type</th>
										<th>Person</th>
										<th>Price</th>
										<th>Max Quantity</th>
										<th></th>
									</tr>
									<?php 
									$i = 1;
									foreach ($items as $item) {
									?>
									<tr>
										<td><input type="text" class="form-control inputField passCatg" id="passCatg<?php echo $i?>" name="passCatg[]" value="<?php echo $item->type;?>" required  autocomplete="off"></td>
										<td><input type="text" class="form-control inputField person" id="person<?php echo $i?>" name="person[]" value="<?php echo $item->person;?>" required autocomplete="off"></td>
										<td><input type="text" class="form-control inputField passPrice" id="passPrice<?php echo $i?>" name="passPrice[]" value="<?php echo $item->price;?>" required autocomplete="off"></td>
										<td><input type="text" class="form-control inputField quantiy" id="quantiy<?php echo $i?>" name="quantiy[]" value="<?php echo $item->max;?>" required autocomplete="off"></td>
										<td></td>
									</tr>
									<?php 
									$i++;
									}?>
								</table>
								<a href="#" class="addMoreBtn pull-right">Add more <i class="fa fa-plus"></i></a>
							</div>
						</div>
						
						<div class="row">
							<label for="tokens" class="col-md-3 col-form-label text-right">Tokens</label>
							<div class="col-md-8 tokenInputContainer">
								<input type="text" class="form-control inputField tokens" id="tokens" name="tokens" value="<?php echo !empty($tokens) ? count($tokens): '';?>">
							</div>
						</div>
						<div class="row tokensFields">
						<?php 
						$t = 1;
						foreach ($tokens as $token) { ?>
							<label for="tokens" class="col-md-3 col-form-label text-right token token<?php echo $t;?>">Token <?php echo $t;?></label>
							<div class="col-md-8 tokenInputContainer token<?php echo $t;?>">
								<div class="form-group is-empty tokenInputsCont">
									<input type="text" class="form-control inputField tokenInputs" required id="tokens<?php echo $t?>" name="tokens[]" value="<?php echo $token->description;?>	"/>
								</div>
							</div>
						<?php 
						$t++;
						}?>
						</div>
					<?php } ?>
					
					<?php if ($isVenue) { ?>
						<div class="row">
							<label for="setAvailability" class="col-md-3 col-form-label text-right">Set Availability</label>
							<div class="col-md-8 tokenInputContainer">
								<input type="text" class="inputField edateFields " id="setAvailability" name="setAvailability" value="<?php echo $_POST["setAvailability"] ? $_POST["setAvailability"] : date("d-M-Y") ?>" required aria-label="Input group example" aria-describedby="btnGroupAddon">
								<input type="hidden" id="isChange" value=""/>
								<div class="input-group-append btnGroupAddon datepickerIcon">
									<div class="input-group-text"><i class="fa fa-calendar"></i></div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="row avilabilitySetupTableCont">
									<div class="col-md-8 col-md-offset-3">
										<table id="avilabilitySetupTable" class="avilabilitySetupTable table table-striped table-responsive">
										<?php create_vanue_table_html ($items);?>
										</table>
									</div>
								</div>
							</div>
						</div>
						
						<div class="row">
							<label for="morningShift" class="col-md-3 col-form-label text-right">Moring Price</label>
							<div class="col-md-8">
								<input type="text" class="form-control-plaintext morningShift" id="morningShift" name="venue_morning_price" value="<?php echo $venue_morning_price;?>" required placeholder="Required">
							</div>
						</div>
						<div class="row">
							<label for="afternoonShift" class="col-md-3 col-form-label text-right">Afternoon Price</label>
							<div class="col-md-8">
								<input type="text" class="form-control-plaintext afternoonShift" id="afternoonShift" name="venue_afternoon_price" value="<?php echo $venue_afternoon_price;?>"required placeholder="Required">
							</div>
						</div>
						<div class="row">
							<label for="eveningShift" class="col-md-3 col-form-label text-right">Evening Price</label>
							<div class="col-md-8">
								<input type="text" class="form-control-plaintext eveningShift" id="eveningShift" name="venue_evening_price" value="<?php echo $venue_evening_price;?>"required placeholder="Required">
							</div>
						</div>
					<?php } ?>
					
					<?php if ($isService) { ?>
						<div class="row">
							<label for="items" class="col-md-3 col-form-label text-right">Tickets</label>
							<div class="col-md-8">
								<table id="serviceItemsTable" class="table table-striped">
									<colgroup>
										<col width="40%">
										<col width="25%">
										<col width="5%">
									</colgroup>
									<tr>
										<th>Type</th>
										<th>Price</th>
										<th></th>
									</tr>
									<?php 
									$i = 1;
									foreach ($items as $item) {
									?>
									<tr>
										<td><input type="text" class="form-control inputField passCatg" id="passCatg<?php echo $i?>" name="passCatg[]" value="<?php echo $item->type;?>" required autocomplete="off"></td>
										<td><input type="text" class="form-control inputField passPrice" id="passPrice<?php echo $i?>" name="passPrice[]" value="<?php echo $item->price;?>" required autocomplete="off" ></td>
										<td></td>
									</tr>
									<?php 
									$i++;
									}?>
								</table>
								<a href="#" class="addMoreSerivceBtn pull-right">Add more <i class="fa fa-plus"></i></a>
							</div>
						</div>
					<?php } ?>
					
					<?php if (!$isEvent && !$isPayment) { ?>
						<div class="row">
							<label for="serviceCharge" class="col-md-3 col-form-label text-right">Service Charge (%)</label>
							<div class="col-md-8">
								<input type="text" class="form-control inputField serviceCharge" id="serviceCharge" name="serviceCharge" value="<?php echo $service_charge;?>" max="100" required placeholder="Required">
							</div>
						</div>
						<div class="row">
							<label for="vatamount" class="col-md-3 col-form-label text-right">VAT (%)</label>
							<div class="col-md-8">
								<input type="text" class="form-control inputField vatamount" id="vatamount" name="vat"  value="<?php echo $vat;?>" max="100" required placeholder="Required">
							</div>
						</div>
					<?php } ?>
					<?php if ($isPayment) { ?>
						<div class="row">
							<label for="paymentOption" class="col-md-3 col-form-label text-right">Payment Option</label>
							<div class="col-md-8">
								<select class="form-control inputField paymentOption" id="paymentOption" name="paymentOption" required>
									<option value="">Required</option>
									<option value="Fixed" <?php echo $payment_option == 'Fixed' ? 'Selected' : ''?>>Fixed Amount</option>
									<option value="Any" <?php echo $payment_option == 'Any' ? 'Selected' : ''?>>Any Amount</option>
								</select>
								<input type="text" class="form-control inputField paymentAmount" id="paymentAmount" name="paymentAmount" value="<?php echo $payment_amount;?>" required placeholder="Required">
								<?php 
								$j = 1;
								foreach ($items as $item) {
								?>
								<input type="hidden" class="passCatg" id="passCatg<?php echo $i?>" name="passCatg[]" value="<?php echo $item->type ? $item->type : "Donation";?>" required>
								<input type="hidden" class="passPrice" id="passPrice<?php echo $i?>" name="passPrice[]" value="<?php echo $item->price;?>" required >
								<?php 
								$j++;
								}?>
							</div>
							<label for="emiOption" class="col-md-3 col-form-label text-right">EMI Available</label>
							<div class="col-md-8">
								<select class="form-control inputField emiOption" id="emiOption" name="emi_option" required>
									<option value="">Required</option>
									<option value="0" <?php echo $emi_option == "0" ? 'Selected' : ''?>>No</option>
									<option value="1" <?php echo $emi_option == "1" ? 'Selected' : ''?>>Yes</option>
								</select>
							</div>
						</div>
					<?php } ?>
					<div class="row">
						<div class="col-md-11 text-right">
							<input type="hidden" name="category" value="<?php echo $category;?>">
							<button type="submit" class="btn btn-primary mb-2 addProductBtn" style="<?php echo $error ?  (($task == 'add') ? 'display: inline-block' : 'display: none') : (!empty($pid) ? 'display: none' : '');?>">Add</button>
							<button type="submit" class="btn btn-primary mb-2 updateProductBtn" style="<?php echo $error ? (($task =='update') ? 'display: inline-block' : 'display: none') : (!empty($pid) ? 'display: inline-block' : 'display: none');?>">Update</button>
							<input type="hidden" value="<?php echo $error ? $task : !empty($pid)? 'update' : 'add'?>" name="task" id="task">
							<button type="button" class="btn btn-secondary mb-2" id="clearAllProduct">Clear</button>
						</div>
					</div>
				</div>
			</div>
		</form>
		<?php echo create_loader();?>
	</div>
</div>
<?php get_footer(); ?>