<?php /* Template Name: cccl_add_subscription_fee */ ?>


<?php get_header(); 
add_filter('show_admin_bar', '__return_false');
?>

<?php 


global $post;
$post_slug = $post->post_name;
?>
<div class="container">
	<div class="row get-pass-sec">
    	<div class="col-12">
         	<p>Add Subscription fee</p>
         	<hr>
     	</div>
    </div>
	<form id="addSubscriptionfeeform">
		
       	<div class="form-group row">
            <label for="staticEmail" class="col-sm-2 col-form-label">Select Year</label>
            <div class="col-sm-10">
              <select class="form-control inputField" id="slctYear" name="slctYear" required>
                  <?php 
                  for($i=1940; $i<=2140; $i++)
                  {
                      echo "<option value=".$i.">".$i."</option>";
                  }
                  ?> 
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="staticEmail" class="col-sm-2 col-form-label">Select Quarter</label>
            <div class="col-sm-10">
              <select class="form-control inputField" id="slctQuarter" name="slctQuarter" required>
                  <option value="Q1">Q1</option>
                  <option value="Q2">Q2</option>
                  <option value="Q3">Q3</option>
                  <option value="Q4">Q4</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="staticEmail" class="col-sm-2 col-form-label">Subscription fee</label>
            <div class="col-sm-10">
              <input type="text" class="form-control inputField" id="sbcrpnfee" name="sbcrpnfee"  placeholder="" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="staticEmail" class="col-sm-2 col-form-label">Late fee (%)</label>
            <div class="col-sm-10">
              <input type="text" class="form-control inputField" id="latefee" name="latefee" placeholder="" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="staticEmail" class="col-sm-2 col-form-label">Abroad fee (%)</label>
            <div class="col-sm-10">
              <input type="text" class="form-control inputField" id="abrdfee" name="abrdfee" placeholder="" required>
            </div>
        </div>
         <div class="form-group row">
            <label for="staticEmail" class="col-sm-2 col-form-label"></label>
            <div class="col-sm-10">
              <button type="button" class="btn btn-primary mb-2 addSubFeeBtn">Submit</button>
            </div>
        </div>
        
	</form>
	
	
	<div class="row get-pass-sec">
    	<div class="col-12">
         	<p>All Subscription fee</p>
         	<hr>
     	</div>
    </div>
	<div class="row">
		<div class="col-12">
		<table id="subscriptionTable" class="table table-bordered datatable">
            <thead>
                <tr>
                    <th>Year</th>
                    <th>Quarter</th>
                    <th>Subscription fee</th>
                    <th>Late fee</th>
                    <th>Abroad fee</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>2020</td>
                    <td>Q1</td>
                    <td>2000</td>
                    <td>2</td>
                    <td>32</td>
                    <td><a href="#">Edit</a></td>
                </tr>
                <tr>
                    <td>2020</td>
                    <td>Q2</td>
                    <td>2000</td>
                    <td>3</td>
                    <td>35</td>
                    <td><a href="#">Edit</a></td>
                </tr>
            </tbody>
        </table> 
        </div>
	
	</div>
    
</div>
<?php get_footer(); ?>