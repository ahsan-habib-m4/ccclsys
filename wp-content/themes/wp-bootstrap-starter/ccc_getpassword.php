<?php /* Template Name: cccl_getpassword */ ?>


<?php get_header(); 
add_filter('show_admin_bar', '__return_false');
?>

<?php 
if ( current_user_can( 'subscriber' ) ){
	echo "Hi, dear Member! Glad seeing you again!";
}elseif (current_user_can( 'contributor' )){
	echo "Hi, dear Operator! Glad seeing you again!";
}
if (is_user_logged_in()) { ?>
<a class="login_button" href="<?php echo wp_logout_url( home_url() ); ?>">Logout</a>
<?php } else { 
    global $post;
    $post_slug = $post->post_name;
    ?>
    
	<div class="container getpassword-cont">
		<div class="row">
			<p class="ip-addr">"Your IP address is 112.164.218.24"</p>
		</div>
		<?php if ( $post_slug == "signup-get-password") { ?>
		<div class="row">
    		<form class="form-inline">
              <div class="form-group mx-sm-3 mb-2">
                <label for="userid" class="">Membership ID&#160;&#160;</label>
                <input type="text" class="form-control" id="userid" name="userid" required placeholder="last 5 digit">
              </div>
              <button type="submit" class="btn btn-primary mb-2">Find</button>
            </form>
        </div>
        <div class="row member-info">
        	<div class="mx-sm-3">
             	<p>Full ID: <span class="highlight">10-00357-12-001432</span></p>
             	<p>Name: <span class="highlight">Md Ahsan Habib</span></p>
             	<p>Phone: <span class="highlight">+8801913829232</span></p>
         	</div>
        </div>
        <?php } else { ?>
        	 <div class="row get-pass-sec">
            	<div class="col-12">
                 	<p>Get Password</p>
                 	<hr>
             	</div>
            </div>
            <div class="row user-name-input">
            	<div class="col-md-2">
                	<label for="userid" class="">User name </label>&#160;
                </div>
                <div class="col-md-9">
                	<input type="text" class="form-control txt-input" id="userid" name="userid" required placeholder="Username">
                </div>
            </div>
        <?php } ?>
        <div class="row received-by-cont">
        	<div class="form-check form-check-inline col-md-2 label">
              <label class="form-check-label">Received password By</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="receiveBy" id="receiveBy1" value="Email" checked>
              <label class="form-check-label" for="receiveBy1">Email</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="receiveBy" id="receiveBy2" value="Phone">
              <label class="form-check-label" for="receiveBy2">Phone</label>
            </div>
        </div>
        <div class="row">
        	<div class="col-12">
        		<input type="button" value="Confirm" class="btn btn-primary confirm-btn">
        	</div>
        </div>
    	<div class="row messages">
    		<div class="mx-sm-3">
    			<p class="text-success">Your password has been sent ! <a href="#">Login now</a></p>
    			<p class="text-danger">Password not sent successfully. Please try again.</p>
    		</div>
    	</div>
	</div>
<?php } ?>
<?php get_footer(); ?>