<?php /* Template Name: cccl_member_profile */ ?>


<?php get_header(); 
add_filter('show_admin_bar', '__return_false');


?>
<div id="primary" class="content-area cccl-login">
	<div class="entry-content">
    	<div class="row-fluid">
			<form id=profile enctype="multipart/form-data">
			    <div class="form-group">
			        <label><?php _e('Choose File:'); ?></label>
			        <input type="file" id="file" accept="image/*" />
			        <input class="submit_button btn btn-primary btn-upload" type="buttom" value="Add" name="submit">
			    </div>
			</form>
    	</div>
	</div>
</div>
<?php get_footer(); ?>