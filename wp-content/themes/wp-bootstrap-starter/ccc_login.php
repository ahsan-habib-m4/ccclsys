<?php /* Template Name: cccl_login */ ?>


<?php get_header(); 
add_filter('show_admin_bar', '__return_false');
?>

<div id="primary" class="content-area cccl-login">

<div class="entry-content">

<?php 
if ( current_user_can( 'subscriber' ) ){
	echo "Hi, dear Member! Glad seeing you again!";
}elseif (current_user_can( 'contributor' )){
	echo "Hi, dear Operator! Glad seeing you again!";
}
if (is_user_logged_in()) { ?>
<a class="login_button" href="<?php echo wp_logout_url( home_url() ); ?>">Logout</a>
<?php } else { 

    global $post;
    $post_slug = $post->post_name;
    ?>
	<div class="container">
		<div class="row-fluid">
			<p class="member-icon text-center">
				<i class="fas fa-user"></i>
			</p>
			<?php if ($post_slug == "login") {?>
                <h2 class="text-center">Member login</h2>
            <?php } else { ?>
                <h2 class="text-center">Manager/Operator login</h2>
            <?php }?>
			
		</div>
    	<div class="row-fluid">
    		<form id="login" action="login" method="post">
    			<fieldset>
	    	        <p class="status"></p>
	    			<div class="fontuser"> 
	    				<?php if ($post_slug == "login") {?>
	                        <input type="text" placeholder="Member ID" id="username"  name="username" class="form-control txt-input" required>
	                        <i class="fa fa-user fa-lg"></i> 
	                    <?php } else { ?>
	                        <input type="text" placeholder="Username" id="username"  name="username" class="form-control txt-input" required>
	                        <i class="fa fa-user fa-lg"></i> 
	                    <?php }?>
	                </div> 
	    	        <div class="fontpassword">
	                    <input type="password" placeholder="Password" name="password" id="password"  required class="form-control txt-input">  
	                    <i class="fa fa-key fa-lg"></i> 
	                </div>    
	    			<p>
	    		        <input class="submit_button btn btn-primary " type="submit" value="Login" name="submit">
	    		        <?php if ($post_slug == "login") {?>
	    		        	<a href="signup-get-password/" class="text-left">Sign up/ Get Password</a>
	    		        <?php } else {?>
	    		        	<a href="get-password-manager-and-operator/" class="text-left">Sign up/ Get Password</a>
	    		        <?php } ?>
	    		        <?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
	    		    </p>    
    	        </fieldset>
    		</form>
    	</div>	
	</div>
<?php } ?>
</div>
</div>
<?php get_footer(); ?>