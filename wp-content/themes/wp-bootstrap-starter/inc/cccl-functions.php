<?php
function ajax_login_init(){

	wp_register_script('ajax-login-script', get_template_directory_uri() . '/inc/assets/js/ajax-login-script.js', array('jquery') );
	wp_enqueue_script('ajax-login-script');

	wp_localize_script( 'ajax-login-script', 'ajax_login_object', array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'redirecturl' => home_url(),
			'loadingmessage' => __('Sending user info, please wait...')
	));

	// Enable the user with no privileges to run ajax_login() in AJAX
	if (!is_user_logged_in()) {
		add_action('wp_ajax_nopriv_ajaxlogin', 'ajax_login');
	}
}
add_action('init', 'ajax_login_init');
// Execute the action only if the user isn't logged in


function ajax_login(){

	// First check the nonce, if it fails the function will break
	check_ajax_referer( 'ajax-login-nonce', 'security' );

	// Nonce is checked, get the POST data and sign user on
	$info = array();
	$info['user_login'] = $_POST['username'];
	$info['user_password'] = $_POST['password'];
	$info['remember'] = true;

	$user_signon = wp_signon( $info, false );
	if ( is_wp_error($user_signon) ){
		echo json_encode(array('loggedin'=>false, 'message'=>__('Wrong username or password.')));
	} else {
		if ( $user_signon->roles{0}  == 'subscriber' ){
			$redirecturl = get_permalink(31);
		}elseif ($user_signon->roles{0}  ==  'contributor' ){
			$redirecturl = get_permalink(33);
		}else{
			$redirecturl = home_url();
		}
		echo json_encode(array('loggedin'=>true, 'redirecturl'=>$redirecturl, 'message'=>__('Login successful, redirecting...')));
	}

	die();
}
add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
	if (!current_user_can('administrator') && !is_admin()) {
		show_admin_bar(false);
	}
}

function aw_scripts() {
	// Register the script
	wp_register_script( 'aw-custom', get_stylesheet_directory_uri(). '/inc/assets/js/ajax-login-script.js', array('jquery'), '1.1', true );

	// Localize the script with new data
	$script_data_array = array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
	);
	wp_localize_script( 'aw-custom', 'aw', $script_data_array );

	// Enqueued script with localized data.
	wp_enqueue_script( 'aw-custom' );
}

function file_upload_callback() {
	$arr_img_ext = array('image/png', 'image/jpeg', 'image/jpg', 'image/gif');
	if (in_array($_FILES['file']['type'], $arr_img_ext)) {
		$upload = wp_upload_bits($_FILES["file"]["name"], null, file_get_contents($_FILES["file"]["tmp_name"]));
		//$upload['url'] will gives you uploaded file path
		echo $upload['url'];
	}
	wp_die();
}
add_action( 'wp_enqueue_scripts', 'aw_scripts' );
add_action( 'wp_ajax_file_upload', 'file_upload_callback' );
add_action( 'wp_ajax_nopriv_file_upload', 'file_upload_callback' );

function add_Subscription_fee() {
	global $wpdb;
	$data = array();
	$data['year'] = $_POST['slctYear'];
	$data['quater'] = $_POST['slctQuarter'];
	$data['subscription_fee'] = $_POST['sbcrpnfee'];
	$data['late_fee'] = $_POST['latefee'];
	$data['abroad_fee'] = $_POST['abrdfee'];
	$data['user_id'] = $_POST['user_id'];
	$table_name = $wpdb->prefix . "quarter_setup";
	$sql = "SELECT * FROM $table_name WHERE year = '$data[year]' AND quater = '$data[quater]'";
	$results = $wpdb->get_results($sql);
	if(count($results) > 0) {
		return "Failed! duplicate quarter";
	}else{
		if($wpdb->insert( $table_name, $data )){
			return;
		}else{
			return $wpdb->last_error;
		}
	}
}

function update_Subscription_fee() {
	global $wpdb;
	$year = $_POST['slctYear'];
	$quater = $_POST['slctQuarter'];
	$subscription_fee = $_POST['sbcrpnfee'];
	$late_fee = $_POST['latefee'];
	$abroad_fee = $_POST['abrdfee'];
	$user_id = $_POST['user_id'];
	$id = $_POST['id'];
	$table_name = $wpdb->prefix . "quarter_setup";
	$sql = "UPDATE $table_name SET year=$year , quater='$quater' , subscription_fee=$subscription_fee , late_fee=$late_fee , abroad_fee=$abroad_fee , user_id=$user_id WHERE id=$id";
	if($wpdb->query($wpdb->prepare($sql))){
		return;
	}else {
		return $wpdb->last_error;
	}
}

function get_Subscription_fee_list() {
	global $wpdb;
	$table_name = $wpdb->prefix . "quarter_setup";
	$sql = "SELECT * FROM $table_name";
	$results = $wpdb->get_results($sql);
	return $results;
}

add_filter( 'wp_nav_menu_items', 'wti_loginout_menu_link', 10, 2 );

function wti_loginout_menu_link( $items, $args ) {
	$user = wp_get_current_user();
	if ( $user->roles{0}  == 'contributor' || $user->roles{0}  == 'author' ) {
		$redirecturl = get_permalink(65);
	}else {
		$redirecturl = home_url();
	}
	if ($args->theme_location == 'primary') {
		if (is_user_logged_in()) {
			$items .= '<li class="right"><a href="'. wp_logout_url($redirecturl) .'">'. __("Log Out") .'</a></li>';
		}
	}
   return $items;
}