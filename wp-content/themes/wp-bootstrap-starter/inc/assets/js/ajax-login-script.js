jQuery(document).ready(function($) {
    // Show the login dialog box on click
    $('a#show_login').on('click', function(e){
        $('body').prepend('<div class="login_overlay"></div>');
        $('form#login').fadeIn(500);
        $('div.login_overlay, form#login a.close').on('click', function(){
            $('div.login_overlay').remove();
            $('form#login').hide();
        });
        e.preventDefault();
    });

    // Perform AJAX login on form submit
    $('form#login').on('submit', function(e){
        $('form#login p.status').show().text(ajax_login_object.loadingmessage);
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajax_login_object.ajaxurl,
            data: { 
                'action': 'ajaxlogin', //calls wp_ajax_nopriv_ajaxlogin
                'username': $('form#login #username').val(), 
                'password': $('form#login #password').val(), 
                'security': $('form#login #security').val() },
            success: function(data){
                $('form#login p.status').text(data.message);
                if (data.loggedin == true){
                    document.location.href = data.redirecturl;
                }
            }
        });
        e.preventDefault();
    });

	$("#changepass").on('click', function(e) {
		var newpassword = $("#newpassword").val().trim();
		var confpassword = $("#confpassword").val().trim();
		console.log(newpassword);
	});
	
   	$("#profile .btn-upload").on('click', function(e){
        $this = $("#file");
        file_data = $this.prop('files')[0];
        form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('action', 'file_upload');
        $.ajax({
            url: aw.ajaxurl,
            type: 'POST',
            contentType: false,
            processData: false,
            data: form_data,
            success: function (response) {
            	$this.val('');
                alert('File uploaded successfully.');
            }
        });
    });
});