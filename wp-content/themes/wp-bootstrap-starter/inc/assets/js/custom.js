jQuery(document).ready(function($) {

$(".addSubFeeBtn").on("click",function() {
	//alert();
	if ($("#addSubscriptionfeeform").valid()) {
		
		// Todo
	}
});

$("#clearSubFee").on("click",function() {
	$(".addSubFeeBtn").show();
	$(".updateSubFeeBtn").hide();
	$("#dataid").val("");
	$("#task").val("addSubFee");
});

var subscriptionTable = $('#subscriptionTable').DataTable({
	order : [ [ 1, 'desc' ], [ 2, 'desc' ] ],
	"columnDefs" : [ {
		"targets" : [ 0 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : 'no-sort',
		"orderable" : false
	}, {
		"targets" : -1,
		"data" : null,
		"defaultContent" : "<button class='tabeleditbtn'>Edit</button>",

	} ]
});

$('#subscriptionTable tbody').on( 'click', 'button', function () {
        var data = subscriptionTable.row( $(this).parents('tr') ).data();
        $("#dataid").val(data[0]);
		$("#slctYear").val(data[1]);
		$("#slctQuarter").val(data[2]);
		$("#sbcrpnfee").val(data[3]);
		$("#latefee").val(data[4]);
		$("#abrdfee").val(data[5]);
		$("#task").val("update");
		$(".updateSubFeeBtn").show();
		$(".addSubFeeBtn").hide();
		$("html, body").animate({ scrollTop: 0 }, "slow");
		$(".alert").remove();
		 
    } );

});