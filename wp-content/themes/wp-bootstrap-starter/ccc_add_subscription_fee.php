<?php /* Template Name: cccl_add_subscription_fee */ ?>


<?php get_header(); 
add_filter('show_admin_bar', '__return_false');
$user = wp_get_current_user();
?>
<div class="container">
	<div class="row">
		<div class="col-12">
		<?php 
		if($_POST['task'] == "addSubFee") {
			$result = add_Subscription_fee();
			if(!empty($result)) {?>
			<div class="alert alert-danger">
			  <?php echo $result;?>
			</div>			
			<?php 
			}else {
			?>
			<div class="alert alert-success">
			  Record Inserted Successfully !!.
			</div>
		<?php 				
			}
		}elseif ($_POST['task'] == "update") {
			$result = update_Subscription_fee();
			if(!empty($result)) {?>
			<div class="alert alert-danger">
			  <?php echo $result;?>
			</div>			
			<?php 
			}else {
			?>
			<div class="alert alert-success">
			  Record Updated Successfully !!.
			</div>
		<?php 				
			}
		}
		?>
		</div>
	</div>
	<div class="row get-pass-sec">
    	<div class="col-12">
         	<p>Add/Update Subscription fee</p>
         	<hr>
     	</div>
    </div>
	<form id="addSubscriptionfeeform" method="post">
       	<div class="form-group row">
            <label for="staticEmail" class="col-sm-2 col-form-label">Select Year</label>
            <div class="col-sm-10">
              <select class="form-control inputField" id="slctYear" name="slctYear" required>
              <option value=""></option>
                  <?php 
                  for($i=1940; $i<=2140; $i++)
                  {
                      echo "<option value=".$i.">".$i."</option>";
                  }
                  ?> 
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="staticEmail" class="col-sm-2 col-form-label">Select Quarter</label>
            <div class="col-sm-10">
              	<select class="form-control inputField" id="slctQuarter" name="slctQuarter" required>
					<option value=""></option>
					<option value="Q1">Q1</option>
					<option value="Q2">Q2</option>
					<option value="Q3">Q3</option>
					<option value="Q4">Q4</option>
				</select>
            </div>
        </div>
        <div class="form-group row">
            <label for="staticEmail" class="col-sm-2 col-form-label">Subscription fee</label>
            <div class="col-sm-10">
              <input type="text" class="form-control inputField" id="sbcrpnfee" name="sbcrpnfee"  placeholder="" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="staticEmail" class="col-sm-2 col-form-label">Late fee (%)</label>
            <div class="col-sm-10">
              <input type="text" class="form-control inputField" id="latefee" name="latefee" placeholder="" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="staticEmail" class="col-sm-2 col-form-label">Abroad fee (%)</label>
            <div class="col-sm-10">
              <input type="text" class="form-control inputField" id="abrdfee" name="abrdfee" placeholder="" required>
            </div>
        </div>
         <div class="form-group row">
            <label for="staticEmail" class="col-sm-2 col-form-label"></label>
            <div class="col-sm-10">
              <button type="submit" class="btn btn-primary mb-2 addSubFeeBtn">Add</button>
              <button type="submit" class="btn btn-primary mb-2 updateSubFeeBtn" style="display: none">Update</button>
              <button type="reset" class="btn btn-secondary mb-2" id="clearSubFee">Clear</button>
              <input type="hidden" value="addSubFee" name="task" id="task">
              <input type="hidden" value="" name="id" id="dataid">
              <input type="hidden" value="<?php echo $user->ID;?>" name="user_id">
            </div>
        </div>
	</form>
	
	
	<div class="row get-pass-sec">
    	<div class="col-12">
         	<p>All Subscription fee</p>
         	<hr>
     	</div>
    </div>
	<div class="row">
		<div class="col-12">
		<table id="subscriptionTable" class="table table-striped datatable">
            <thead>
                <tr>
                	<th>id</th>
                    <th>Year</th>
                    <th>Quarter</th>
                    <th>Subscription fee</th>
                    <th>Late fee</th>
                    <th>Abroad fee</th>
                    <th class="no-sort"></th>
                </tr>
            </thead>
            <tbody>
            <?php 
            $results = get_Subscription_fee_list();
            if(count($results)>0) {
            	foreach ($results as $value) {?>
                <tr>
                	<td><?php echo $value->id;?></td>
                    <td><?php echo $value->year;?></td>
                    <td><?php echo $value->quater;?></td>
                    <td><?php echo $value->subscription_fee;?></td>
                    <td><?php echo $value->late_fee;?></td>
                    <td><?php echo $value->abroad_fee;?></td>
                    <td><a href="#">Edit</a></td>
                </tr>           	
           	<?php }
            }
           	?>
           	
            </tbody>
        </table> 
        </div>
	
	</div>
    
</div>
<?php get_footer(); ?>