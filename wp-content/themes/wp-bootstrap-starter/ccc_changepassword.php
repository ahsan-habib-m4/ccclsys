<?php /* Template Name: cccl_login */ ?>


<?php get_header(); 
add_filter('show_admin_bar', '__return_false');
?>

<div id="primary" class="content-area cccl-login">

<div class="entry-content">

<?php 
if ( current_user_can( 'subscriber' ) ){
	echo "Hi, dear Member! Glad seeing you again!";
}elseif (current_user_can( 'contributor' )){
	echo "Hi, dear Operator! Glad seeing you again!";
}
if (is_user_logged_in()) { ?>
<a class="login_button" href="<?php echo wp_logout_url( home_url() ); ?>">Logout</a>
<?php } else { 

    global $post;
    $post_slug = $post->post_name;
    ?>
	<div class="container">
		<div class="row-fluid">
			<p class="member-icon text-center">
				<i class="fas fa-user"></i>
			</p>
            <h2 class="text-center">Change Password</h2>
		</div>
    	<div class="row-fluid">
    		<form id="changepassword" action="" method="post">
    		<fieldset>
    	        <p class="status"></p>
    	        <div class="fontpassword">
                    <input type="password" placeholder="Old Password" name="oldpassword" id="oldpassword"  required class="form-control txt-input">  
                    <i class="fa fa-key fa-lg"></i> 
                </div>    
                <div class="fontpassword">
                    <input type="password" placeholder="New Password" name="newpassword" id="newpassword"  required class="form-control txt-input">  
                    <i class="fa fa-key fa-lg"></i> 
                </div>   
                <div class="fontpassword">
                    <input type="password" placeholder="Confirm Password" name="confpassword" id="confpassword"  required class="form-control txt-input">  
                    <i class="fa fa-key fa-lg"></i> 
                </div>   
    			<p>
    		        <input class="submit_button btn btn-primary " type="submit" value="Change password" name="changepass" id="changepass">
    		        <?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
    		    </p>    
    	        </fieldset>
    		</form>
    	</div>	
	</div>
<?php } ?>
</div>
</div>
<?php get_footer(); ?>