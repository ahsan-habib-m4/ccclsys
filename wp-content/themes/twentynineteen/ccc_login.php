<?php /* Template Name: cccl_login */ ?>


<?php get_header(); 
add_filter('show_admin_bar', '__return_false');
?>

<div id="primary" class="content-area">
<main id="main" class="site-main">
<header class="entry-header">
		
<h1 class="entry-title">Sample Page</h1>
	</header>
<div class="entry-content">

<?php if (is_user_logged_in()) { ?>
<a class="login_button" href="<?php echo wp_logout_url( home_url() ); ?>">Logout</a>
<?php } else { ?>
<form id="login" action="login" method="post">
        <h1>Site Login</h1>
        <p class="status"></p>
        <label for="username">Username</label>
        <input id="username" type="text" name="username">
        <label for="password">Password</label>
        <input id="password" type="password" name="password">
        <a class="lost" href="<?php echo wp_lostpassword_url(); ?>">Lost your password?</a>
        <input class="submit_button" type="submit" value="Login" name="submit">
        <?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
</form>
<?php } ?>
</div>
</main>
</div>
<?php get_footer(); ?>