<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'ccclnew' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '(@R.^E2xq3}(~}:.$&I`q28y`te3|C58*,;ghlbzJC]vvYyUT]^aZ/OK([K,UHgL' );
define( 'SECURE_AUTH_KEY',  'v$Gli4eH;8 m4<f&YFB1zz&=fHZ$Cm5>47D?%X,H`_Q!ioz:HtnOVcgp-5BM-_$o' );
define( 'LOGGED_IN_KEY',    'D5,v1B,nH^o].vSy~hZlL7 9^G1T)*RJTk%jw*?e4VO>])TQ0JbeiW8GI%@&[*!,' );
define( 'NONCE_KEY',        'Tb!]C3u fdr:grJm%p)C3DTjO^a%/!Zc>xTp/B=z;g^.*XZSM7ChRe7?},`f,BZy' );
define( 'AUTH_SALT',        '*}R&pt;%G5n1 S#@QzA@P3HbX.{`5&{0Ph4:K9(oSR$K~r<))a2>@Rs#>k(Ag{Z[' );
define( 'SECURE_AUTH_SALT', '@5`emS<m<ZyeKn`mvBhupvvJf[NcdVQ7&tv#~FCg`#<ARi}@d8Z>d,tIbxAt>Q_f' );
define( 'LOGGED_IN_SALT',   '%@PMT%K>0uk2M,*Zypo. {n>s@R6iyO&?g@Z-u(3}pBx71bu:xN5w_$pgML.Lgoz' );
define( 'NONCE_SALT',       'nG9PJnCZ>w.>HG{p#|?S2T}{spK[]4:[GqXfl<E&u$M|N.`TQx#[;5U?<3t)bg~V' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'cccl_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

define('FS_METHOD','direct');

/** Mail */
define('SENDER_EMAIL','no-reply@cccl.metafour.com');
define('SENDER_NAME','Cadet College Club Limited');

/* EBL payment page */
define('EBL_ORDER_CURRENCY', 'BDT');
define('EBL_CALCEL_PAGE_ID', 214);
define('EBL_RETURN_PAGE_ID', 210);
define('EBL_TIMEOUT_PAGE_ID', 212);
define('EBL_TIMEOUT', 600);
define('EBL_OPERATION', 'PURCHASE');
define('EBL_MARCHANT_NAME', 'Cadet College Club Limited');
define('EBL_MARCHANT_LOGO', 'https://cccl.metafour.com/wp-content/themes/hestia/assets/img/small_logo.png');
define('EBL_BILLING_ADDRESS', 'HIDE');
define('EBL_ORDER_SUMMARY', 'HIDE');

/* SSL payment page */
define('PUBLIC_PAGES', '65,70,67,256,261,312,328,332');

/* SMS */
define('USER', 'CCCL');
define('PASSWORD', '37G9h682');
define('SID', 'CCCLBng');
define('URL', 'http://sms.sslwireless.com/pushapi/dynamic/server.php');

/* HSC year */
define('HSC_FROM_YR', 1940);

/* Category */
define('MEMBER', 'Directory');
define('SUBSFEE', 'Subscription');
define('EVENT', 'Event');
define('SERVICE', 'Service');
define('PAYMENT', 'Payment');
define('VENUE', 'Venue');
define('DEVELOPMENT', 'Development');

/* R2M login secret key */
define('SECRET_KEY', 'sti6asefriThuGunumipipre');

define('PRODUCT_CONFIRM', 241);
define('PAYMENT_PROCESS', 208);
define('TEMP_CONFIRM', 312);
define('TEMP_REPORT', 296);

define('CC_EMAIL', 'adnanrabbi@gmail.com');

/* Define receipt id prefix */
define('PAYMENT_RECEIVED', 'R');
define('PAYMENT_ONLINE', 'M');

/* Custom log path */
define('CMS_LOG_PATH', 'log/');
define( 'SQL_DEBUG', true );
define( 'WEBMSG_DEBUG', true );
